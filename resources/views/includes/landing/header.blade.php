<div data-collapse="medium" data-animation="default" data-duration="400" class="header w-nav">
    <div class="w-embed">
        <style>
            @media screen and (max-width: 1140px) {
                .phone-link {
                    display: none;
                }
            }

            @media screen and (max-width: 991px) {
                .phone-link {
                    display: inline-block;
                }
            }
        </style>
    </div>


    <div class="preheader">
        <div class="container-1200 icon-info">
            <p class="paragraph-eyebrow"> &nbsp; &nbsp;
                <!-- google translator -->
            <div id="google_translate_element" style="text-align: center !important;"></div>

            <script type="text/javascript">
                function googleTranslateElementInit() {
                    new google.translate.TranslateElement({pageLanguage: 'en'}, 'google_translate_element');
                }
            </script>

            <script type="text/javascript"
                    src="http://translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"></script>

            <!-- google translator end -->

            <br/></p>
        </div>


        <div class="eyebrow-overlay"></div>
    </div>
    <div class="header-blue">
        <a href="{{route('landing-home')}}" class="logo w-nav-brand w--current"></a>
{{--        <a class="phone-link w-nav-link">+1 (405) 389-4204</a>--}}
        <a class="phone-link w-nav-link">+1 (450) 274-6030</a>
    </div>
    <div class="menu-button w-nav-button">
        <div class="hamburger-top"></div>
        <div class="hamburger-middle"></div>
        <div class="hamburger-bottom"></div>
    </div>
    <div class="container-login hide-tablet"><a data-goodbye-skip="1" href="{{route('login')}}"
                                                class="link-login margin10-right">Consumer Login</a><a
            data-goodbye-skip="1" href="{{route('login')}}"
            class="link-login">Business Login</a></div>
    <nav role="navigation" class="nav-menu w-nav-menu">
        <a href="{{route('login')}}" data-goodbye-skip="1"
           class="nav-link hide-desktop w-nav-link">Consumer Login</a><a
            href="{{route('login')}}" data-goodbye-skip="1"
            class="nav-link hide-desktop w-nav-link">Business Login</a>
        <div data-delay="0" data-hover="1" class="w-dropdown">
            <div class="nav-link w-dropdown-toggle">
                <div class="text-dropdown">About Us</div>
                <div class="icon-dropdown w-icon-dropdown-toggle"></div>
            </div>
            <nav class="dropdown-list w-dropdown-list">
{{--                <a href="{{route('in-the-news')}}"--}}
{{--                   class="nav-link-dropdown w-dropdown-link">History</a>--}}
                <a href="{{route('in-the-news')}}"
                   class="nav-link-dropdown w-dropdown-link">In The News</a>
{{--                <a href="{{route('in-the-news')}}"--}}
{{--                   class="nav-link-dropdown w-dropdown-link">Core Values</a>--}}

            </nav>


        </div>

        <div data-delay="0" data-hover="1" class="w-dropdown">
            <div class="nav-link w-dropdown-toggle">
                <div class="text-dropdown">Leadership</div>
                <div class="icon-dropdown w-icon-dropdown-toggle"></div>
            </div>

            <nav class="dropdown-list w-dropdown-list"><a href="{{route('boards')}}"
                                                          class="nav-link-dropdown w-dropdown-link">Board of Directors</a><a
                    href="{{route('executives')}}"
                    class="nav-link-dropdown w-dropdown-link">Executive Team</a></nav>
        </div>


        <div data-delay="0" data-hover="1" class="w-dropdown">
            <div class="nav-link w-dropdown-toggle">
                <div class="text-dropdown">Business Banking</div>
                <div class="icon-dropdown w-icon-dropdown-toggle"></div>
            </div>
            <nav class="dropdown-list w-dropdown-list"><a href="{{route('business-checking')}}"
                                                          class="nav-link-dropdown w-dropdown-link">Business Checking</a><a
                    href="{{route('business-savings')}}"
                    class="nav-link-dropdown w-dropdown-link">Business Savings &amp; Investments</a><a
                    href="{{route('business-treasury')}}"
                    class="nav-link-dropdown w-dropdown-link">Treasury Management</a><a
                    href="{{route('business-cdars')}}"
                    class="nav-link-dropdown w-dropdown-link">CDARS</a><a
                    href="{{route('business-credit-cards')}}"
                    class="nav-link-dropdown w-dropdown-link">Business Credit Cards</a><a
                    href="{{route('business-loans')}}"
                    class="nav-link-dropdown w-dropdown-link">Business Loans</a><a
                    href="{{route('business-online-banking')}}"
                    class="nav-link-dropdown w-dropdown-link">Online Banking</a><a
                    href="{{route('business-other-services')}}"
                    class="nav-link-dropdown w-dropdown-link">Other Business Services</a></nav>
        </div>
        <div data-delay="0" data-hover="1" class="w-dropdown">
            <div class="nav-link w-dropdown-toggle">


                <div class="text-dropdown">Personal Banking</div>
                <div class="icon-dropdown w-icon-dropdown-toggle"></div>
            </div>
            <nav class="dropdown-list w-dropdown-list"><a href="{{route('personal-checking')}}"
                                                          class="nav-link-dropdown w-dropdown-link">Personal Checking</a><a
                    href="{{route('personal-savings')}}"
                    class="nav-link-dropdown w-dropdown-link">Savings &amp; Investments</a><a
                    href="{{route('personal-cdars')}}"
                    class="nav-link-dropdown w-dropdown-link">CDARS</a><a
                    href="{{route('personal-credit-cards')}}"
                    class="nav-link-dropdown w-dropdown-link">Personal Credit Card</a><a
                    href="{{route('personal-loans')}}"
                    class="nav-link-dropdown w-dropdown-link">Personal Loans</a><a
                    href="{{route('personal-online-services')}}" class="nav-link-dropdown w-dropdown-link">Online Services</a><a
                    href="{{route('personal-other-services')}}"
                    class="nav-link-dropdown w-dropdown-link">Other Services</a>
            </nav>
        </div>
        <a href="{{route('blog')}}" class="nav-link w-nav-link">Blog</a>
        <a href="{{route('contact')}}" class="nav-link w-nav-link">Contact</a>
    </nav>
</div>
