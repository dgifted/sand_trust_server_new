<footer class="section-footer">
    <div class="container1200">
        <div class="w-clearfix"><a href="#" class="logo-footer w-nav-brand"></a>
            <div class="fdic"></div>
            <a class="bbb w-inline-block"></a>
        </div>
        <div class="row-4 w-row">
            <div class="column-12 w-col w-col-3 w-col-small-small-stack">
                <div class="subhead-white">Ottawa</div>
                <p class="footer-heading">PL Ottawa, ON KLZ 5l5, Canada<br/><a
                        class="link-footer inline-block"></a></p>
            </div>
            <div class="column-13 w-col w-col-3 w-col-small-small-stack">
                <div class="subhead-white">Kanata</div>
                <p class="footer-heading">Steacie Dr, Kanata, ON K2K 2A9 Canada<br/>
                    <a href="#"
                       class="link-footer inline-block"></a>
                </p>
            </div>
            {{--            <div class="column-11 w-col w-col-3 w-col-small-small-stack">--}}
            {{--                <div class="footer-block">--}}
            {{--                    <div class="subhead-white">Pawhuska</div>--}}
            {{--                </div>--}}
            {{--                <p class="footer-heading">101 E. 8th Street<br/>Pawhuska, OK 74056<br/><a--}}
            {{--                        class="link-footer inline-block"></a></p>--}}
            {{--            </div>--}}
            {{--            <div class="column-10 w-col w-col-3 w-col-small-small-stack">--}}
            {{--                <div class="subhead-white">Cleveland</div>--}}
            {{--                <p class="footer-heading">400 N. Broadway<br/>Cleveland, OK 74020<br/><a--}}
            {{--                        class="link-footer inline-block"></a></p>--}}
            {{--            </div>--}}
        </div>
    {{--        <div class="row-4 w-row">--}}
    {{--            <div class="column-12 w-col w-col-3 w-col-small-small-stack">--}}
    {{--                <div class="subhead-white">Cushing</div>--}}
    {{--                <p class="footer-heading">224 East Broadway<br/>Cushing, OK 74023<br/><a class="link-footer"></a></p>--}}
    {{--            </div>--}}
    {{--            <div class="column-13 w-col w-col-3 w-col-small-small-stack">--}}
    {{--                <div class="subhead-white">Cushing</div>--}}
    {{--                <p class="footer-heading">2106 East Main Street<br/>Cushing, OK 74023<br/><a class="link-footer"></a>--}}
    {{--                </p>--}}
    {{--            </div>--}}
    {{--            <div class="column-11 w-col w-col-3 w-col-small-small-stack"></div>--}}
    {{--            <div class="column-10 w-col w-col-3 w-col-small-small-stack"></div>--}}
    {{--        </div>--}}
    </div>
    <div class="container1200">
        <div class="divider-dark"></div>

        <div class="row-4 w-row">
            <div class="column-12 w-col w-col-3">
                <div class="subhead-white">Navigation</div>
                <div class="footer-block"><a class="link-footer">About</a>
                    <a href="{{route('boards')}}"
                       class="link-footer">Leadership</a><a href="{{route('business-checking')}}"
                                                            class="link-footer">Business Banking</a><a
                        href="{{route('personal-checking')}}"
                        class="link-footer">Personal Banking</a><a href="{{route('contact')}}" class="link-footer">Contact</a>
                </div>
            </div>
            <div class="column-13 w-col w-col-3">
                <div class="subhead-white">System</div>
                <div class="footer-block"><a class="link-footer">Privacy Policy</a><a href="{{route('security')}}"
                                                                                      class="link-footer">Security</a><a
                        class="link-footer">Terms of Use</a><a class="link-footer">USA Patriot
                        Act</a></div>
            </div>
            <div class="column-11 w-col w-col-3">
                <div class="subhead-white">Login</div>
                <div class="footer-block"><a href="{{route('login')}}" target="_blank" data-goodbye-skip="1"
                                             class="link-footer">Consumer Login</a><a href="{{route('login')}}"
                                                                                      target="_blank"
                                                                                      data-goodbye-skip="1"
                                                                                      class="link-footer">Business
                        Login</a></div>


                <div class="subhead-white">Social</div>
                <div class="footer-block"><a class="social-facebook w-inline-block"></a><a
                        class="social-linkedin w-inline-block"></a></div>
            </div>
            <div class="column-10 w-col w-col-3">
                <div class="subhead-white">App</div>
                <div class="footer-block">
                    <div class="app-store"></div>
                    <div class="coming-soon"><a class="link-footer inline-block">Business</a> | <a
                            class="link-footer inline-block">Consumer</a> (iPhone)<br/><a
                            class="link-footer inline-block">Business</a> | <a
                            class="link-footer inline-block">Consumer</a> (iPad)
                    </div>
                </div>
                <div class="footer-block">
                    <div class="google-play"></div>
                    <div class="coming-soon"><a class="link-footer inline-block">Business</a> | <a
                            class="link-footer inline-block">Consumer</a></div>
                </div>
            </div>
        </div>
    </div>
    <div class="divider-dark"></div>
    <div class="copyright">© <label id="date"></label> Sand Trust Bank</div>
</footer>
