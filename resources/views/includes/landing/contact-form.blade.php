<form action="">
    <div class="rich-text-block w-richtext">
        <h2>Get in touch with us:</h2>
    </div>
    <div class="margin10-top w-clearfix">
        <div class="column-half-left">
            <p class="paragraph-small-light">Name*</p><input type="text"
                                                             class="input-field-white user gray w-input"
                                                             maxlength="256" name="Name-8"
                                                             data-name="Name 8" id="Name-8"
                                                             required=""/>
            <p class="paragraph-small-light">Email*</p><input type="email"
                                                              class="input-field-white email gray w-input"
                                                              maxlength="256" name="Email-9"
                                                              data-name="Email 9"
                                                              id="Email-9" required=""/>
            <p class="paragraph-small-light">Phone*</p><input type="tel"
                                                              class="input-field-white phone gray w-input"
                                                              maxlength="256" name="Phone-5"
                                                              data-name="Phone 5" id="Phone-5"
                                                              required=""/>
        </div>
        <div class="column-half-right">
            <p class="paragraph-small-light">Are you a current customer?*</p><label
                class="radio-bullet margin10-top w-radio"><input type="radio"
                                                                 data-name="Current Customer?" id="Yes"
                                                                 name="Current-Customer" value="Yes"
                                                                 required=""
                                                                 class="w-form-formradioinput checkbox w-radio-input"/><span
                    for="Yes-2"
                    class="label-checkbox w-form-label">Yes</span></label><label
                class="radio-bullet margin-radio w-radio"><input type="radio"
                                                                 data-name="Current Customer?" id="No"
                                                                 name="Current-Customer" value="No"
                                                                 required=""
                                                                 class="w-form-formradioinput checkbox w-radio-input"/><span
                    for="No-2"
                    class="label-checkbox w-form-label">No</span></label>
{{--            <div class="margin20-top">--}}
{{--                <p class="paragraph-small-light">Preferred {{config('app.name')}} Bank branch:*</p>--}}
{{--            </div>--}}
{{--            <label class="radio-bullet radio-block w-radio"><input type="radio"--}}
{{--                                                                   data-name="Preferred {{config('app.name')}} Branch:"--}}
{{--                                                                   id="Tulsa-2"--}}
{{--                                                                   name="Preferred-Blue-Sky-Branch"--}}
{{--                                                                   value="Tulsa"--}}
{{--                                                                   required=""--}}
{{--                                                                   class="w-form-formradioinput checkbox w-radio-input"/><span--}}
{{--                    for="Tulsa-2"--}}
{{--                    class="label-checkbox w-form-label">Tulsa</span></label><label--}}
{{--                class="radio-bullet radio-block w-radio"><input type="radio"--}}
{{--                                                                data-name="Preferred {{config('app.name')}} Branch:"--}}
{{--                                                                id="Pawhuska"--}}
{{--                                                                name="Preferred-Blue-Sky-Branch"--}}
{{--                                                                value="Pawhuska" required=""--}}
{{--                                                                class="w-form-formradioinput checkbox w-radio-input"/><span--}}
{{--                    for="Pawhuska-2"--}}
{{--                    class="label-checkbox w-form-label">Pawhuska</span></label><label--}}
{{--                class="radio-bullet radio-block w-radio"><input type="radio"--}}
{{--                                                                data-name="Preferred {{config('app.name')}} Branch:"--}}
{{--                                                                id="Cleveland"--}}
{{--                                                                name="Preferred-Blue-Sky-Branch"--}}
{{--                                                                value="Cleveland" required=""--}}
{{--                                                                class="w-form-formradioinput checkbox w-radio-input"/><span--}}
{{--                    for="Cleveland-2"--}}
{{--                    class="label-checkbox w-form-label">Cleveland</span></label><label--}}
{{--                class="radio-bullet radio-block w-radio"><input type="radio"--}}
{{--                                                                data-name="Preferred {{config('app.name')}} Branch:"--}}
{{--                                                                id="Cushing-2"--}}
{{--                                                                name="Preferred-Blue-Sky-Branch"--}}
{{--                                                                value="Cushing" required=""--}}
{{--                                                                class="w-form-formradioinput checkbox w-radio-input"/><span--}}
{{--                    for="Cushing-2"--}}
{{--                    class="label-checkbox w-form-label">Cushing</span></label>--}}
            <div class="margin20-top">
                <p class="paragraph-small-light">What type of help do you need?*</p>
            </div>
            <label class="radio-bullet radio-block w-radio"><input type="radio"
                                                                   data-name="What type of help do you need?"
                                                                   id="Product inquiry-2"
                                                                   name="What-type-of-help-do-you-need"
                                                                   value="Product inquiry" required=""
                                                                   class="w-form-formradioinput checkbox w-radio-input"/><span
                    for="Product inquiry-3"
                    class="label-checkbox w-form-label">Product inquiry</span></label><label
                class="radio-bullet radio-block w-radio"><input type="radio"
                                                                data-name="What type of help do you need?"
                                                                id="Customer service request-2"
                                                                name="What-type-of-help-do-you-need"
                                                                value="Customer service request"
                                                                required=""
                                                                class="w-form-formradioinput checkbox w-radio-input"/><span
                    for="Customer service request-3" class="label-checkbox w-form-label">Customer service
                  request</span></label>
        </div>
        <div class="clear-both">
            <p class="paragraph-small-light margin20-top-tablet">What can we help you with?*</p>
            <textarea
                name="What-can-we-help-with-2" maxlength="5000" id="What-can-we-help-with-2" required=""
                data-name="What Can We Help With 2" class="textarea-2 w-input"></textarea>
        </div>
    </div>
    <input type="submit" value="Submit" data-wait="Please wait..."
           class="submit-hero full-width w-button"/>
</form>
