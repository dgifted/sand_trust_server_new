<!-- timeline time label -->
<div class="time-label">
    <span class="bg-red">@yield('label')</span>
</div>
<!-- /.timeline-label -->

<!-- timeline item -->
<div>
    <i class="fas fa-envelope bg-blue"></i>
    <div class="timeline-item">
        <span class="time"><i class="fas fa-clock"></i> @yield('time')</span>
        <h3 class="timeline-header">@yield('header')</h3>

        <div class="timeline-body">@yield('body')</div>
    </div>
</div>
<!-- END timeline item -->
