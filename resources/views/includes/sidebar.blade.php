@php
    $route = \Route::currentRouteName();
@endphp

<!-- Main Sidebar Container -->
<aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    <a href="{{route('home')}}" class="brand-link">
        <img src="{{asset('assets/dist/img/AdminLTELogo.png')}}" alt="AdminLTE Logo"
             class="brand-image img-circle elevation-3"
             style="opacity: .8">
        <span class="brand-text font-weight-light">{{config('app.name')}}</span>
    </a>

    <!-- Sidebar -->
    <div class="sidebar">
        <!-- Sidebar Menu -->
        <nav class="mt-2">
            <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
                <li class="nav-item">
                    <a href="{{route('admin.customers')}}"
                       class="nav-link {{$route === 'admin.customers' || $route === 'admin.customers.single' ? 'active' : ''}}">
                        <i class="nav-icon fas fa-user"></i>
                        <p>Customers</p>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="{{route('admin.accounts')}}"
                       class="nav-link {{$route === 'admin.accounts' || $route === 'admin.accounts.single' ? 'active' : ''}}">
                        <i class="fas fa-suitcase nav-icon"></i>
                        <p>Accounts</p>
                    </a>
                </li>

                <li class="nav-item">
                    <a href="{{route('admin.account-types')}}"
                       class="nav-link {{$route === 'admin.account-types' || $route === 'admin.account-types.single' || $route === 'admin.account-types.create' ? 'active' : ''}}">
                        <i class="fas fa-warehouse nav-icon"></i>
                        <p>Account Types</p>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="{{route('admin.deposits')}}"
                       class="nav-link {{$route === 'admin.deposits' ? 'active' : ''}}">
                        <i class="fas fa-wallet nav-icon"></i>
                        <p>Deposits</p>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="{{route('admin.cards')}}"
                       class="nav-link {{$route === 'admin.cards' ? 'active' : ''}}">
                        <i class="fas fa-credit-card nav-icon"></i>
                        <p>Cards</p>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="{{route('admin.card-requests')}}"
                       class="nav-link {{$route === 'admin.card-requests' ? 'active' : ''}}">
                        <i class="fas fa-directions nav-icon"></i>
                        <p>Card Requests</p>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="{{route('admin.credit-limit-requests')}}"
                       class="nav-link {{$route === 'admin.credit-limit-requests' ? 'active' : ''}}">
                        <i class="fas fa-border-all nav-icon"></i>
                        <p>Credit Limit Requests</p>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="{{route('admin.withdrawals')}}"
                       class="nav-link {{$route === 'admin.withdrawals' ? 'active' : ''}}">
                        <i class="fas fa-compress-arrows-alt nav-icon"></i>
                        <p>Withdrawals</p>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="{{route('admin.support-tickets')}}"
                       class="nav-link {{$route === 'admin.support-tickets' || $route === 'admin.support-tickets-single' ? 'active' : ''}}">
                        <i class="fas fa-clipboard-list nav-icon"></i>
                        <p>Support Tickets</p>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="{{route('admin.sent-emails')}}"
                       class="nav-link {{$route === 'admin.sent-emails' || $route === 'admin.send-new-email' ? 'active' : ''}}">
                        <i class="fas fa-mail-bulk nav-icon"></i>
                        <p>Emails</p>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="{{route('admin.settings')}}"
                       class="nav-link {{$route === 'admin.settings' ? 'active' : ''}}">
                        <i class="fas fa-cog nav-icon"></i>
                        <p>Setup</p>
                    </a>
                </li>
            </ul>
        </nav>
        <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
</aside>
