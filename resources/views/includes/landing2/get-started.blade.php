<!-- Get Start In start -->
<section class="get-start wow fadeInUp">
    <div class="overlay">
        <div class="container">
            <div class="col-12">
                <div class="get-content">
                    <div class="section-text">
                        <h3 class="title">Ready to get started?</h3>
                        <p>It only takes a few minutes to register your FREE {{config('app.name')}} account.</p>
                    </div>
                    <a href="{{ route('register') }}" class="cmn-btn">Open an Account</a>
                    <img src="{{asset('assets/type_2/images/get-start.png')}}" alt="images">
                </div>
            </div>
        </div>
    </div>
</section>
<!-- Get Start In end -->
