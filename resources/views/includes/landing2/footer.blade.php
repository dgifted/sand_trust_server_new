<!-- Footer Area Start -->
<div class="footer-section">
    <div class="container pt-120">
        <div class="row cus-mar pt-120 pb-120 justify-content-between wow fadeInUp">
            <div class="col-xl-3 col-lg-3 col-md-4 col-6">
                <div class="footer-box">
                    <a href="{{ route('landing-home') }}" class="logo">
                        <img src="{{asset('assets/type_2/images/logo.png')}}" alt="logo">
                    </a>
                    <p>A modern, technology-first bank built for you and your growing business.</p>
                    <div class="social-link d-flex align-items-center">
                        <a href="javascript:void(0)"><i class="fab fa-facebook-f"></i></a>
                        <a href="javascript:void(0)"><i class="fab fa-twitter"></i></a>
                        <a href="javascript:void(0)"><i class="fab fa-linkedin-in"></i></a>
                        <a href="javascript:void(0)"><i class="fab fa-instagram"></i></a>
                    </div>
                </div>
            </div>
            <div class="col-xl-2 col-lg-2 col-6">
                <div class="footer-box">
                    <h5>Company</h5>
                    <ul class="footer-link">
                        <li><a href="{{ route('about') }}">About Us</a></li>
{{--                        <li><a href="about.html">Awards</a></li>--}}
{{--                        <li><a href="career-single.html">Careers</a></li>--}}
                    </ul>
                </div>
            </div>
            <div class="col-xl-2 col-lg-2 col-6">
                <div class="footer-box">
                    <h5>Useful Links</h5>
                    <ul class="footer-link">
                        <li><a href="{{ route('accounts') }}">Accounts</a></li>
                        <li><a href="{{ route('business-loan') }}">Business Loan</a></li>
                        <li><a href="{{ route('affiliate') }}">Affiliate Program</a></li>
{{--                        <li><a href="blog-list.html">Blog</a></li>--}}
                    </ul>
                </div>
            </div>
            <div class="col-xl-2 col-lg-2 col-6">
                <div class="footer-box">
                    <h5>Support</h5>
                    <ul class="footer-link">
                        <li><a href="mailto:{{ config('mail.from.address') }}}">{!!config('mail.from.address') !!}</a></li>
                        <li><a href="{{ route('contact') }}">Contact Us</a></li>
                        <li><a href="{{ route('faqs') }}">FAQ</a></li>
                    </ul>
                </div>
            </div>
            <div class="col-xl-3 col-8">
                <div class="footer-box">
                    <h5>Subscribe</h5>
                    <form id="subscribe-form">
                        <div class="form-group">
                            <input type="email" id="subEmail" placeholder="Enter Your Email address" required>
                            <button class="cmn-btn" id="submit" type="submit">Subscribe</button>
                        </div>
                    </form>
                    <p>Get the latest updates via email. Any time you may unsubscribe</p>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                <div class="footer-bottom">
                    <div class="left">
                        <p> Copyright © <a href="{{ route('landing-home') }}">{{ config('app.name') }}</a>
                        </p>
                    </div>
                    <div class="right">
                        <a href="{{ route('privacy-policy') }}" class="cus-bor">Privacy </a>
                        <a href="{{ route('terms-conditions') }}">Terms &amp; Condition </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="img-area">
        <img src="{{asset('assets/type_2/images/footer-Illu-left.png')}}" class="left" alt="Images">
        <img src="{{asset('assets/type_2/images/footer-Illu-right.png')}}" class="right" alt="Images">
    </div>
</div>
<!-- Footer Area End -->
