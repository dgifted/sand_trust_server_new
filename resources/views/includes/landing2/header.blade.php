<!-- header-section start -->
<header class="header-section">
    <div class="overlay">
        <div class="container">
            <div class="row d-flex header-area">
                <nav class="navbar navbar-expand-lg navbar-light">
                    <a class="navbar-brand" href="{{ route('landing-home') }}">
                        <img src="{{asset('assets/type_2/images/logo.png')}}" class="logo" alt="logo">
                    </a>
                    <button class="navbar-toggler collapsed" type="button" data-bs-toggle="collapse"
                            data-bs-target="#navbar-content">
                        <i class="fas fa-bars"></i>
                    </button>
                    <div class="collapse navbar-collapse justify-content-end" id="navbar-content">
                        <ul class="navbar-nav mr-auto mb-2 mb-lg-0">
                            <li class="nav-item">
                                <a class="nav-link active" aria-current="page" href="{{ route('landing-home') }}">Home</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" aria-current="page" href="{{ route('about') }}">About Us</a>
                            </li>
                            <li class="nav-item dropdown main-navbar">
                                <a class="nav-link dropdown-toggle" href="javascript:void(0)" data-bs-toggle="dropdown"
                                   data-bs-auto-close="outside">Services</a>
                                <ul class="dropdown-menu main-menu shadow">
                                    <li><a class="nav-link" href="{{ route('accounts') }}">Account</a></li>
                                    <li class="dropend sub-navbar">
                                        <a href="javascript:void(0)" class="dropdown-item dropdown-toggle" data-bs-toggle="dropdown"
                                           data-bs-auto-close="outside">Loan</a>
                                        <ul class="dropdown-menu sub-menu shadow">
                                            <li><a class="nav-link" href="{{ route('business-loan') }}">Business Loans</a></li>
                                            <li><a class="nav-link" href="{{ route('educational-loan') }}">Educations Loans</a></li>
                                            <li><a class="nav-link" href="{{ route('home-loan') }}">Home Loans</a></li>
                                            <li><a class="nav-link" href="{{ route('car-loan') }}">Car Loans</a></li>
                                            <li><a class="nav-link" href="{{ route('personal-loan') }}">Personal Loans</a></li>
                                        </ul>
                                    </li>
                                    <li><a class="nav-link" href="{{ route('cards') }}">Card</a></li>
                                </ul>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="{{ route('contact') }}">Contact Us</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="{{ route('login') }}">Login</a>
                            </li>
                        </ul>
                        <div class="right-area header-action d-flex align-items-center">
                            <a href="{{ route('register') }}" class="cmn-btn">Open Account</a>
                        </div>
                    </div>
                </nav>
            </div>
        </div>
    </div>
</header>
<!-- header-section end -->
