@extends('layouts.landing-master')
@section('page-title', 'Contact')
@section('page-meta')
@stop

@section('content')
    <div id="section1" class="hero-inside about">
        <div class="hero-inside-container">
            <h1 class="h1-hero-inside">Contact {{config('app.name')}} Bank</h1>
        </div>
    </div>
    <div id="section2" class="section120-white">
        <div class="container1200 w-clearfix">
            <div class="column-left-nav"><a href="contact.html" class="nav-link-sidebar w--current">Contact</a>
                <div class="sidebar-text w-richtext">
                    <p>To report a lost or stolen ATM/Debit card please call:<br/>+1 (405) 389-4204</p>
                    <p>To open a new account, get more information, or ask a question about an existing account, please
                        contact
                        your nearest branch.</p>
                </div>
            </div>
            <div class="column-right-content">
                <div class="form-contact w-form">
                    @include('includes.landing.contact-form')
                </div>


                <div class="location-container w-clearfix">
                    <div class="column-left-small">
                        <div class="rich-text-block w-richtext">
                            <h2>Tulsa Boxyard</h2>
                            <p>502 East 3rd Street<br/>Container 1<br/>Tulsa, OK  74120<br/>+1 (405) 389-4204</p>
                            <p><strong>Hours</strong><br/>Mon-Fri: 10am-6pm<br/>Sat: 10am-1pm<br/></p>
                        </div>
                    </div>
                    <div class="column-right-map">
                        <div class="w-embed w-iframe">
                            <iframe
                                src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d805.3655993903769!2d-95.98544317071708!3d36.15530099875544!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x0!2zMzbCsDA5JzE5LjEiTiA5NcKwNTknMDUuNiJX!5e0!3m2!1sen!2sus!4v1581711188253!5m2!1sen!2sus"
                                width="100%" height="400" frameborder="0" style="border:0;" allowfullscreen=""></iframe>
                        </div>
                    </div>
                </div>
                <div class="location-container w-clearfix">
                    <div class="column-left-small">
                        <div class="rich-text-block w-richtext">
                            <h2>Tulsa</h2>
                            <p>3353 E. 41st Street<br/>Tulsa, OK 74135<br/>+1 (405) 389-4204</p>
                            <p><strong>Lobby Hours</strong><br/>Mon-Fri: 9am-4pm</p>
                            <p><strong>Drive Thru Hours</strong><br/>Mon-Thu: 7:30am-5:30pm<br/>Fri: 7:30am-6pm</p>
                        </div>
                    </div>
                    <div class="column-right-map">
                        <div class="w-embed w-iframe">
                            <iframe
                                src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d805.885255086525!2d-95.93845217071717!3d36.1046749987561!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x0!2zMzbCsDA2JzE2LjgiTiA5NcKwNTYnMTYuNSJX!5e0!3m2!1sen!2sus!4v1581711389756!5m2!1sen!2sus"
                                width="100%" height="400" frameborder="0" style="border:0;" allowfullscreen=""></iframe>
                        </div>
                    </div>
                </div>
                <div class="location-container w-clearfix">
                    <div class="column-left-small">
                        <div class="rich-text-block w-richtext">
                            <h2>Pawhuska</h2>
                            <p>101 E. 8th Street<br/>Pawhuska, OK 74056<br/>918.287.4111</p>
                            <p><strong>Lobby Hours</strong><br/>Mon-Fri: 9am-3pm</p>
                            <p><strong>Drive Thru Hours</strong><br/>Mon-Fri: 8am-5:30pm<br/>Sat: 9am-12pm</p>
                        </div>
                    </div>
                    <div class="column-right-map">
                        <div class="w-embed w-iframe">
                            <iframe
                                src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d800.0832994124866!2d-96.33869217071657!3d36.666488998748534!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x0!2zMzbCsDM5JzU5LjQiTiA5NsKwMjAnMTcuMyJX!5e0!3m2!1sen!2sus!4v1581711230719!5m2!1sen!2sus"
                                width="100%" height="400" frameborder="0" style="border:0;" allowfullscreen=""></iframe>
                        </div>
                    </div>
                </div>
                <div class="location-container w-clearfix">
                    <div class="column-left-small">
                        <div class="rich-text-block w-richtext">
                            <h2>Cleveland</h2>
                            <p>400 N. Broadway<br/>Cleveland, OK 74020<br/>918.358.5004</p>
                            <p><strong>Lobby Hours</strong><br/>Mon-Fri: 9am-4pm</p>
                            <p><strong>Drive Thru Hours</strong><br/>Mon-Fri: 8am-5:30pm<br/>Sat: 9am-12pm</p>
                        </div>
                    </div>
                    <div class="column-right-map">
                        <div class="w-embed w-iframe">
                            <iframe
                                src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d803.7487510238536!2d-96.46511717071695!3d36.312427998753314!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x0!2zMzbCsDE4JzQ0LjciTiA5NsKwMjcnNTIuNSJX!5e0!3m2!1sen!2sus!4v1581711264391!5m2!1sen!2sus"
                                width="100%" height="400" frameborder="0" style="border:0;" allowfullscreen=""></iframe>
                        </div>
                    </div>
                </div>
                <div class="location-container w-clearfix">
                    <div class="column-left-small">
                        <div class="rich-text-block w-richtext">
                            <h2>Cushing</h2>
                            <p>224 East Broadway<br/>Cushing, OK 74023<br/>918.225.2010</p>
                            <p><strong>Lobby Hours</strong><br/>Mon-Thu: 9am-3pm<br/>Fri: 9am-4pm</p>
                            <p><strong>Drive Thru Hours</strong><br/>Mon-Fri: 8am-5:30pm</p>
                        </div>
                    </div>
                    <div class="column-right-map">
                        <div class="w-embed w-iframe">
                            <iframe
                                src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d807.1612266091397!2d-96.77096517071732!3d35.98010599875789!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x0!2zMzXCsDU4JzQ4LjQiTiA5NsKwNDYnMTMuNSJX!5e0!3m2!1sen!2sus!4v1581711323846!5m2!1sen!2sus"
                                width="100%" height="400" frameborder="0" style="border:0;" allowfullscreen=""></iframe>
                        </div>
                    </div>
                </div>
                <div class="location-container w-clearfix">
                    <div class="column-left-small">
                        <div class="rich-text-block w-richtext">
                            <h2>Cushing</h2>
                            <p>2106 East Main Street<br/>Cushing, OK 74023<br/>918.225.2012</p>
                            <p><strong>Lobby Hours</strong><br/>Mon-Fri: 9am-4pm</p>
                            <p><strong>Drive Thru Hours</strong><br/>Mon-Fri: 8am-5:30pm<br/>Sat: 9am-12pm</p>
                        </div>
                    </div>
                    <div class="column-right-map">
                        <div class="w-embed w-iframe">
                            <iframe
                                src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d807.101226196873!2d-96.73963217071727!3d35.98597199875778!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x0!2zMzXCsDU5JzA5LjUiTiA5NsKwNDQnMjAuNyJX!5e0!3m2!1sen!2sus!4v1581711349696!5m2!1sen!2sus"
                                width="100%" height="400" frameborder="0" style="border:0;" allowfullscreen=""></iframe>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop
