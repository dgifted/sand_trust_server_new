@extends('layouts.landing-master')
@section('page-title', 'Personal Loans')
@section('page-meta')
@stop

@section('content')
    <div id="section1" class="hero-inside personal">
        <div class="hero-inside-container">
            <h1 class="h1-hero-inside">Personal Loans</h1>
        </div>
    </div>
    <div id="section2" class="section120-white">
        <div class="container1200 w-clearfix">
            <div class="column-right-content">
                <div class="box-blue">
                    <div class="margin20-bottom w-richtext">
                        <p><strong>Our promise of how {{config('app.name')}} Bank will work with you:</strong></p>
                    </div>
                    <ul class="list">
                        <li class="list-item">We will pick up the phone and answer your questions, no strings
                            attached.
                        </li>
                        <li class="list-item">We’ll work quickly to understand your financial situation before ever
                            suggesting ideas
                            and solutions.
                        </li>
                        <li class="list-item">We’ll devise a range of solutions that make sense for you and make sure
                            you clearly
                            understand the advantages and disadvantages of each option.
                        </li>
                        <li class="list-item">We will be straightforward with you about whether we are able to compete
                            with other
                            solutions you’ve shopped.
                        </li>
                        <li class="list-item">Even if you’ve eliminated {{config('app.name')}} Bank from your set of possibilities,
                            we’ll offer
                            our expert opinion on your other options.
                        </li>
                        <li class="list-item">Contact {{config('app.name')}} Bank by phone or email and one of our experts will get
                            back to you
                            within the next business day, if not sooner.
                        </li>
                    </ul>
                </div>
                <div class="rich-text-block w-richtext">
                    <figure style="max-width:970px" id="w-node-5fc17659f962-c3fcf6ef"
                            class="w-richtext-align-fullwidth w-richtext-figure-type-image">
                        <div><img
                                src="https://bluskyonlinebank.com/assets.website-files.com/5cf0968c612e876b4a49efc9/5d2619c403e410a6c63daaeb_Personal Loan.jpg"
                                alt=""/></div>
                    </figure>
                    <p>‍</p>
                    <p>Whether you are seeking a loan for home improvement, buying your child’s first car, or buying a
                        vacation
                        home, {{config('app.name')}} Bank will find the best personal loan solution for you. </p>
                    <p>A personal loan is money that is borrowed from a bank and paid back in fixed monthly payments,
                        typically
                        over a few years, along with interest. These interest rates will vary based on your credit
                        score.</p>
                    <p>The world of consumer lending is a complicated one. That’s why {{config('app.name')}} Bank works to help you
                        navigate
                        through the different loan offers, rates, and terms. </p>
                    <p><strong>Speak with one of our lenders today:</strong></p>
                    <ul>
                        <li>Visit any of our four branch locations during business hours whose addresses are listed at
                            the bottom of
                            the page.
                        </li>
                        <li>Call us at any of the four branch locations during business hours whose phone numbers are
                            listed at the
                            bottom of the page.
                        </li>
                        <li>Download and fill out the <a
                                href="https://assets.website-files.com/5cf0968c612e876b4a49efc9/5d68c0f1677c2a195cf1371d_Personal-Loan-app.pdf"
                                target="_blank" data-goodbye-skip="1">Personal Loan Application</a>.
                        </li>
                    </ul>
                    <h3>Vehicle Loans</h3>
                    <p>Want to hit the road in a new set of wheels? A car loan is secured against the new or used
                        vehicle you want
                        to buy, meaning the car itself serves as collateral for the loan. These loans typically
                        translate to lower
                        fixed rates.</p>
                    <p>Most car loans are fixed at 36 months, 48 months, or 60 months. The terms and interest rates of
                        these loans
                        are typically determined by the age of vehicle and the amount of the borrower’s down payment.
                        Interest rates
                        are typically fixed for the entirety of the loan repayment term.</p>
                    <h3>Recreational Loans</h3>
                    <p>Your next adventure starts here. Recreational loans are secured by the recreational vehicle you
                        wish to
                        buy, such as a motorcycle, camper, motor home, snowmobile, boat, or ATV (all-terrain vehicle).
                        {{config('app.name')}} Bank
                        offers competitive rates and flexible financing solutions.</p>
                    <h3>Mortgage Loans</h3>
                    <p>Buying a home is one of the biggest decisions and investments you’ll ever make. Mortgage loans
                        are secured
                        by the real estate itself and can be used to buy a home, build a new home, or refinance an
                        existing
                        mortgage. Your income, down payment amount, existing debts, and credit score will all factor
                        into your loan
                        options.</p>
                    <h3>Construction Financing</h3>
                    <p>Gain control of the home-building process with a construction loan. This loan works for anyone
                        investing
                        their time and money in construction or related expenses including homeowners, contractors, and
                        business
                        owners. Aside from the building itself, a construction loan can also be used to pay for building
                        equipment,
                        materials, and employees used for construction.</p>
                    <p>{{config('app.name')}} Bank’s experienced lending team will not only support and monitor the construction of
                        your dream
                        home, but will also provide access to long-term financing through our mortgage lending
                        partners.</p>
                    <h3>Vacation Home Loans</h3>
                    <p>Need to get away? Whether you’re thinking about buying a second home as an investment, a regular
                        getaway,
                        or a place to retire, owning a vacation spot can be a smart purchase. You can take out a
                        conventional
                        mortgage loan on the second home, or finance the vacation home using your current house’s equity
                        with a
                        HELOC (home equity line of credit).</p>
                    <p>{{config('app.name')}} Bank will work to help you choose the smartest way to finance your vacation home,
                        whether it sits
                        on a lake in Ottawa, along the beach, or nestled in the mountains.</p>
                    <h3>Personal/Unsecured Loans</h3>
                    <p>An unsecured loan is a personal loan that doesn’t require you to put up any collateral for the
                        loan. That
                        means if you default on the loan, there is no collateral to claim as compensation. The risk you
                        take on with
                        an unsecured loan is to your personal credit score.</p>
                    <p>Unsecured personal loans typically have higher annual percentage rates than secured loans. Your
                        credit
                        report will serve as an important factor in your approval for an unsecured loan.</p>
                    <h3>Cash-Secured Loans</h3>
                    <p>Bump up your credit score while protecting the money in your bank account with a cash-secured
                        loan. These
                        are loans secured by CDs (certificates of deposit) or marketable securities.</p>
                    <p>Cash-secured loans essentially allow you to borrow against your own savings in the bank. Because
                        your money
                        is readily available, this loan is a minimal risk and easier to get approved for. Those who
                        cannot qualify
                        for other types of loans, such as unsecured personal loans or credit cards, might consider
                        cash-secured
                        loans. Rates for cash-secured loans are typically lower than they are for unsecured loans.</p>
                    <p>Cash-secured loans can be used for any purpose. It can come in the form of a lump sum, line of
                        credit, or a
                        cash-secured credit card.</p>
                    <h3>Apply for a Personal Loan</h3>
                    <p>Please click to print the <a
                            href="https://assets.website-files.com/5cf0968c612e876b4a49efc9/5d68c0f1677c2a195cf1371d_Personal-Loan-app.pdf"
                            target="_blank" data-goodbye-skip="1">Personal Loan Application</a> for your use and deliver
                        or mail it to
                        the bank. <em>Please do not use this application for real estate-secured requests. We cannot
                            prevent
                            unauthorized interception of information transmitted by you over the internet. You should
                            not email any
                            personal or financial information to the bank through the internet, including this loan
                            application, your
                            account number, or your social security number.</em></p>
                </div>
            </div>
            <div class="column-left-nav">
                @include('landing.personal.mini-nav')
            </div>
        </div>
    </div>
    <div id="section2" class="section bg-gradient">
        <div class="container-8900">
            <div class="form-contact w-form">
                @include('includes.landing.contact-form')
            </div>
        </div>
    </div>
@stop
