@extends('layouts.landing-master')
@section('page-title', 'CDARS')
@section('page-meta')
@stop

@section('content')
    <div id="section1" class="hero-inside personal">
        <div class="hero-inside-container">
            <h1 class="h1-hero-inside">CDARS</h1>
        </div>
    </div>
    <div id="section2" class="section120-white">
        <div class="container1200 w-clearfix">
            <div class="column-right-content">
                <div class="rich-text-block w-richtext">
                    <figure style="max-width:970px" id="w-node-3628fe5616a1-0c09cd7c"
                            class="w-richtext-align-fullwidth w-richtext-figure-type-image">
                        <div><img
                                src="https://bluskyonlinebank.com/assets.website-files.com/5cf0968c612e876b4a49efc9/5d26195b69609cfff28e4e75_CDARS Personal.jpg"
                                alt=""/></div>
                    </figure>
                    <p>‍</p>
                    <p>CDARS is a safe, convenient service through which we can place your large CD deposits into
                        smaller-denomination CDs at multiple institutions.</p>
                    <p>‍</p>
                    <h2><strong>CDARS could be right for you if you:</strong></h2>
                    <ul>
                        <li>Have significant FDIC insurance needs.</li>
                        <li>Want to simplify management of your CD accounts.</li>
                    </ul>
                    <p>‍</p>
                    <h2><strong>Safe</strong></h2>
                    <p><strong>‍</strong>Deposits are held with other CDARS Network members, each an FDIC-insured
                        institution, so
                        deposits are eligible for FDIC insurance at each member bank.</p>
                    <p>‍</p>
                    <h2><strong>Attractive</strong></h2>
                    <p><strong>‍</strong>Rates on CDs may compare favorably with those of other high-quality,
                        fixed-income
                        investments, such as Treasury securities.</p>
                    <p>‍</p>
                    <h2><strong>Convenient</strong></h2>
                    <p><strong>‍</strong>You can manage all your CD investments through one relationship.</p>
                    <p>‍</p>
                    <h2><strong>Confidential</strong></h2>
                    <p><strong>‍</strong>Your CDs with other banks are identified only by a tracking number and your
                        personal
                        information remains confidential.</p>
                    <p>‍</p>
                    <h2><strong>How CDARS works:</strong></h2>
                    <ol>
                        <li><strong>Submit an order</strong><br/>We submit an order to request placement of your money.
                        </li>
                        <li><strong>CDs issued through the CDARS system</strong><br/>Your money is placed into CDs with
                            denominations under $250,000, which are issued by other banks. You can identify banks where
                            you do not
                            wish your funds to be placed, and you can also review a list of banks where CDARS proposes
                            to place your
                            deposits. This allows you to ensure your entire deposit is fully insured.
                        </li>
                        <li><strong>Confirmation of deposit</strong><br/>When your CDs are issued, we will send you a
                            notice
                            confirming your deposit.
                        </li>
                        <li><strong>Interest paid directly to you</strong><br/>Interest is paid directly to you or a
                            designated
                            account
                        </li>
                        <li><strong>Monthly Statement</strong><br/>Each month, we send you a statement listing all the
                            banks
                            issuing the CDs, maturity dates, interest earned and other details.
                        </li>
                        <li><strong>Annual 1099 tax summary</strong><br/>At the end of each year, we send you a
                            consolidated 1099
                            tax summary that reports your taxable interest income.<br/>Please click on the CDARS website
                            link at
                            <a>cdar/a> for further information.</li>
                    </ol>
                </div>
            </div>
            <div class="column-left-nav">
                @include('landing.personal.mini-nav')
            </div>
        </div>
    </div>
    <div id="section2" class="section bg-gradient">
        <div class="container-8900">
            <div class="form-contact w-form">
                @include('includes.landing.contact-form')
            </div>
        </div>
    </div>
@stop
