@extends('layouts.landing-master')
@section('page-title', 'Personal Checking')
@section('page-meta')
@stop

@section('content')
    <div id="section1" class="hero-inside personal">
        <div class="hero-inside-container">
            <h1 class="h1-hero-inside">Personal Checking</h1>
        </div>
    </div>
    <div id="section2" class="section120-white">
        <div class="container1200 w-clearfix">
            <div class="column-right-content">
                <div class="rich-text-block w-richtext">
                    <figure style="max-width:970px" id="w-node-2cabeabdc2e9-e144937c"
                            class="w-richtext-align-fullwidth w-richtext-figure-type-image">
                        <div><img
                                src="https://bluskyonlinebank.com/assets.website-files.com/5cf0968c612e876b4a49efc9/5d2618ce69609c72518e4c41_Personal Checking.jpg"
                                alt=""/></div>
                    </figure>
                    <p>‍</p>
                    <p><strong>Regular Checking</strong></p>
                    <ul>
                        <li>$100.00 minimum deposit to open</li>
                        <li>Unlimited Check writing</li>
                        <li>Free ATM/Debit card</li>
                        <li>Free online banking</li>
                        <li>Mobile banking with check deposit</li>
                        <li>$5.00 Monthly service charge*<br/>*Service charge is not assessed if you sign up for
                            eStatements or if
                            your balance does not fall under $500.00.
                        </li>
                    </ul>
                    <p>‍</p>
                    <p><strong>Now Checking</strong></p>
                    <ul>
                        <li>$1,500.00 minimum deposit to open</li>
                        <li>Unlimited Check writing</li>
                        <li>Free ATM/Debit Card</li>
                        <li>Free online banking</li>
                        <li>Mobile banking with check deposit</li>
                        <li>Tiered Interest Rate is subject to change daily</li>
                        <li>Interest is calculated on the daily balance, based on the posted rate, and paid monthly.
                        </li>
                    </ul>
                    <p>‍</p>
                    <p>If the balance in the account falls below $1,500.00, a $10.00 service charge for that month will
                        be
                        assessed. Additionally, the account will not earn interest for any day the balance is below
                        $1,500.00.</p>
                    <p>All business related NOW’s are subject to per item fees as shown in the Banks Commercial Demand
                        Deposit
                        Account plus an $8.00 monthly service charge.</p>
                    <p>‍</p>
                    <p><strong>Lifestyles VIP Checking</strong></p>
                    <ul>
                        <li>$15,000.00 minimum deposit to open</li>
                        <li>Free {{config('app.name')}} Bank checks</li>
                        <li>Account would earn interest at the Now rate + .15 basis points. Interest is paid on daily
                            balance
                            method. If the balance in the account falls below $15,000.00, a $5.00 service charge, for
                            that month will
                            be assessed. Additionally, the account will not earn interest for any day the balance is
                            below $1,500.00.
                        </li>
                    </ul>
                </div>
            </div>
            <div class="column-left-nav">
                @include('landing.personal.mini-nav')
            </div>
        </div>
    </div>
    <div id="section2" class="section bg-gradient">
        <div class="container-8900">
            <div class="form-contact w-form">
                @include('includes.landing.contact-form')
            </div>
        </div>
    </div>
@stop
