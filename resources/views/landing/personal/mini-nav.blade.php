@php
$route = \Route::currentRouteName();
@endphp

<a href="{{route('personal-checking')}}" class="nav-link-sidebar {{$route == 'personal-checking' ? 'w--current' : ''}}">Personal Checking</a>
<a href="{{route('personal-savings')}}" class="nav-link-sidebar {{$route == 'personal-savings' ? 'w--current' : ''}}">Savings and Investments</a>
<a href="{{route('personal-cdars')}}" class="nav-link-sidebar {{$route == 'personal-cdars' ? 'w--current' : ''}}">CDARS</a>
<a href="{{route('personal-credit-cards')}}" class="nav-link-sidebar {{$route == 'personal-credit-cards' ? 'w--current' : ''}}">Personal Credit Card</a>
<a href="{{route('personal-loans')}}" class="nav-link-sidebar {{$route == 'personal-loans' ? 'w--current' : ''}}">Personal Loans</a>
<a href="{{route('personal-online-services')}}" class="nav-link-sidebar {{$route == 'personal-online-services' ? 'w--current' : ''}}">Online Services</a>
<a href="{{route('personal-other-services')}}" class="nav-link-sidebar {{$route == 'personal-other-services' ? 'w--current' : ''}}">Other Services</a>
