@php
$route = \Route::currentRouteName()
@endphp

<a href="{{route('business-checking')}}" class="nav-link-sidebar {{$route == 'business-checking' ? 'w--current' : ''}}">Business Checking</a>
<a href="{{route('business-savings')}}" class="nav-link-sidebar {{$route == 'business-savings' ? 'w--current' : ''}}">
    Business Savings and Investments
</a>
<a href="{{route('business-treasury')}}" class="nav-link-sidebar {{$route == 'business-treasury' ? 'w--current' : ''}}">Treasury Management</a>
<a href="{{route('business-cdars')}}" class="nav-link-sidebar {{$route == 'business-cdars' ? 'w--current' : ''}}">CDARS</a>
<a href="{{route('business-credit-cards')}}" class="nav-link-sidebar {{$route == 'business-credit-cards' ? 'w--current' : ''}}">Business Credit Cards</a>
<a href="{{route('business-loans')}}" class="nav-link-sidebar {{$route == 'business-loans' ? 'w--current' : ''}}">Business Loans</a>
<a href="{{route('business-online-banking')}}" class="nav-link-sidebar {{$route == 'business-online-banking' ? 'w--current' : ''}}">Online Banking</a>
<a href="{{route('business-other-services')}}" class="nav-link-sidebar {{$route == 'business-other-services' ? 'w--current' : ''}}">
    Other Business Services
</a>
