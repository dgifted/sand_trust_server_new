@extends('layouts.landing-master')
@section('page-title', 'Online Banking')
@section('page-meta')
@stop

@section('content')
    <div id="section1" class="hero-inside">
        <div class="hero-inside-container">
            <h1 class="h1-hero-inside"><strong>Online Business Banking with Cash Management</strong></h1>
        </div>
    </div>
    <div id="section2" class="section120-white">
        <div class="container1200 w-clearfix">
            <div class="column-left-nav">
                @include('landing.business.mini-nav')
            </div>
            <div class="column-right-content">
                <div class="box-blue">
                    <div class="margin20-bottom w-richtext">
                        <p><strong>Our promise of how {{config('app.name')}} Bank will work with you:</strong></p>
                    </div>
                    <ul class="list">
                        <li class="list-item">We will pick up the phone and answer your questions, no strings
                            attached.
                        </li>
                        <li class="list-item">We’ll work quickly to understand your financial situation before ever
                            suggesting ideas
                            and solutions.
                        </li>
                        <li class="list-item">We’ll devise a range of solutions that make sense for you and make sure
                            you clearly
                            understand the advantages and disadvantages of each option.
                        </li>
                        <li class="list-item">We will be straightforward with you about whether we are able to compete
                            with other
                            solutions you’ve shopped.
                        </li>
                        <li class="list-item">Even if you’ve eliminated {{config('app.name')}} Bank from your set of possibilities,
                            we’ll offer
                            our expert opinion on your other options.
                        </li>
                        <li class="list-item">Contact {{config('app.name')}} Bank by phone or email and one of our experts will get
                            back to you
                            within the next business day, if not sooner.
                        </li>
                    </ul>
                </div>
                <div class="rich-text-block w-richtext">
                    <figure style="max-width:970px" id="w-node-a55aec52aa1d-21ed6b26"
                            class="w-richtext-align-fullwidth w-richtext-figure-type-image">
                        <div><img
                                src="https://bluskyonlinebank.com/assets.website-files.com/5cf0968c612e876b4a49efc9/5d26073d6c9788a3f98563bb_Online Banking.jpg"
                                alt=""/></div>
                    </figure>
                    <p>‍</p>
                    <p>Your business is moving quickly, and we want to make it easy and convenient for you to stay
                        ahead. With
                        {{config('app.name')}} Bank’s Cash Management tool, you can access important business banking account
                        information right
                        from your computer. The Cash Management online platform provides a variety of banking tools,
                        allowing you to
                        handle business banking from your home or office at any time, day or night.</p>
                    <p>‍</p>
                    <h2>Features of Cash Management</h2>
                    <p>The Cash Management allows you to: </p>
                    <ul>
                        <li>View your transaction history </li>
                        <li>View inclearing checks</li>
                        <li>Transfer money between your {{config('app.name')}} Bank business accounts</li>
                        <li>Make payments on your {{config('app.name')}} Bank loan</li>
                        <li>View and download your monthly bank statements</li>
                        <li>Direct Deposit Payroll - eliminate payroll checks by entering and submitting payrolls online
                            using
                            electronic ACH (Automated Clearing House) transactions
                        </li>
                        <li>Direct Payments - make payments to vendors or collect payments from your clients using
                            electronic ACH
                            transactions
                        </li>
                        <li>Secure Access 24/7</li>
                    </ul>
                    <p>‍</p>
                    <h2>Your security is our priority</h2>
                    <p>Keeping your business’s financial information isn’t just important to {{config('app.name')}} Bank — it’s
                        absolutely
                        necessary. </p>
                    <p>Cash Management utilizes the highest security standards in the industry, allowing you to manage
                        your
                        business banking in a convenient, secure way. Security features include: </p>
                    <p><strong>Advanced encryption</strong></p>
                    <ul>
                        <li>Sophisticated encryption methods allow for reliable, safe, and secure transmission of your
                            business’s
                            most sensitive data.
                        </li>
                    </ul>
                    <p><strong>Multi-factor authentication</strong></p>
                    <ul>
                        <li>Layered security requires users to prove their identities using multiple verification
                            methods, providing
                            several barriers against fraud.
                        </li>
                    </ul>
                    <p><strong>Dual control</strong></p>
                    <ul>
                        <li>This feature reduces the risk of fraud by requiring two people to complete a single wire
                            transfer or ACH
                            batch transaction. Dual Control prevents a single user from creating or initiating an ACH
                            batch
                            transaction or wire transfer. One user is able to do a single action, but not both within
                            the same
                            transaction. 
                        </li>
                        <li>Dual Control also decreases the chance for errors because it requires a second user to
                            review any ACH
                            transactions or wire transfers before confirmation. Multiple users can be authorized to
                            approve
                            transactions.
                        </li>
                    </ul>
                    <p><strong>Time restrict</strong></p>
                    <ul>
                        <li>This feature allows administrators to limit the days and time frames of access to the online
                            banking
                            portal. This added security layer prevents access outside of set business hours. Different
                            time
                            restrictions can be customized and assigned to specific users.
                        </li>
                    </ul>
                    <p><strong>IP restrict</strong></p>
                    <ul>
                        <li>Access to accounts can be limited to certain IP addresses, such as the addresses belonging
                            to computers
                            at your business. This prevents anyone outside the IP network from accessing the online
                            banking tool.
                        </li>
                    </ul>
                    <p>Contact us to learn more about our additional layered security options.</p>
                    <p>‍</p>
                    <h2>Manage your business banking 24/7 with Bank-By-Phone</h2>
                    <p>If you’re not near a computer, you can still access your accounts and manage your money. Simply
                        call our
                        Bank-By-Phone line, (877) 970-2265.</p>
                    <p>Banking by phone is free, secure, and available at all hours of the day. Call the line and follow
                        the
                        instructions outlined in the voicemail message.</p>
                    <p>Bank-By-Phone allows you to:</p>
                    <ul>
                        <li>Check your balances</li>
                        <li>Review recent transactions</li>
                        <li>Check if a check has cleared</li>
                        <li>Transfer funds</li>
                        <li>Make loan payments</li>
                    </ul>
                    <p>‍</p>
                    <h2>Wire Transfers</h2>
                    <p>Wire transfers are electronic transfers of money. They’re safe, reliable, and fast. When you need
                        to send
                        or receive money almost immediately, wire transfers may be the right option. They allow you to
                        send money
                        quickly and cost-efficiently across the country or around the globe without having to send a
                        check, money
                        order, or cash.</p>
                    <p>Wire transfers are also one of the most secure ways to transfer money. They can be especially
                        beneficial to
                        businesses because once a wire payment is received in the business’s account, that business can
                        be certain
                        that payment has been collected and cannot be revoked or recalled. Wire transfers do not take
                        several days
                        to clear, unlike checks. A check may appear to have been deposited and cleared, but if a check
                        is found to
                        be improperly issued or presented, the depositor of the check may have the funds reversed.
                        Checks also carry
                        the risk of fraud, or a check “bouncing” for lack of funds. </p>
                    <p>With a wire transfer, the sender must have the actual funds present in their account in order to
                        initiate
                        the wire transfer. This way, when the wire is received, the recipient can be certain that the
                        funds have
                        actually been deposited into their account.</p>
                    <p>{{config('app.name')}} Bank originates and accepts both domestic and international wire transfers. This can be
                        done
                        online and at any branch location.</p>
                    <h3>Wire Transfer Fees:</h3>
                    <ul>
                        <li>Incoming Wire Domestic – $5</li>
                        <li>Outgoing Wire Domestic – $17</li>
                        <li>Incoming Wire International – $35</li>
                        <li>Outgoing Wire International – $50</li>
                    </ul>
                    <p>Contact us or stop by any one of our branch locations for a complete price list on all products
                        and
                        services.</p>
                    <h3>Wire Cutoff Times</h3>
                    <p>Domestic wire: 3:30 p.m. CT for same-business-day transfer </p>
                    <p>International wire: 1:30 p.m. CT for same-business-day transfer </p>
                    <p>Any wire transfer instructions received after these cutoff times will be processed the following
                        business
                        day.</p>
                    <h3>Incoming Wire Instructions</h3>
                    <p>A bank wire consists of instructions about who will be receiving the money. This includes the
                        recipient’s
                        bank account number and the amount he or she should receive. </p>
                    <p>To avoid processing delays, please use the following routing instructions for incoming wire
                        transfers to
                        {{config('app.name')}} Bank.</p>
                    <p><strong>Wire to:</strong><br/>{{config('app.name')}} Bank<br/>101 East 8th Street<br/>Pawhuska, OK 74056<br/>Routing
                        #103101877</p>
                    <p>‍<br/><strong>For Credit to:</strong><br/>Account Name<br/>Account Address<br/>Account Number</p>
                    <p>‍</p>
                    <h2>ACH Origination</h2>
                    <p>With {{config('app.name')}} Bank’s ACH (Automated Clearing House) origination solution, you can easily and
                        securely
                        collect or transfer funds online. </p>
                    <p>ACH is the most cost-effective method for making payments. You can use ACH to make a variety of
                        electronic
                        payments, such as direct deposit payroll, vendor payments, and tax payments. With {{config('app.name')}}
                        Bank’s ACH
                        solutions, you can automate and expedite collection of your receivables.</p>
                    <p>ACH origination is provided through {{config('app.name')}} Bank’s online banking platform and provides a
                        variety of
                        benefits and multiple payment types. Using this feature allows you to:</p>
                    <ul>
                        <li>Reduce risk of fraud and unauthorized access, as multiple levels of security can be used
                            when creating
                            and originating ACH files that provide internal controls and audits
                        </li>
                        <li>Streamline your accounts payable and receivable while improving cash flow forecasting by
                            reducing checks
                            and potential check fraud
                        </li>
                        <li>Spend less time reconciling by reducing check volume, making monthly reconciling fast and
                            easy
                        </li>
                    </ul>
                    <p>{{config('app.name')}} Bank’s ACH origination offers several benefits designed to make business banking more
                        convenient
                        and simple:</p>
                    <ul>
                        <li><strong>Automatic transactions.</strong> Schedule automatic generation of transactions for
                            your review
                            and release. Initiate and release batches from anywhere you have Internet access,
                            eliminating trips to the
                            bank.
                        </li>
                        <li><strong>Transaction database.</strong> Create and maintain your transaction database,
                            allowing you to
                            keep your information organized and on file. You can view and/or print all of your past ACH
                            activity,
                            including audit reports.
                        </li>
                        <li><strong>Recurring payments.</strong> Deposit recurring payments for payroll direct deposit
                            and vendor
                            payments, reducing accounting errors and providing your employees and vendors quick access
                            to funds.
                        </li>
                        <li><strong>Recurring collections.</strong> Execute recurring billings or collections from your
                            customers,
                            such as insurance premiums, monthly service fees, rent or lease payments, membership dues,
                            or charitable
                            contributions. 
                        </li>
                        <li><strong>Consolidating funds.</strong> Consolidate funds from remote bank accounts into one
                            main
                            operating account at {{config('app.name')}} Bank.
                        </li>
                        <li><strong>Quick processing.</strong> Same-day ACH processing is available at {{config('app.name')}} Bank.
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <div id="section2" class="section bg-gradient">
        <div class="container-8900">
            <div class="form-contact w-form">
                @include('includes.landing.contact-form')
            </div>
        </div>
    </div>
@stop
