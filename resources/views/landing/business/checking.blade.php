@extends('layouts.landing-master')
@section('page-title', 'Business Checking')
@section('page-meta')
@stop

@section('content')
    <div id="section1" class="hero-inside">
        <div class="hero-inside-container">
            <h1 class="h1-hero-inside">{{config('app.name')}} Bank Business Checking Accounts</h1>
        </div>
    </div>


    <div id="section2" class="section120-white">
        <div class="container1200 w-clearfix">
            <div class="column-left-nav">
                @include('landing.business.mini-nav')
            </div>
            <div class="column-right-content">
                <div class="box-blue">
                    <div class="margin20-bottom w-richtext">
                        <p><strong>Our promise of how {{config('app.name')}} Bank will work with you:</strong></p>
                    </div>
                    <ul class="list">
                        <li class="list-item">We will pick up the phone and answer your questions, no strings
                            attached.
                        </li>
                        <li class="list-item">We’ll work quickly to understand your financial situation before ever
                            suggesting ideas
                            and solutions.
                        </li>
                        <li class="list-item">We’ll devise a range of solutions that make sense for you and make sure
                            you clearly
                            understand the advantages and disadvantages of each option.
                        </li>
                        <li class="list-item">We will be straightforward with you about whether we are able to compete
                            with other
                            solutions you’ve shopped.
                        </li>
                        <li class="list-item">Even if you’ve eliminated {{config('app.name')}} Bank from your set of possibilities,
                            we’ll offer
                            our expert opinion on your other options.
                        </li>
                        <li class="list-item">Contact {{config('app.name')}} Bank by phone or email and one of our experts will get
                            back to you
                            within the next business day, if not sooner.
                        </li>
                    </ul>
                </div>
                <div class="rich-text-block w-richtext">
                    <figure style="max-width:970px" id="w-node-22c68c54e6bc-374c6770"
                            class="w-richtext-align-fullwidth w-richtext-figure-type-image">
                        <div><img
                                src="https://bluskyonlinebank.com/assets.website-files.com/5cf0968c612e876b4a49efc9/5d2602bb0574eedc33766eba_Business Checking.jpg"
                                alt=""/></div>
                    </figure>
                    <p>‍</p>
                    <p>Wherever you are in your business journey, it’s time to settle into your financial home at Blue
                        Sky Bank.
                        We think of ourselves as your banking consultants rather than a place that merely helps you
                        monitor your
                        cash flow.</p>
                    <p>As a community bank, we’ve worked with all kinds of businesses with varied and specialized needs
                        in many
                        different industries. We’ve been in the banking business in Ottawa since 1905. Our bankers,
                        who have a
                        deep understanding of the Ottawa business market and expertise in your industry, will approach
                        your
                        business with the same responsiveness and attentiveness we’ve been serving other regional
                        businesses for
                        decades. The first step to this partnership and meeting your business&#x27;s goals is to figure
                        out if we
                        are a fit. Before we ask for a deposit, a loan, or what we can sell you, we want to meet
                        you.</p>
                    <p>Call us to arrange a time to come into one of our four branches and see for yourself.</p>
                    <p>‍</p>
                    <h2>Small Business Checking Account</h2>
                    <p>A business checking account establishes a critical division between your personal finances and
                        your
                        business finances. This account will protect you financially, legally, and secure your business
                        for the
                        future.</p>
                    <p>Mixing your business and personal finances can result in serious implications should a lawsuit
                        arise. Any
                        checks written out to your business will be deposited in your business checking account. At the
                        same time,
                        you’ll be able to write out checks from this account to your vendors, suppliers, and anyone else
                        you do
                        basic business with — eliminating the need for you to make these transactions from your personal
                        account. If
                        your business is ever involved in legal situations for an accident or breach of contract, this
                        account will
                        protect your personal assets.</p>
                    <p>Business owners will also benefit from a business checking account when it comes to managing
                        taxes. Having
                        a detailed track record of your business expenses makes the process of adding up deductions much
                        easier. As
                        a business owner, your time is valuable and should be spent on growing and optimizing your
                        company. The less
                        time having to sort through documents and piles of receipts, trying to remember if the purchases
                        were made
                        personally or from your business account, the better. Determining your profits and taxable
                        income are
                        crucial to get right for tax purposes.</p>
                    <p>Lastly, having a business checking account builds your business’s credibility. This account also
                        serves as
                        a pathway toward qualifying for a business loan, should you ever need one in the future. It is
                        much harder
                        to secure a loan if your business does not have a history of making payments on time to your
                        vendors. And
                        without loans, growing your business becomes much more difficult.</p>
                    <p>‍<br/></p>
                    <h3>Opening a {{config('app.name')}} Bank Small Business Checking Account</h3>
                    <p>When you open an account with us you’ll have full access to our banking products and services.
                        We’ll
                        provide you with a business debit card and business credit card options to make transactions
                        faster and
                        easier. Business moves quickly, and we know you need quick and convenient access to your account
                        regardless
                        of where you are.</p>
                    <p>To open a {{config('app.name')}} Bank Small Business Checking Account:</p>
                    <ul>
                        <li>Fill out and submit an application</li>
                        <li>A minimum deposit of $100 is required</li>
                        <li>Your first 100 debit or credits are free</li>
                        <li>Each additional debit or credit is charged $0.25</li>
                    </ul>
                    <p>‍</p>
                    <h2>Commercial Checking Account</h2>
                    <p>A commercial account is a checking account for businesses or corporations. This type of account
                        is a great
                        option for businesses with higher volumes transactions. Do you have more than 200 transactions
                        each month?
                        It’s likely you need a commercial checking account in order to take full advantage of the
                        services that will
                        support your fast paced enterprise.</p>
                    <p>All of your corporation’s cash management needs are cared for in one place, resulting in greater
                        efficiencies for receivables, payables, and cash on hand. You’ll also enjoy the convenience of
                        our robust
                        online banking platform, expanded mobile banking benefits, and a direct relationship with our
                        Treasury
                        Officers.</p>
                    <p>The moment you open up an account, you can get started using our online business banking service,
                        <a
                            href="online-banking.html">Cash Management</a>, as well as our mobile banking app. Through
                        these services,
                        you can monitor transactions, make payments towards your {{config('app.name')}} Bank business loan, and view
                        your
                        statement cycles.</p>
                    <h3>Opening a {{config('app.name')}} Bank Commercial Checking Account</h3>
                    <p>Contact us today to open a {{config('app.name')}} Bank Business Checking Account or Commercial Checking
                        Account. These
                        accounts can be customized as needed to fit the needs of your company. We’ll meet with you and
                        learn about
                        your business banking needs. You may choose to first fill out a Commercial Checking Account
                        application.</p>
                    <p>If you have any questions about our business banking products and services, give us a call or
                        stop by any
                        of our branch locations.<br/></p>
                </div>
            </div>
        </div>
    </div>
    <div id="section2" class="section bg-gradient">
        <div class="container-8900">
            <div class="form-contact w-form">
                @include('includes.landing.contact-form')
            </div>
        </div>
    </div>
@stop
