@extends('layouts.landing-master')
@section('page-title', 'Treasury Management')
@section('page-meta')
@stop

@section('content')
    <div id="section1" class="hero-inside">
        <div class="hero-inside-container">
            <h1 class="h1-hero-inside">{{config('app.name')}} Bank’s Treasury Management Services and Offerings</h1>
        </div>
    </div>
    <div id="section2" class="section120-white">
        <div class="container1200 w-clearfix">
            <div class="column-left-nav">
                @include('landing.business.mini-nav')
            </div>

            <div class="column-right-content">
                <div class="box-blue">
                    <div class="margin20-bottom w-richtext">
                        <p><strong>Our promise of how {{config('app.name')}} Bank will work with you:</strong></p>
                    </div>
                    <ul class="list">
                        <li class="list-item">We will pick up the phone and answer your questions, no strings
                            attached.
                        </li>
                        <li class="list-item">We’ll work quickly to understand your financial situation before ever
                            suggesting ideas
                            and solutions.
                        </li>
                        <li class="list-item">We’ll devise a range of solutions that make sense for you and make sure
                            you clearly
                            understand the advantages and disadvantages of each option.
                        </li>
                        <li class="list-item">We will be straightforward with you about whether we are able to compete
                            with other
                            solutions you’ve shopped.
                        </li>
                        <li class="list-item">Even if you’ve eliminated {{config('app.name')}} Bank from your set of possibilities,
                            we’ll offer
                            our expert opinion on your other options.
                        </li>
                        <li class="list-item">Contact {{config('app.name')}} Bank by phone or email and one of our experts will get
                            back to you
                            within the next business day, if not sooner.
                        </li>
                    </ul>
                </div>
                <div class="rich-text-block w-richtext">
                    <figure style="max-width:970px" id="w-node-60a7ac24a864-e38cf622"
                            class="w-richtext-align-fullwidth w-richtext-figure-type-image">
                        <div><img
                                src="https://bluskyonlinebank.com/assets.website-files.com/5cf0968c612e876b4a49efc9/5d260397528d2e4279bf8d86_Treasury Management.jpg"
                                alt=""/></div>
                    </figure>
                    <p>‍</p>
                    <p>Whether your business is new or a long-standing staple in the region, it needs superior
                        protection. Arming
                        your business against fraud and managing your business’s working capital are two of the ways
                        {{config('app.name')}} Bank
                        helps its customers grow their businesses with confidence. Our treasury management systems (TMS)
                        and
                        professionals will work with you to find the most innovative solutions for your day-to-day
                        banking needs,
                        from financial risk management to finding the most creative ways to collect your money
                        quicker.</p>
                    <p>We know you are busy running your business. That’s why {{config('app.name')}} Bank has thoughtfully crafted
                        products and
                        services offerings that will help you reach your business goals.</p>
                    <h3><strong>Remote Deposit Capture</strong></h3>
                    <p>We know you’re on-the-go and don’t always have time to drop in at one of our branches. We believe
                        convenience is a must, which is why we’ve included {{config('app.name')}} Bank’s Remote Deposit Capture in our
                        line-up of
                        convenient services. Our customers can quickly deposit and process their personal checks,
                        cashier’s checks,
                        money orders, convenience checks, and corporate checks.</p>
                    <p>These deposits can be made from a smartphone and other scanner options.  Because Remote Deposit
                        Capture is
                        based online, there’s no need to purchase, install, or maintain any software. Don’t worry about
                        getting this
                        system up and running or deployed to the rest of your business. Our team will set up Remote
                        Deposit Capture
                        for your business and guide you through the entire operations process. We’ll even train your
                        employees on
                        the service.</p>
                    <h3><strong>Merchant Services</strong></h3>
                    <p>The key to growing your business is having access to multiple payment options. {{config('app.name')}} Bank
                        backs this
                        belief by accommodating customers who are opting out of conventional payment processing systems.
                        {{config('app.name')}}
                        Bank’s Merchant Processing provides our customers with advanced credit and debit card processing
                        services
                        for any way they decide to collect payment, whether it’s through a terminal, computer, phone, or
                        even a
                        tablet.</p>
                    <p>{{config('app.name')}} Bank’s merchant services team will install the processing equipment for you,
                        troubleshoot it, and
                        provide hands-on training for everyone on your team. If your merchant equipment ever needs
                        repairing, we’ll
                        supply you with a loaner while we work to get your equipment back up and running. We’ll work as
                        hard as we
                        can to make sure your business never wastes time or loses money because of a technical
                        problem.</p>
                    <p>To grow your business wisely, you need to make purchases that make the most sense. We’ll work
                        closely with
                        you to find the best merchant processing option at a comfortable price point. If you are already
                        processing
                        card transactions with someone else, we’d love to see if we can help you save money. Email your
                        current
                        processing statement to <a href="mailto:">onlinesupport@bluesky.com</a> and we’ll quickly get
                        back to you
                        with how we can help.</p>
                    <h3><strong>Positive Pay</strong></h3>
                    <p>Our team is constantly working to keep our customers safe from financial and reputational risk.
                        We know
                        even one counterfeit check incident can have devastating implications for your business. Blue
                        Sky Bank’s
                        Positive Pay supplies our customers with a protective cash management system and a powerful
                        fraudulent
                        detection tool. It quickly identifies and prevents fraudulent check payments that are becoming
                        increasingly
                        difficult to catch.</p>
                    <p>With Positive Pay, the check number, dollar amount, and account number fields are screened and
                        matched. If
                        the numbers don’t match the issuing company’s master list, we won’t clear the check. You will be
                        notified
                        immediately and we’ll hold the check until you decide to accept or reject it. These reports are
                        available in
                        an online format.</p>
                    <p>We also offer traditional and printed bar-code positive pay options. The bar-code option is a
                        secure seal,
                        laser-printed, barcode technology. It’s another affordable layer of protection to stop check
                        fraud.</p>
                    <p>Learn more about check fraud and the Secure Seal Barcode <a
                            href="https://assets.website-files.com/5cf0968c612e876b4a49efc9/5d68c0f18dfed99096598f38_Positive-Pay.pdf"
                            target="_blank" data-goodbye-skip="1">here</a>.</p>
                    <h3><strong>Local Customer Service</strong></h3>
                    <p>{{config('app.name')}} Bank’s customers are diverse and their banking needs require different approaches and
                        solutions.
                        Your business is no exception. With {{config('app.name')}} Bank, you will always have someone to turn to for
                        help, advice,
                        or guidance on cash management decisions. Our team of treasury management professionals are
                        committed to
                        helping complete your business objectives so you can be a more confident business owner, free of
                        worry.
                        Let’s get started finding the right, personalized {{config('app.name')}} Bank-backed solutions for your
                        business.<br/>
                    </p>
                </div>
            </div>
        </div>
    </div>
    <div id="section2" class="section bg-gradient">
        <div class="container-8900">
            <div class="form-contact w-form">
                @include('includes.landing.contact-form')
            </div>
        </div>
    </div>
@stop
