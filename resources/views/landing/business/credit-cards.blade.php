@extends('layouts.landing-master')
@section('page-title', 'Business Credit Cards')
@section('page-meta')
@stop

@section('content')
    <div id="section1" class="hero-inside">
        <div class="hero-inside-container">
            <h1 class="h1-hero-inside">{{config('app.name')}} Bank Business Credit Cards</h1>
        </div>
    </div>
    <div id="section2" class="section120-white">
        <div class="container1200 w-clearfix">
            <div class="column-left-nav">
                @include('landing.business.mini-nav')
            </div>
            <div class="column-right-content">
                <div class="box-blue">
                    <div class="margin20-bottom w-richtext">
                        <p><strong>Our promise of how {{config('app.name')}} Bank will work with you:</strong></p>
                    </div>
                    <ul class="list">
                        <li class="list-item">We will pick up the phone and answer your questions, no strings
                            attached.
                        </li>
                        <li class="list-item">We’ll work quickly to understand your financial situation before ever
                            suggesting ideas
                            and solutions.
                        </li>
                        <li class="list-item">We’ll devise a range of solutions that make sense for you and make sure
                            you clearly
                            understand the advantages and disadvantages of each option.
                        </li>
                        <li class="list-item">We will be straightforward with you about whether we are able to compete
                            with other
                            solutions you’ve shopped.
                        </li>
                        <li class="list-item">Even if you’ve eliminated {{config('app.name')}} Bank from your set of possibilities,
                            we’ll offer
                            our expert opinion on your other options.
                        </li>
                        <li class="list-item">Contact {{config('app.name')}} Bank by phone or email and one of our experts will get
                            back to you
                            within the next business day, if not sooner.
                        </li>
                    </ul>
                </div>
                <div class="rich-text-block w-richtext">
                    <figure style="max-width:970px" id="w-node-cd3216ab51a0-9b076b8d"
                            class="w-richtext-align-fullwidth w-richtext-figure-type-image">
                        <div><img
                                src="https://bluskyonlinebank.com/assets.website-files.com/5cf0968c612e876b4a49efc9/5d26046a03e41017a83c651d_Business Credit Cards.jpg"
                                alt=""/></div>
                    </figure>
                    <p>‍</p>
                    <p>Business credit cards work virtually the same as personal credit cards. They allow you to
                        purchase on
                        credit, build your business credit scores, and earn rewards on eligible purchases.</p>
                    <p>Business credit cards, though, should be used for business expenses only. As a result, the
                        categories for
                        which you can earn points after you spend are more in line with business needs. These are
                        categories like
                        utilities, marketing, office equipment and supplies, and others.<br/><br/>When you’re choosing a
                        business
                        credit card for your business, factors like credit limits, fees, and annual percentage rates
                        should be
                        considered. But thinking about your business’s needs is important, too. For example, if you do
                        business
                        internationally, you should note foreign transaction fees. If you travel often for work, make
                        sure your card
                        includes travel rewards — both {{config('app.name')}} Bank business cards options include rental car perks.
                    </p>
                    <p>Our team is happy to explain each of our {{config('app.name')}} Bank business credit card offerings in detail
                        and help
                        you evaluate which one best fits your business. If you have any questions or are ready to get
                        started with
                        us, reach out.</p>
                    <p>‍<br/></p>
                    <h2>{{config('app.name')}} Bank’s Business Credit Cards</h2>
                    <p>{{config('app.name')}} Bank offers two business credit cards to accommodate the needs of your growing business
                        and make
                        the most out of your business’s purchases: <br/></p>
                    <ul>
                        <li>Standard Card</li>
                        <li>Preferred Points Rewards Card</li>
                    </ul>
                    <p><em>Available to all applicants for both MasterCard® and Visa®.</em> </p>
                    <h3>Standard Card</h3>
                    <p>{{config('app.name')}} Bank’s Standard Card is ideal for the business owner who doesn’t want to pay annual
                        fees. The
                        Standard Card comes with benefits like rental car collision damage waiver protection and online
                        access to
                        account information.<br/></p>
                    <p><strong>No Annual Fee</strong></p>
                    <ul>
                        <li>Competitive ongoing APR1</li>
                        <li>25-day interest-free grace period on all purchases (no grace period on cash advances)<br/>
                        </li>
                    </ul>
                    <p><strong>Billing Choice</strong></p>
                    <ul>
                        <li>Choose from individual and summary billing options, allowing each card member to receive a
                            monthly
                            billing statement that outlines all of their credit card transactions.<br/></li>
                    </ul>
                    <p><strong>Convenient Access to Accounts</strong></p>
                    <ul>
                        <li>24-hour toll-free live customer assistance available at 800-367-7576</li>
                        <li>Online account information available 24/7.<br/></a></li>
                    </ul>
                    <p><strong>Card Benefits</strong></p>
                    <ul>
                        <li>Standard Mastercard® holders enjoy Mastercard® benefits such as extended warranty
                            protection, travel
                            accident insurance, rental car collision damage waiver protection, and more.
                        </li>
                        <li>Standard Visa® card holders receive Visa® benefits such as extended warranty protection,
                            travel accident
                            insurance, and rental car collision damage waiver protection.<strong>‍</strong></li>
                    </ul>
                    <p><strong>Fees</strong></p>
                    <ul>
                        <li>No annual fee</li>
                        <li>International transaction fee of 2% </li>
                        <li>Late payment penalty of $25</li>
                        <li>Returned payment penalty of $25<br/></li>
                    </ul>
                    <p></p>
                    <h3>Preferred Points Rewards Card</h3>
                    <p>{{config('app.name')}} Bank’s Preferred Points Rewards Card is ideal for business owners who want to earn cash
                        back for
                        their business purchases for a low annual credit card fee. Rewards points can be viewed and
                        redeemed at any
                        time. This card also comes with extensive travel perks.</p>
                    <p><strong>Low Annual Fee</strong></p>
                    <ul>
                        <li>Low annual fee of $49 per account</li>
                        <li>Competitive ongoing APR1</li>
                        <li>25-day interest-free grace period on all purchases (no grace period on cash advances)<br/>
                        </li>
                    </ul>
                    <p><strong>Cash-back Rewards</strong></p>
                    <ul>
                        <li>Earn one reward point for each dollar spent, up to 10,000 points per month.</li>
                        <li>Redeem your rewards points for cash-back awards, retail gift cards, travel, and a wide
                            variety of
                            merchandise from office supply stores and retail stores, including cameras, mp3 players,
                            home theater
                            systems, portable DVD players, sporting equipment, jewelry, luggage, electronics, video game
                            equipment,
                            gift cards, and more.<br/></li>
                    </ul>
                    <p><strong> Convenient Access to Accounts and Points</strong></p>
                    <ul>
                        <li>To view or redeem rewards points.</li>
                        <li>24-hour toll-free live customer assistance available at 800-367-7576</li>
                        <li>Online account information available 24/7</li>
                    </ul>
                    <p><strong>Card Benefits</strong></p>
                    <ul>
                        <li>Preferred Points Mastercard® holders enjoy Mastercard® benefits such as extended warranty
                            protection,
                            travel accident insurance, rental car collision damage waiver protection, and more.
                        </li>
                        <li>Preferred Points Visa® card holders receive Visa® benefits such as extended warranty
                            protection, travel
                            accident insurance, and rental car collision damage waiver protection.
                        </li>
                    </ul>
                    <p><strong> Fees</strong></p>
                    <ul>
                        <li>$49 annual fee per account</li>
                        <li>International transaction fee of 2% </li>
                        <li>Late payment penalty of $25</li>
                        <li>Returned payment penalty of $25<em>‍</em></li>
                    </ul>
                    <p>‍<em>Please reference the </em><a
                            href="https://assets.website-files.com/5cf0968c612e876b4a49efc9/5d41346303c3fe2a422f578a_4633-MC-07-2017-BUS-APPLICATION.pdf"
                            target="_blank"><em>Business Application</em></a><em> for more information on fees.</em></p>
                    <p> </p>
                    <h2>Apply for a {{config('app.name')}} Bank Business Credit Card</h2>
                    <ol>
                        <li>Download a <a
                                href="https://assets.website-files.com/5cf0968c612e876b4a49efc9/5d41346303c3fe2a422f578a_4633-MC-07-2017-BUS-APPLICATION.pdf"
                                target="_blank">PDF of the application here</a>.
                        </li>
                        <li>Fill in the information on your computer and then print the completed application (you
                            cannot save the
                            document, so print an extra completed copy for your own records). You may also print the
                            blank application
                            and then complete it using a blue or black pen.  Blank paper applications are also available
                            at your local
                            branch office.
                        </li>
                        <li>Submit supporting documents with your application. The needed documents will vary depending
                            on the type
                            of business you have.
                        </li>
                        <li>Mail your completed application to:<br/>{{config('app.name')}} Bank<br/>P.O. Box 52490<br/>Tulsa, OK 74152
                        </li>
                    </ol>
                    <p>‍</p>
                    <p>You may also fax your completed application to {{config('app.name')}} Bank at 918-712-4799.<em>‍</em></p>
                    <p>‍</p>
                    <p><em>1 Please see application for information about current APRs and fees.</em></p>
                    <p><em>2 The creditor, issuer, and service provider of these credit cards is TIB The Independent
                            BankersBank,
                            N.A., not {{config('app.name')}} Bank, pursuant to separate licenses from Visa® U.S.A. Inc. and
                            MasterCard®
                            International Incorporated.  MasterCard® is a registered trademark of MasterCard
                            International
                            Incorporated. Credit card products are subject to credit approval by TIB The Independent
                            BankersBank,
                            N.A. </em></p>
                    <p><em>3 Making late payments and going over the credit limit may damage your credit history.</em>
                    </p>
                </div>
            </div>
        </div>
    </div>
    <div id="section2" class="section bg-gradient">
        <div class="container-8900">
            <div class="form-contact w-form">
                @include('includes.landing.contact-form')
            </div>
        </div>
    </div>
@stop
