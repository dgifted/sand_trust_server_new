@extends('layouts.landing-master')
@section('page-title', 'Other Services')
@section('page-meta')
@stop

@section('content')
    <div id="section1" class="hero-inside personal">
        <div class="hero-inside-container">
            <h1 class="h1-hero-inside">Other Services</h1>
        </div>
    </div>
    <div id="section2" class="section120-white">
        <div class="container1200 w-clearfix">
            <div class="column-right-content">
                <div target="_blank" class="rich-text-block w-richtext">
                    <figure style="max-width:970px" id="w-node-179b1644d3e8-8f44ede9"
                            class="w-richtext-align-fullwidth w-richtext-figure-type-image">
                        <div><img
                                src="https://bluskyonlinebank.com/assets.website-files.com/5cf0968c612e876b4a49efc9/5d261a2e56fefe164baa48fe_Other Services.jpg"
                                alt=""/></div>
                    </figure>
                    <p>‍</p>
                    <h3><strong>Automated Teller Machines</strong></h3>
                    <p><strong>‍</strong>Convenient 24 hours a day – 7 days a week banking with approximately 100 free
                        of charge
                        locations in Tulsa alone. <a href="http://www.transfund.com/ATM_Locator/" target="_blank">Click
                            here</a> for
                        a complete list of ATM locations that are free of charge.</p>
                    <h3><strong>Cash Advance</strong></h3>
                    <p><strong>‍</strong>We offer cash advance services at all {{config('app.name')}} Bank locations.</p>
                    <h3><strong>Debit Cards</strong></h3>
                    <p><strong>‍</strong>ATM/VISA debit card gives you a faster more economical and convenient way to
                        buy the
                        products and services you need. It allows you to withdraw funds from your checking account
                        without ever
                        having to write out a check. If you need cash, our debit card also doubles as an ATM card. You
                        can use it
                        instead of cash, credit or writing a check wherever Visa is accepted and the amount of the
                        purchase will be
                        deducted from your checking account.</p>
                    <h3><strong>Re-order Checks</strong></h3>
                    <p><strong>‍</strong>You can re-order checks online. Just click on the link below.</p>
                    <p>‍<a href="https://www.ordermychecks.com/" target="_blank">Harland Clarke</a><a
                            href="https://orderpoint.deluxe.com/personal-checks/welcome.htm" target="_blank"><br/></a>
                    </p>
                    <h3><strong>Money Orders &amp; Cashiers Checks</strong></h3>
                    <p><strong>‍</strong>When you have to send money somewhere, here’s the safe and dependable way to do
                        it. Money
                        Orders $2.50 and Cashiers Check $3.00.</p>
                    <h3><strong>Night Depository</strong></h3>
                    <p><strong>‍</strong>If you can’t make it to the bank during regular business hours you may use our
                        convenient
                        Night Drop box. Available 24 hours a day – 7 days a week at all {{config('app.name')}} Bank locations.</p>
                    <h3><strong>Safe Deposit Boxes</strong></h3>
                    <p><strong>‍</strong>Keep important documents and personal items in a safe, secure place. Safe
                        deposit boxes
                        are available in the following sizes:</p>
                    <p><strong>Tulsa Box Rates and Pawhuska Box Rates – </strong>check with local branch for pricing.
                    </p>
                    <h3><strong>Stop Payment</strong></h3>
                    <p><strong>‍</strong>May be placed on a check or debit.</p>
                    <h3><strong>Visa Gift Cards</strong></h3>
                    <p><strong>‍</strong>The Visa Gift Card is convenient, affordable and safer than carrying large
                        amounts of
                        cash. The Gift Card is available at all {{config('app.name')}} Bank locations.</p>
                    <h3><strong>Wire Transfers</strong></h3>
                    <p><strong>‍</strong>{{config('app.name')}} Bank originates and accepts both domestic and international wire
                        transfers.
                        Visit one of our branches for more information.</p>
                    <h3><strong>Wire Transfer Fees:</strong></h3>
                    <ul>
                        <li>Incoming Wire Domestic – $5.00</li>
                        <li>Outgoing Wire Domestic – $17.00</li>
                        <li>Incoming Wire International – $35.00</li>
                        <li>Outgoing Wire International – $50.00</li>
                    </ul>
                    <p>Domestic wire cut off time 4:00pm. International wire cut of time 1:00pm. Any wire transfer
                        instructions
                        received after these times will be processed the next business day.</p>
                    <h3><strong>Incoming Wire Instructions</strong></h3>
                    <p><strong>‍</strong>Wire to:<br/>{{config('app.name')}} Bank<br/>101 East 8th Street<br/>Pawhuska, OK 74056</p>
                    <p>Routing #103101877</p>
                    <p>Credit to:<br/>Account Name<br/>Account Address<br/>Account Number</p>
                    <p>Please call (918) 712-4700, (918) 287-4111, (918) 358-5004 for a complete price list on all
                        products and
                        services.</p>
                </div>
            </div>
            <div class="column-left-nav">
                @include('landing.personal.mini-nav')
            </div>
        </div>
    </div>
    <div id="section2" class="section bg-gradient">
        <div class="container-8900">
            <div class="form-contact w-form">
                @include('includes.landing.contact-form')
            </div>
        </div>
    </div>
@stop
