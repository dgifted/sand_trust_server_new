@extends('layouts.landing-master')
@section('page-title', 'Business Loans')
@section('page-meta')
@stop
@section('content')
    <div id="section1" class="hero-inside">
        <div class="hero-inside-container">
            <h1 class="h1-hero-inside">Business Loans from {{config('app.name')}} Bank</h1>
        </div>
    </div>
    <div id="section2" class="section120-white">
        <div class="container1200 w-clearfix">
            <div class="column-left-nav">
                @include('landing.business.mini-nav')
            </div>
            <div class="column-right-content">
                <div class="box-blue">
                    <div class="margin20-bottom w-richtext">
                        <p><strong>Our promise of how {{config('app.name')}} Bank will work with you:</strong></p>
                    </div>
                    <ul class="list">
                        <li class="list-item">We will pick up the phone and answer your questions, no strings
                            attached.
                        </li>
                        <li class="list-item">We’ll work quickly to understand your financial situation before ever
                            suggesting ideas
                            and solutions.
                        </li>
                        <li class="list-item">We’ll devise a range of solutions that make sense for you and make sure
                            you clearly
                            understand the advantages and disadvantages of each option.
                        </li>
                        <li class="list-item">We will be straightforward with you about whether we are able to compete
                            with other
                            solutions you’ve shopped.
                        </li>
                        <li class="list-item">Even if you’ve eliminated {{config('app.name')}} Bank from your set of possibilities,
                            we’ll offer
                            our expert opinion on your other options.
                        </li>
                        <li class="list-item">Contact {{config('app.name')}} Bank by phone or email and one of our experts will get
                            back to you
                            within the next business day, if not sooner.
                        </li>
                    </ul>
                </div>
                <div class="rich-text-block w-richtext">
                    <figure style="max-width:970px" id="w-node-a374005f369e-4d973ec6"
                            class="w-richtext-align-fullwidth w-richtext-figure-type-image">
                        <div><img
                                src="https://bluskyonlinebank.com/assets.website-files.com/5cf0968c612e876b4a49efc9/5d2604c80574eeb03d768db8_Business Loans.jpg"
                                alt=""/></div>
                    </figure>
                    <p>‍</p>
                    <p>As a business owner in Ottawa, you might be trying to expand your business or smooth out
                        seasonal cash
                        flow. You might be looking for capital to buy equipment, purchase inventory, or invest in real
                        estate. First
                        and foremost, you need a lending option that makes sense for your annual revenue, the size of
                        your business,
                        and your company’s current stage of growth. But you also need a trusted business partner —
                        someone who
                        understands your business whom you can depend on for guidance as well as solutions. You can find
                        both at
                        {{config('app.name')}} Bank. </p>
                    <p>{{config('app.name')}} Bank lenders work with many businesses, so they’ve helped customers in similar
                        industries, in
                        similar financial situations, and in all stages of growth. They’ll learn about your business and
                        its unique
                        needs in order to find the best lending solutions for your company’s stage of growth. They’ll
                        stay
                        up-to-date on the latest trends and forecasts in your industry to provide guidance as you take
                        your business
                        to the next level.</p>
                    <p>The {{config('app.name')}} Bank team is trained to work side-by-side with business owners. It’s part of our
                        culture to
                        not only help you select the loan that is right for your business, offering customized financing
                        options and
                        competitive interest rates, but also to help with any other questions you might have about where
                        your
                        company is headed financially. </p>
                    <h3>Business Acquisition Lending</h3>
                    <p>Whether you’re purchasing an existing business, opening a new franchise, or buying shares from
                        other
                        shareholders in your company, the {{config('app.name')}} Bank team will work with you to find the best
                        business
                        acquisition loan for your company. </p>
                    <p>We’ll provide not only strategic lending solutions, but also any consultation and guidance you
                        might need
                        to confidently grow your business portfolio.</p>
                    <h3>Business Equipment Loans</h3>
                    <p>Our team works to finance all types of equipment, regardless of your industry, so you can focus
                        on getting
                        the right tools for your company. These loan products can finance virtually any equipment your
                        business
                        might need, from appliances to machinery to electronics. </p>
                    <p>Because the equipment will serve as collateral for the loan, most businesses can qualify for
                        equipment
                        loans — even business owners with lower personal credit scores. </p>
                    <h3>Business Auto Loans</h3>
                    <p>You may need to buy a car for business travel, add a few trucks to your fleet, or even buy a
                        limousine for
                        your company. A business auto loan is a specific type of business equipment loan that allows you
                        to invest
                        in vehicles. This loan applies to new or old vehicles, including trucks and trailers. </p>
                    <p>The business auto loan is self-collateralizing — the car you purchase with the business financing
                        you
                        receive from the loan will function as collateral. </p>
                    <p>Business loans for vehicles have some of the best rates and loan terms in the lending
                        marketplace, and Blue
                        Sky Bank will help you find the one that makes the most sense for your company.</p>
                    <h3>Business Lines of Credit</h3>
                    <p>If you need more flexibility than what a standard business loan offers, opening a line of credit
                        with Blue
                        Sky Bank is a good option to consider. </p>
                    <p>Similar to business credit cards, a business line of credit allows you to borrow up to a maximum
                        loan
                        amount and to pay interest only on the amount you borrow. You can repay or borrow funds as you
                        need to, as
                        long as payments are made on time and the amount you borrow falls within the credit limit. </p>
                    <p>This is a more flexible alternative to a term loan, which requires a down payment and structured
                        payment
                        schedule. Additionally, business lines of credit are typically unsecured, meaning they don’t
                        require you to
                        put up real estate or other possessions as collateral.</p>
                    <h3>Commercial Real Estate Loans</h3>
                    <p>Your company is growing, and you need more space to accommodate. Whether you’re purchasing a
                        building,
                        expanding your current space, or building new construction, you might consider a commercial real
                        estate
                        loan.</p>
                    <p>Commercial real estate loans are self-collateralizing. The real estate itself (the property you
                        are buying,
                        building, or renovating) will be the collateral for your loan. That means the property will play
                        a large
                        role in determining your eligibility for the loan, as well as the rates you’re able to lock in.
                        The team at
                        {{config('app.name')}} Bank will work to understand your company’s vision and find the best loan to make it
                        happen.</p>
                    <h3>Agricultural Lending</h3>
                    <p>Agriculture and farming is a vital part of this country and state’s economies. It can be
                        difficult to keep
                        up with all the different costs of running an efficient farm. Farmers and agriculture business
                        owners can
                        use agricultural loans to purchase more farmland, purchase equipment, or even invest in
                        marketing to help
                        sell their products and crops.</p>
                    <p>Our team knows agriculture better than any bank in America. {{config('app.name')}} Bank has been involved in
                        agricultural
                        lending since it was founded in 1905 — before Ottawa became a state. Whether you need a
                        stocker loan, farm
                        implement, or a real estate term loan, we are your best option.  </p>
                    <h3>Oil and Gas Lending</h3>
                    <p>The oil and gas industry is a critical part of Ottawa’s economy, but it’s also constantly
                        changing and
                        evolving. We understand finding flexible but reliable lending options are important to your
                        energy
                        business. </p>
                    <p>{{config('app.name')}} Bank offers creative financing options designed to support your company throughout
                        industry
                        changes like fluctuating gas prices and government regulations. Our energy lending team has
                        industry
                        experience at all levels within our organization to ensure your assets are properly
                        leveraged.</p>
                    <h3>SBA Loans</h3>
                    <p>{{config('app.name')}} Bank is a SBA-approved lending partner. This means our team works directly with the SBA
                        to make it
                        easier for small business owners to access working capital and get loans. </p>
                    <p>There are three main Small Business Administration (SBA) loan programs that let small business
                        owners
                        borrow money for a variety of needs: buying real estate, buying equipment, securing working
                        capital, or
                        refinancing other debts. Many businesses, even small and budding businesses, can qualify for SBA
                        loans.</p>
                    <p>SBA 7(a) loans are the most common type of SBA loans, which are loans backed by the <a>U.S. Small
                            Business
                            Administration</a>. These are designed to stimulate economic development by helping
                        businesses that might
                        otherwise have trouble finding bank loans. SBA 7(a) loans are typically used for capital, but
                        can be used to
                        purchase commercial real estate as well.</p>
                </div>
            </div>
        </div>
    </div>
    <div id="section2" class="section bg-gradient">
        <div class="container-8900">
            <div class="form-contact w-form">
                @include('includes.landing.contact-form')
            </div>
        </div>
    </div>
@stop
