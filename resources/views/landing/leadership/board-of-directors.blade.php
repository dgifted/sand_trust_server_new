@extends('layouts.landing-master')
@section('page-title', 'Board of Directors')
@section('page-meta')
    <meta content="Ottawa owned for over 100 years." name="description"/>
    <meta content="Executive Team | {{config('app.name')}}" property="og:title"/>
    <meta content="Ottawa owned for over 100 years." property="og:description"/>
    <meta content="https://assets.website-files.com/5cf0968c612e876b4a49efc9/5d51de4e2ca59a54469245f9_BSB_SEO_Home.jpg"
          property="og:image"/>
@stop

@section('content')
    <div id="section1" class="hero-inside leadership">
        <div class="hero-inside-container">
            <h1 class="h1-hero-inside">Board of Directors</h1>
        </div>
    </div>
    <div id="section2" class="section120-white">
        <div class="container1200 w-clearfix">
            <div class="column-left-nav">
                <a href="{{route('boards')}}" class="nav-link-sidebar w--current">
                    Board of Directors</a>
                <a href="{{route('executives')}}" class="nav-link-sidebar">
                    Executive Team</a></div>
            <div class="column-right-content">
                <div class="rich-text-block w-richtext">
                    <h2>Ottawa Owned for over 100 years</h2>
                    <p><strong>Board of Directors<br/></strong></p>
                    <p>Gary S. Weyl, Chairman</p>
                    <p>Gentner F. Drummond</p>
                    <p>David Weyl</p>
                    <p>Patricia Chernicky</p>
                    <p>David Story</p>
                    <p>Brian Schneider</p>
                    <p>Chris Wilson</p>
                    <p>John Marouk, D.O., Advisory Director</p>
                    <p>Stratford Tolson, Director Emeritus</p>
                </div>
            </div>
        </div>
    </div>
    <div id="section2" class="section bg-gradient">
        <div class="container-8900">
            <div class="form-contact w-form">
                @include('includes.landing.contact-form')
            </div>
        </div>
    </div>
@stop
