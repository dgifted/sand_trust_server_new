@extends('layouts.landing-master')
@section('page-title', 'In The News')
@section('page-meta')
    <meta content="The latest news on {{config('app.name')}} Bank." name="description"/>
    <meta content="In The News | {{config('app.name')}} Bank" property="og:title"/>
    <meta content="The latest news on {{config('app.name')}} Bank." property="og:description"/>
    <meta content="https://assets.website-files.com/5cf0968c612e876b4a49efc9/5d51de4e2ca59a54469245f9_BSB_SEO_Home.jpg"
          property="og:image"/>
@stop

@section('page-styles')
@stop

@section('content')
    <div id="section1" class="hero-inside about">
        <div class="hero-inside-container">
            <h1 class="h1-hero-inside">In The News</h1>
        </div>
    </div>
    <div id="section2" class="section120-white">
        <div class="container1200 w-clearfix">
            <div class="column-left-nav">
{{--                <a href="history.html" class="nav-link-sidebar">History</a>--}}
{{--                <a href="in-the-news.html" class="nav-link-sidebar w--current">In The News</a>--}}
{{--                <a href="core-values.html" class="nav-link-sidebar">Core Values</a>--}}
{{--                <a href="https://www.paycomonline.net/v4/ats/web.php/jobs?clientkey=FCF88DE32941E4539867618CB40A37BD"--}}
{{--                    target="_blank" class="nav-link-sidebar">Careers</a>--}}
            </div>
            <div class="column-right-content">
                <div class="rich-text-block w-richtext">
                    <figure style="max-width:970px" id="w-node-a5a123fc96cb-61669a71"
                            class="w-richtext-align-fullwidth w-richtext-figure-type-image">
                        <div><img
                                src="{{asset('assets/imgs/name_changed.jpg')}}"
                                alt=""/></div>
                    </figure>
                    <p>‍<br/></p>
                </div>
                <div class="rich-text-block w-richtext">
                    <h2>2019</h2>
                    <ul>
                        <li><a
                                href="https://www.tulsaworld.com/business/businesspeople/financial-brian-schneider-nbc/article_d43fe5e4-0d78-11ea-82b9-d7abd49b9a92.html"
                                target="_blank">Brian Schneider hired as CEO of NBC, {{config('app.name')}} Bank and Bank of
                                Cushing</a></li>
                    </ul>
                    <h2>2018</h2>
                    <ul>
                        <li>
                            <a href="https://www.pawhuskajournalcapital.com/news/20180808/blue-sky-parent-buys-cushing-bank"
                               target="_blank">{{config('app.name')}} Bank&#x27;s holding company, N.B.C. Bancshares, purchases Bank
                                of Cushing</a>
                        </li>
                    </ul>
                    <h2>2017</h2>
                    <ul>
                        <li>You have likely heard about the recent Equifax data breach, as this unfortunate event
                            affects
                            approximately of Americans.  This was NOT a compromise of {{config('app.name')}} Bank and no information
                            was taken from
                            our systems.  We wanted to share this information about Equifax to ensure you are aware of
                            the issue and
                            take the proper precautions to reduce the chances of fraud and identity theft.  Please click
                            for more
                            information <a
                                href="https://assets.website-files.com/5cf0968c612e876b4a49efc9/5d68c0f1677c2a77e6f1371c_Customer-Notice.pdf"
                                target="_blank" data-goodbye-skip="1">Equifax Data Breach Information</a>.
                        </li>
                    </ul>
                    <ul>
                        <li><a href="https://youtu.be/0QV_Cj4r7Yk" target="_blank">Sand Trust Bank Commercial #2</a>
                        </li>
                    </ul>
                    <p>‍</p>
                    <h2>2016</h2>
                    <ul>
                        <li><a
                                href="http://www.tulsaworld.com/business/consumer/local-financial-institution-changes-name-to-blue-sky-bank/article_a3e93d78-2b2c-5609-a2fd-95d34693657d.html#.WBlBQfMp_SQ.facebook"
                                target="_blank">Introducing Sand Trust Bank!</a></li>
                        <li>
                            <a href="http://www.theclevelandamerican.com/articles/2016/11/10/news/doc58233dddcd618531649484.txt"
                               target="_blank">Sand Trust Bank – Cleveland, Ottawa</a></li>
                        <li><a
                                href="http://www.tulsaworld.com/business/realestate/the-boxyard-expected-to-open-downtown-dec/article_0d507325-ecbd-54e0-b3f6-0dab53ad4385.html#.WCRu2wtwOoI.facebook"
                                target="_blank">Boxyard and Sand Trust</a></li>
                        <li><a href="https://youtu.be/zxMM6bRDo40" target="_blank">Good Day Tulsa Interview</a></li>
                        <li><a href="https://youtu.be/z5DAXIuT04g" target="_blank">Sand Trust Bank Commercial</a></li>
                    </ul>
                    <p>‍</p>
{{--                    <figure class="w-richtext-align-normal w-richtext-figure-type-image">--}}
{{--                        <div><img--}}
{{--                                src="https://bluskyonlinebank.com/assets.website-files.com/5cf0968c612e876b4a49efc9/5d13b147389a347fdfc84d49_bsb_boxyard.jpg"--}}
{{--                                alt=""/></div>--}}
{{--                    </figure>--}}
{{--                    <p>Now in the Boxyard downtown</p>--}}
                    <p>‍</p>
                    <figure style="padding-bottom:33.723653395784545%" id="w-node-c794c8486196-61669a71"
                            class="w-richtext-align-normal w-richtext-figure-type-video">
                        <div>
                            <iframe allowfullscreen="true" frameborder="0" scrolling="no"
                                    src="https://www.youtube.com/embed/9X00eJqYdSQ"></iframe>
                        </div>
                    </figure>
                </div>
            </div>
        </div>
    </div>
    <div id="section2" class="section bg-gradient">
        <div class="container-8900">
            <div class="form-contact w-form">
                @include('includes.landing.contact-form')
            </div>
        </div>
    </div>
@stop

@section('page-scripts')
@endsection
