@extends('layouts.admin-master')

@section('page-title', 'Approve Debit Card Request')
@section('plugin-styles')
@stop
@section('page-styles')

@stop
@section('content-header', 'Approve Debit Card Request')

@section('content')
    <div id="eventList" class="row">
        <div class="col-12">
            <div class="card">
                <!-- /.card-header -->
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-6 offset-md-3">
                            <p><b>Requested by: <i class="text-uppercase">{{$cardRequest->user->first_name}} {{$cardRequest->user->last_name}}</i></b></p>
                            <p><b>Requested on: {{$cardRequest->created_at->toFormattedDateString()}}</b></p>

                        </div>
                    </div>
                    <hr>
                    <form action="{{route('admin.card-requests-approve', $cardRefId)}}" method="POST">
                        @csrf

                        <div class="row my-5">
                            <div class="col-12 col-md-6 offset-md-3">
                                <div class="form-group">
                                    <label for="number">Number</label>
                                    <input type="text" class="form-control" id="number" name="number" required>
                                </div>

                                <div class="row">
                                    <div class="col-4">
                                        <div class="form-group">
                                            <label for="pin">CVV</label>
                                            <input type="number" class="form-control" id="pin" name="pin" required>
                                        </div>
                                    </div>
                                    <div class="col-4">
                                        <div class="form-group">
                                            <label for="expMonth">Expiry month</label>
                                            <input type="number" class="form-control" id="expMonth" name="expMonth" required>
                                        </div>
                                    </div>
                                    <div class="col-4">
                                        <div class="form-group">
                                            <label for="expYear">Expiry year</label>
                                            <input type="number" class="form-control" id="expYear" name="expYear" required>
                                        </div>
                                    </div>
                                </div>
                                <div class="mt-3">
                                    <button type="submit" class="btn btn-primary mr-3">Create card</button>
                                    <a href="{{ url('https://www.vccgenerator.org/') }}">Follow link to generate card</a>
                                </div>
                            </div>

                            <input type="hidden" name="accountId" value="{{$accountId}}">
                        </div>
                    </form>
                </div>
                <!-- /.card-body -->
            </div>
        </div>
    </div>
@stop
@section('page-plugin')
@stop
@section('page-scripts')
    <script>
        $("#example1").DataTable({
            "responsive": true,
            "lengthChange": false,
            "autoWidth": false,
            "buttons": ["copy", "csv", "excel", "pdf", "print", "colvis"]
        }).buttons().container().appendTo('#example1_wrapper .col-md-6:eq(0)');
    </script>
@stop
