@extends('layouts.admin-master')

@section('page-title', 'Ticket Detail')
@section('plugin-styles')
@stop
@section('page-styles')
@stop
@section('content-header', 'Ticket Detail')

@section('content')
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body text-center">
                    <strong>
                        Ticket Reference Id: {{ $ticket->ref_id }}
                    </strong>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            @if (session('message'))
                <div class="alert alert-{{ session('type') }} alert-dismissible fade show" role="alert">
                    <p class="m-0 p-0">
                        {{ session('message') }}
                    </p>
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
            @endif

            <div class="card">
                <div class="card-header">
                    <div class="d-flex justify-content-between">
                        <h3 class="card-title mt-2">Support Ticket Details</h3>
                    </div>
                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-4">
                            <!-- Widget: user widget style 1 -->
                            <div class="card card-widget widget-user">
                                <!-- Add the bg color to the header using any of the bg-* classes -->
                                <div class="widget-user-header bg-info">
                                    <h3 class="widget-user-username">
                                        {{ $ticket->author->first_name }}
                                        {{ $ticket->author->last_name }}
                                    </h3>
                                    <h5 class="widget-user-desc">{{ $ticket->author->email }}</h5>
                                </div>
                                <div class="widget-user-image">
                                    <img class="img-circle elevation-2" src="{{ $ticket->author->avatar }}"
                                         alt="User Avatar">
                                </div>
                                <div class="card-footer">
                                    <div class="row">
                                        <div class="col-sm-6 offset-md-3">
                                            <div class="description-block">
                                                <h5 class="description-header">Registered on:</h5>
                                                <span class="description-text text-sm">
                                                   {{ $ticket->author->created_at->toFormattedDateString() }}
                                                </span>
                                                <a href="{{ route('admin.customers.single', $ticket->author->uid) }}"
                                                   class="btn btn-link"
                                                >
                                                    View customer details
                                                </a>
                                            </div>
                                            <!-- /.description-block -->
                                        </div>
                                        <!-- /.col -->
                                        <!-- /.col -->
                                    </div>
                                    <!-- /.row -->
                                </div>
                            </div>
                            <!-- /.widget-user -->
                        </div>
                        <div class="col-md-8">
                            <div class="row">
                                <div class="col-12">
                                    <div class="timeline">
                                        <div class="time-label">
                                            <span
                                                class="bg-red">{{ $ticket->created_at->toFormattedDateString() }}</span>
                                        </div>

                                        <!-- timeline item -->
                                        <div>
                                            <i class="fas fa-envelope bg-blue"></i>
                                            <div class="timeline-item">
                                                <span class="time"><i class="fas fa-clock"></i> {{ $ticket->created_at->toFormattedDateString() }}</span>
                                                <h3 class="timeline-header"><a
                                                        href="#">{{ $ticket->author->first_name }} {{ $ticket->author->last_name }}</a>
                                                    created this ticket
                                                </h3>

                                                <div class="timeline-body">{!! $ticket->content !!}</div>
                                                <div class="timeline-footer">
                                                    @if($ticket->is_closed === \App\Models\SupportTicket::OPEN)
                                                        <a class="btn btn-primary btn-sm"
                                                           href="{{ route('admin.support-tickets-single-close', $ticket->ref_id) }}"
                                                        >Mark Closed
                                                        </a>
                                                    @endif
                                                    @if($ticket->is_closed === \App\Models\SupportTicket::CLOSED)
                                                        <a class="btn btn-primary btn-sm"
                                                           href="{{ route('admin.support-tickets-single-reopen', $ticket->ref_id) }}"
                                                        >Reopen <Ticket></Ticket>
                                                        </a>
                                                    @endif
                                                </div>
                                            </div>
                                        </div>
                                        <!-- END timeline item -->

                                        @if($ticket->replies->count() > 0)
                                            @foreach($ticket->replies as $reply)
                                                <div>
                                                    <i class="fas fa-comments bg-yellow"></i>
                                                    <div class="timeline-item">
                                                        <span class="time"><i class="fas fa-clock"></i> {{ $reply->created_at->diffForHumans() }}</span>
                                                        <h3 class="timeline-header"><a
                                                                href="#">{{ $reply->author->first_name }} {{ $reply->author->last_name }}</a>
                                                            commented on this post</h3>
                                                        <div class="timeline-body">
                                                            {!! $reply->content !!}
                                                        </div>
                                                    </div>
                                                </div>
                                            @endforeach
                                        @endif
                                    </div>
                                    @if($ticket->is_closed === \App\Models\SupportTicket::OPEN)
                                        <form action="{{ route('admin.support-tickets-single', $ticket->ref_id) }}"
                                              method="post">
                                            @csrf

                                            <label for="summernote" class="d-none"></label>
                                            <textarea id="summernote" name="content">...</textarea>
                                            <button type="submit" class="btn btn-primary btn-sm">Submit</button>
                                        </form>
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop
@section('page-plugin')
@stop
@section('page-scripts')
    <script>
        $(function () {
            $('#summernote').summernote()
        });
    </script>
@stop
