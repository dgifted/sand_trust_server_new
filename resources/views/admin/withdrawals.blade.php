@extends('layouts.admin-master')

@section('page-title', 'Withdrawals')
@section('plugin-styles')
    <link rel="stylesheet" href="{{ asset('assets/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/plugins/datatables-responsive/css/responsive.bootstrap4.min.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/plugins/datatables-buttons/css/buttons.bootstrap4.min.css') }}">
@stop
@section('page-styles')

@stop
@section('content-header', 'Withdrawals')

@section('content')
    <div id="eventList" class="row">
        <div class="col-12">
            <div class="card">
                <!-- /.card-header -->
                <div class="card-body">
                    <table id="example1" class="table table-bordered table-striped">
                        <thead>
                        <tr>
                            <th>S/N</th>
                            <th>Amount</th>
                            <th>Account Number</th>
                            <th>Customer</th>
                            <th>Destination Bank Name</th>
                            <th>Destination Bank Account No</th>
                            <th>Initiated on</th>
                            <th>Status</th>
                            <th>Actions</th>
                        </tr>
                        </thead>
                        <tbody>
                        @php
                            $count = 0;
                        @endphp
                        @if ($withdrawals->count() > 0)
                            @foreach ($withdrawals as $withdrawal)
                                @php
                                    $count++;
                                @endphp
                                <tr>
                                    <td>{{ $count }}</td>
                                    <td>{{ $withdrawal->amount }}</td>
                                    <td class="text-capitalize">
                                        {{ $withdrawal->account->number }}
                                    </td>
                                    <td>
                                        <a href="{{ route('admin.customers.single', $withdrawal->account->user->uid) }}">
                                            {{ $withdrawal->account->user->first_name }} {{ $withdrawal->account->user->last_name }}
                                        </a>
                                    </td>
                                    <td>{{ $withdrawal->bank->bank_name }}</td>
                                    <td>{{ $withdrawal->bank->bank_account_number }}</td>
                                    <td>
                                        {{ $withdrawal->created_at->toFormattedDateString() }}
                                    </td>
                                    <td>
                                        @switch($withdrawal->approval_status)
                                            @case(\App\Models\Withdrawal::STATUS_PENDING)
                                            <span class="badge badge-sm badge-warning">Pending</span>
                                            @break
                                            @case(\App\Models\Withdrawal::STATUS_COMPLETED)
                                            <span class="badge badge-sm badge-success">Completed</span>
                                            @break
                                            @case(\App\Models\Withdrawal::STATUS_FAILED)
                                            <span class="badge badge-sm badge-danger">Failed</span>
                                            @break
                                        @endswitch
                                    </td>
                                    <td>
                                        @if($withdrawal->approval_status === \App\Models\Withdrawal::STATUS_PENDING)
                                            <div class="dropdown">
                                                <button class="btn btn-light dropdown-toggle" type="button"
                                                        id="dropdownMenu2" data-toggle="dropdown" aria-haspopup="true"
                                                        aria-expanded="false">
                                                    <i class="fas fa-ellipsis-v"></i>
                                                </button>

                                                <div class="dropdown-menu dropdown-menu-right"
                                                     aria-labelledby="dropdownMenu2">
                                                    <button class="dropdown-item">
                                                        <a class="text-info"
                                                           href="{{ route('admin.withdrawals-approve', $withdrawal->ref_id) }}">
                                                            <i class="fas fa-pen-square mr-1"></i> Approve
                                                        </a>
                                                    </button>
                                                    <button class="dropdown-item">
                                                        <a class="text-info"
                                                           href="{{ route('admin.withdrawals-decline', $withdrawal->ref_id) }}">
                                                            <i class="fas fa-pen-square mr-1"></i> Decline
                                                        </a>
                                                    </button>
                                                </div>
                                            </div>
                                        @endif
                                    </td>
                                </tr>
                            @endforeach
                        @endif
                        </tbody>

                        <tfoot>
                        <tr>
                            <th>S/N</th>
                            <th>Amount</th>
                            <th>Account Number</th>
                            <th>Customer</th>
                            <th>Destination Bank Name</th>
                            <th>Destination Bank Account No</th>
                            <th>Initiated on</th>
                            <th>Status</th>
                            <th>Actions</th>
                        </tr>
                        </tfoot>
                    </table>
                </div>
                <!-- /.card-body -->
            </div>
        </div>
    </div>
@stop
@section('page-plugin')
    <script src="{{ asset('assets/plugins/datatables/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('assets/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js') }}"></script>
    <script src="{{ asset('assets/plugins/datatables-responsive/js/dataTables.responsive.min.js') }}"></script>
    <script src="{{ asset('assets/plugins/datatables-responsive/js/responsive.bootstrap4.min.js') }}"></script>
    <script src="{{ asset('assets/plugins/datatables-buttons/js/dataTables.buttons.min.js') }}"></script>
    <script src="{{ asset('assets/plugins/datatables-buttons/js/buttons.bootstrap4.min.js') }}"></script>
    <script src="{{ asset('assets/plugins/jszip/jszip.min.js') }}"></script>
    <script src="{{ asset('assets/plugins/pdfmake/pdfmake.min.js') }}"></script>
    <script src="{{ asset('assets/plugins/pdfmake/vfs_fonts.js') }}"></script>
    <script src="{{ asset('assets/plugins/datatables-buttons/js/buttons.html5.min.js') }}"></script>
    <script src="{{ asset('assets/plugins/datatables-buttons/js/buttons.print.min.js') }}"></script>
    <script src="{{ asset('assets/plugins/datatables-buttons/js/buttons.colVis.min.js') }}"></script>
@stop
@section('page-scripts')
    <script>
        $("#example1").DataTable({
            "responsive": true,
            "lengthChange": false,
            "autoWidth": false,
            "buttons": ["copy", "csv", "excel", "pdf", "print", "colvis"]
        }).buttons().container().appendTo('#example1_wrapper .col-md-6:eq(0)');
    </script>
@stop
