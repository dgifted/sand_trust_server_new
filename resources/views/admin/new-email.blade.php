@extends('layouts.admin-master')

@section('page-title', 'Send out new email')
@section('plugin-styles')
@stop
@section('page-styles')

@stop
@section('content-header', 'Sent Out New Email')

@section('content')
    <div id="eventList" class="row">
        @if(session('message'))
            <div class="col-12">
                <div class="alert alert-{{ session('type') }}">
                    <p>{{ session('message') }}</p>
                </div>
            </div>
        @endif
        <div class="col-12">
            <div class="card">
                <!-- /.card-header -->
                <div class="card-body">
                    <form action="{{route('admin.send-new-email')}}" method="POST">
                        @csrf

                        <div class="row my-5">
                            <div class="col-12 col-md-6 offset-md-3">
                                <div class="form-group">
                                    <label for="recipient">Recipient</label>
                                    <select class="form-control" id="recipient" name="recipient" required>
                                        <option value="">Select recipient</option>
                                        @foreach($users as $user)
                                            <option value="{{ $user->id }}">
                                                {{ $user->first_name }} {{ $user->last_name }}
                                            </option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label for="subject">Subject</label>
                                    <input type="text" class="form-control" id="subject" name="subject" required
                                           value="{{ old('subject') }}">
                                </div>

                                <div class="form-group">
                                    <label for="summernote">Message</label>
                                    <textarea id="summernote" name="content"></textarea>
                                    <input type="hidden" value="{{ old('content') ?? '' }}" id="content-value">
                                </div>

                                <div class="mt-3">
                                    <button type="submit" class="btn btn-primary">Send email</button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
                <!-- /.card-body -->
            </div>
        </div>
    </div>
@stop
@section('page-plugin')
@stop
@section('page-scripts')
    <script>
        $(function () {
            $('#summernote').summernote('pasteHTML', $('#content-value').val())
        });
    </script>
@stop
