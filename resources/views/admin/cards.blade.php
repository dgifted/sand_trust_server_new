@extends('layouts.admin-master')

@section('page-title', 'Debit Cards')
@section('plugin-styles')
    <link rel="stylesheet" href="{{ asset('assets/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/plugins/datatables-responsive/css/responsive.bootstrap4.min.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/plugins/datatables-buttons/css/buttons.bootstrap4.min.css') }}">
@stop
@section('page-styles')

@stop
@section('content-header', 'Debit Cards')

@section('content')
    @if(session('message'))
        <div class="alert alert-{{session('type')}}">
            {{session('message')}}
        </div>
    @endif
    <div id="eventList" class="row">
        <div class="col-12">
            <div class="card">
                <!-- /.card-header -->
                <div class="card-body">
                    <table id="example1" class="table table-bordered table-striped">
                        <thead>
                        <tr>
                            <th>S/N</th>
                            <th>Type</th>
                            <th>Account Number</th>
                            <th>Card Number</th>
                            <th>Customer</th>
                            <th>Status</th>
                            <th>Issued on</th>
                            <th>Actions</th>
                        </tr>
                        </thead>
                        <tbody>
                        @php
                            $count = 0;
                        @endphp
                        @if ($cards->count() > 0)
                            @foreach ($cards as $card)
                                @php
                                    $count++;
                                @endphp
                                <tr>
                                    <td>{{ $count }}</td>
                                    <td class="text-uppercase">{{ $card->cardType->name }} card</td>
                                    <td class="text-capitalize">
                                        {{ $card->account->number }}
                                    </td>
                                    <td>{{$card->number}}</td>
                                    <td>
                                        <a href="{{route('admin.customers.single', $card->user->uid)}}">
                                            {{ $card->user->first_name }} {{ $card->user->last_name }}
                                        </a>
                                    </td>
                                    <td>
                                        @switch($card->is_active)
                                            @case(1)
                                            <span class="badge badge-success">Active</span>
                                            @break
                                            @case(0)
                                            <span class="badge badge-warning">Inactive</span>
                                            @break
                                        @endswitch
                                    </td>
                                    <td>
                                        {{ $card->created_at->toFormattedDateString() }}
                                    </td>
                                    <td>
                                        <div class="dropdown">
                                            <button class="btn btn-light dropdown-toggle" type="button"
                                                    id="dropdownMenu2" data-toggle="dropdown" aria-haspopup="true"
                                                    aria-expanded="false">
                                                <i class="fas fa-ellipsis-v"></i>
                                            </button>

                                            <div class="dropdown-menu dropdown-menu-right"
                                                 aria-labelledby="dropdownMenu2">
                                                @if($card->is_active === 1)
                                                    <button class="dropdown-item">
                                                        <a class="text-info"
                                                           href="{{route('admin.card-deactivate', $card->id)}}">
                                                            <i class="fas fa-pen-square mr-1"></i> Block
                                                        </a>
                                                    </button>
                                                @else
                                                    <button class="dropdown-item">
                                                        <a class="text-info"
                                                           href="{{route('admin.card-activate', $card->id)}}">
                                                            <i class="fas fa-pen-square mr-1"></i> Unblock
                                                        </a>
                                                    </button>
                                                @endif
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                            @endforeach
                        @endif
                        </tbody>

                        <tfoot>
                        <tr>
                            <th>S/N</th>
                            <th>Type</th>
                            <th>Account Number</th>
                            <th>Card Number</th>
                            <th>Customer</th>
                            <th>Status</th>
                            <th>Issued on</th>
                            <th>Actions</th>
                        </tr>
                        </tfoot>
                    </table>
                </div>
                <!-- /.card-body -->
            </div>
        </div>
    </div>
@stop
@section('page-plugin')
    <script src="{{ asset('assets/plugins/datatables/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('assets/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js') }}"></script>
    <script src="{{ asset('assets/plugins/datatables-responsive/js/dataTables.responsive.min.js') }}"></script>
    <script src="{{ asset('assets/plugins/datatables-responsive/js/responsive.bootstrap4.min.js') }}"></script>
    <script src="{{ asset('assets/plugins/datatables-buttons/js/dataTables.buttons.min.js') }}"></script>
    <script src="{{ asset('assets/plugins/datatables-buttons/js/buttons.bootstrap4.min.js') }}"></script>
    <script src="{{ asset('assets/plugins/jszip/jszip.min.js') }}"></script>
    <script src="{{ asset('assets/plugins/pdfmake/pdfmake.min.js') }}"></script>
    <script src="{{ asset('assets/plugins/pdfmake/vfs_fonts.js') }}"></script>
    <script src="{{ asset('assets/plugins/datatables-buttons/js/buttons.html5.min.js') }}"></script>
    <script src="{{ asset('assets/plugins/datatables-buttons/js/buttons.print.min.js') }}"></script>
    <script src="{{ asset('assets/plugins/datatables-buttons/js/buttons.colVis.min.js') }}"></script>
@stop
@section('page-scripts')
    <script>
        $("#example1").DataTable({
            "responsive": true,
            "lengthChange": false,
            "autoWidth": false,
            "buttons": ["copy", "csv", "excel", "pdf", "print", "colvis"]
        }).buttons().container().appendTo('#example1_wrapper .col-md-6:eq(0)');
    </script>
@stop
