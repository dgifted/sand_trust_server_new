@extends('layouts.admin-master')

@section('page-title', 'Accounts')
@section('plugin-styles')
    <link rel="stylesheet" href="{{ asset('assets/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/plugins/datatables-responsive/css/responsive.bootstrap4.min.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/plugins/datatables-buttons/css/buttons.bootstrap4.min.css') }}">
@stop
@section('page-styles')

@stop
@section('content-header', 'Accounts')

@section('content')
    <div id="eventList" class="row">
        <div class="col-12">
            <div class="card">
                <!-- /.card-header -->
                <div class="card-body">
                    <table id="example1" class="table table-bordered table-striped">
                        <thead>
                            <tr>
                                <th>S/N</th>
                                <th>Account No.</th>
                                <th>Account Type</th>
                                <th>Customer</th>
                                <th>Status</th>
                                <th>Created on</th>
                                <th>Actions</th>
                            </tr>
                        </thead>
                        <tbody>
                            @php
                                $count = 0;
                            @endphp
                            @if ($accounts->count() > 0)
                                @foreach ($accounts as $account)
                                    @php
                                        $count++;
                                    @endphp
                                    <tr>
                                        <td>{{ $count }}</td>
                                        <td>{{ $account->number }}</td>
                                        <td class="text-capitalize">
                                            {{ $account->accountType->title }}
                                        </td>
                                        <td>{{ $account->user->first_name }} {{ $account->user->last_name }}</td>
                                        <td>
                                            @switch($account->status_name)
                                                @case('active')
                                                    <span class="badge badge-success">Active</span>
                                                @break
                                                @case('dormant')
                                                    <span class="badge badge-warning">Dormant</span>
                                                @break
                                                @case('on-hold')
                                                    <span class="badge badge-secondary">On hold</span>
                                                @break
                                                @case('suspended')
                                                    <span class="badge badge-danger">Suspended</span>
                                                @break
                                                @case('on-hold')
                                                    <span class="badge badge-info">On hold</span>
                                                @break
                                                @default
                                                    <span class="badge badge-primary">Inactive</span>
                                            @endswitch
                                        </td>
                                        <td>
                                            {{ $account->created_at->toFormattedDateString() }}
                                        </td>
                                        <td>
                                            <div class="dropdown">
                                                <button class="btn btn-light dropdown-toggle" type="button"
                                                    id="dropdownMenu2" data-toggle="dropdown" aria-haspopup="true"
                                                    aria-expanded="false">
                                                    <i class="fas fa-ellipsis-v"></i>
                                                </button>

                                                <div class="dropdown-menu dropdown-menu-right"
                                                    aria-labelledby="dropdownMenu2">
                                                    <button class="dropdown-item">
                                                        <a class="text-info"
                                                            href="{{ route('admin.accounts.single', $account->ref_id) }}">
                                                            <i class="fas fa-pen-square mr-1"></i> View
                                                        </a>
                                                    </button>
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                @endforeach
                            @endif
                        </tbody>

                        <tfoot>
                            <tr>
                                <th>S/N</th>
                                <th>Account No.</th>
                                <th>Account Type</th>
                                <th>Customer</th>
                                <th>Status</th>
                                <th>Created on</th>
                                <th>Actions</th>
                            </tr>
                        </tfoot>
                    </table>
                </div>
                <!-- /.card-body -->
            </div>
        </div>
    </div>
@stop
@section('page-plugin')
    <script src="{{ asset('assets/plugins/datatables/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('assets/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js') }}"></script>
    <script src="{{ asset('assets/plugins/datatables-responsive/js/dataTables.responsive.min.js') }}"></script>
    <script src="{{ asset('assets/plugins/datatables-responsive/js/responsive.bootstrap4.min.js') }}"></script>
    <script src="{{ asset('assets/plugins/datatables-buttons/js/dataTables.buttons.min.js') }}"></script>
    <script src="{{ asset('assets/plugins/datatables-buttons/js/buttons.bootstrap4.min.js') }}"></script>
    <script src="{{ asset('assets/plugins/jszip/jszip.min.js') }}"></script>
    <script src="{{ asset('assets/plugins/pdfmake/pdfmake.min.js') }}"></script>
    <script src="{{ asset('assets/plugins/pdfmake/vfs_fonts.js') }}"></script>
    <script src="{{ asset('assets/plugins/datatables-buttons/js/buttons.html5.min.js') }}"></script>
    <script src="{{ asset('assets/plugins/datatables-buttons/js/buttons.print.min.js') }}"></script>
    <script src="{{ asset('assets/plugins/datatables-buttons/js/buttons.colVis.min.js') }}"></script>
@stop
@section('page-scripts')
    <script>
        $("#example1").DataTable({
            "responsive": true,
            "lengthChange": false,
            "autoWidth": false,
            "buttons": ["copy", "csv", "excel", "pdf", "print", "colvis"]
        }).buttons().container().appendTo('#example1_wrapper .col-md-6:eq(0)');
    </script>
@stop
