@extends('layouts.admin-master')

@section('page-title', 'Dashboard')

@section('plugin-styles')
@stop

@section('page-styles')
@stop

@section('content-header', 'Dashboard')

@section('content')
    <div class="row">
        <div class="col-12 col-sm-12 col-md-8">
            <!-- TABLE: LATEST ORDERS -->
            <div class="card">
                <div class="card-header border-transparent">
                    <h3 class="card-title">Recent Deposits</h3>

                    <div class="card-tools">
                        <button type="button" class="btn btn-tool" data-card-widget="collapse">
                            <i class="fas fa-minus"></i>
                        </button>
                        <button type="button" class="btn btn-tool" data-card-widget="remove">
                            <i class="fas fa-times"></i>
                        </button>
                    </div>
                </div>
                <!-- /.card-header -->
                <div class="card-body p-0">
                    <div class="table-responsive">
                        <table class="table m-0">
                            <thead>
                            <tr>
                                <th>Ref ID</th>
                                <th>Investor</th>
                                <th>Amount</th>
                                <th>Date</th>
                                <th>Status</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach ($deposits as $deposit)
                                <tr>
                                    <td>
{{--                                        <a href="{{ route('admin.investment_details', $invest->ref_id) }}">{{ $invest->ref_id }}</a>--}}
                                        <a href="#">{{ $deposit->ref_id }}</a>
                                    </td>
                                    <td>{{ $deposit->user->first_name }} {{ $deposit->user->last_name }}</td>
                                    <td class=""><i
                                            class="fas fa-dollar-sign mr-2"></i>{{ $deposit->amount }}</td>
                                    <td>{{ $deposit->created_at->toFormattedDateString() }}</td>
                                    <td>
                                        @switch($deposit->status)
                                            @case(0)
                                            <span class="badge badge-info">pending</span>
                                            @break
                                            @case(2)
                                            <span class="badge badge-danger">failed</span>
                                            @break
                                            @default
                                            <span class="badge badge-primary">completed</span>
                                        @endswitch
                                    </td>
                                </tr>
                            @endforeach

                            </tbody>
                        </table>
                    </div>
                    <!-- /.table-responsive -->
                </div>
                <!-- /.card-body -->
                <div class="card-footer clearfix">
                    <a href="javascript:void(0)" class="btn btn-sm btn-light float-left">&nbsp;</a>
                    <a href="{{route('admin.deposits')}}" class="btn btn-sm btn-secondary float-right">View all deposits</a>
                </div>
                <!-- /.card-footer -->
            </div>
            <!-- /.card -->
        </div>
        <div class="col-12 col-sm-12 col-md-4">
            <!-- PRODUCT LIST -->
            <div class="card">
                <div class="card-header">
                    <h3 class="card-title">Recently Registered Customers</h3>

                    <div class="card-tools">
                        <button type="button" class="btn btn-tool" data-card-widget="collapse">
                            <i class="fas fa-minus"></i>
                        </button>
                        <button type="button" class="btn btn-tool" data-card-widget="remove">
                            <i class="fas fa-times"></i>
                        </button>
                    </div>
                </div>
                <!-- /.card-header -->
                <div class="card-body p-0">
                    <ul class="products-list product-list-in-card pl-2 pr-2">
                        @foreach ($users as $user)
                            <li class="item">
                                <div class="product-img">
                                    <img src="{{$user->avatar}}" alt="investor avatar" class="img-size-50">
                                </div>
                                <div class="product-info">
{{--                                     <a href="javascript:void(0)" class="product-title">{{$investor->name}}--}}
                                    <a href="#"
                                       class="product-title">{{ $user->first_name }} {{ $user->last_name }}
                                        <span class="badge badge-primary float-right">
                                            {{$user->accountType}}
                                        </span>
                                    </a>
                                    <span class="product-description">
                                        {{ $user->countryName }}.
                                    </span>
                                </div>
                            </li>
                        @endforeach
                    </ul>
                </div>
                <!-- /.card-body -->
                <div class="card-footer text-center">
                    <a href="{{ route('admin.customers') }}" class="uppercase">View all customers<i
                            class="fas fa-chevron-right ml-2"></i></a>
                </div>
                <!-- /.card-footer -->
            </div>
            <!-- /.card -->
        </div>

    </div>

@stop

@section('page-plugin')
@stop
@section('page-scripts')
@stop
