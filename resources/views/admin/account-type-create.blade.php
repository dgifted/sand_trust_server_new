@extends('layouts.admin-master')

@section('page-title', 'Add Account Type')
@section('plugin-styles')
@stop
@section('page-styles')
@stop
@section('content-header', 'Add Account Type')

@section('content')
    <div class="row">
        <div class="col-md-9">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header">
                            <div class="d-flex justify-content-between">
                                <h3 class="card-title text-lg mt-1 text-capitalize">Add Account Type</h3>
                            </div>
                        </div>
                        <!-- /.card-header -->
                        @if($errors->any())
                            <div class="alert alert-warning alert-dismissible fade show mt-2 mx-2" role="alert">
                                @foreach ($errors->all() as $msg)
                                    <i class="fas fa-exclamation text-danger mr-2"></i>{{ $msg }}
                                @endforeach
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                        @endif
                        <div class="card-body">
                            <form action="{{route('admin.account-types')}}" method="POST">
                                @csrf

                                <hr class="mt-3">
                                <div class="row p-2">
                                    <div class="col-12 col-md-6">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label for="name">Name</label>
                                                    <input type="text" name="title" class="form-control" id="title"
                                                           placeholder="Enter account type name"
                                                           value="{{old('title') ?? ''}}">
                                                </div>
                                            </div>

                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label for="description">Description</label>
                                                    <textarea name="description" class="form-control" id="description"
                                                              placeholder="Enter account type description">{{old('description') ?? ''}}</textarea>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <hr class="mt-1">
                                <div class="row mt-2 p-2">
                                    <div class="col-md-6">
                                        <div class="input-group">
                                            <button type="submit" id="submit" class="btn btn-md btn-primary">
                                                Save changes
                                                <i class="far fa-check-circle ml-2"></i>
                                            </button>
                                        </div>
                                    </div>
                                </div>
                                {{--                                <input type="hidden" name="ref_id" value="{{$type->ref_id}}">--}}
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop
@section('page-plugin')
@stop
@section('page-scripts')
@stop
