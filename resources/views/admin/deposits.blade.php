@extends('layouts.admin-master')

@section('page-title', 'Customers')
@section('plugin-styles')
    <link rel="stylesheet" href="{{ asset('assets/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css')}}">
    <link rel="stylesheet" href="{{ asset('assets/plugins/datatables-responsive/css/responsive.bootstrap4.min.css')}}">
    <link rel="stylesheet" href="{{ asset('assets/plugins/datatables-buttons/css/buttons.bootstrap4.min.css')}}">
@stop
@section('page-styles')

@stop
@section('content-header', 'Customers')

@section('content')
    <div id="eventList" class="row">
        <div class="col-12">
            <div class="card">
                <!-- /.card-header -->
                <div class="card-body">
                    <table id="example1" class="table table-bordered table-striped">
                        <thead>
                        <tr>
                            <th>S/N</th>
                            <th>Deposited By</th>
                            <th>Amount</th>
                            <th>Deposited On</th>
                            <th>Status</th>
                            <th>Actions</th>
                        </tr>
                        </thead>
                        <tbody>
                        @php
                            $count = 0;
                        @endphp
                        @if($deposits->count() > 0)
                            @foreach($deposits as $deposit)
                                @php
                                    $count++;
                                @endphp
                                <tr>
                                    <td>{{$count}}</td>
                                    <td>
                                        {{$deposit->user->first_name}} {{$deposit->user->last_name}}
                                    </td>
                                    <td>
                                        <i class="text-uppercase">{{$deposit->user->accounts->first()->currency->abbreviation ?? ''}}</i>
                                        {{$deposit->amount}}
                                    </td>
                                    <td>{{$deposit->created_at->toFormattedDateString()}}</td>
                                    <td>
                                        @switch($deposit->status)
                                            @case(0)
                                            <span class="badge badge-warning">Pending</span>
                                            @break
                                            @case(1)
                                            <span class="badge badge-success">Completed</span>
                                            @break
                                            @case(2)
                                            <span class="badge badge-danger">Failed</span>
                                            @break
                                        @endswitch
                                    </td>
                                    <td>
                                        <div class="dropdown">
                                            <button class="btn btn-light dropdown-toggle" type="button"
                                                    id="dropdownMenu2" data-toggle="dropdown" aria-haspopup="true"
                                                    aria-expanded="false">
                                                <i class="fas fa-ellipsis-v"></i>
                                            </button>

                                            <div class="dropdown-menu dropdown-menu-right"
                                                 aria-labelledby="dropdownMenu2">
                                                @switch($deposit->status)
                                                    @case(0)
                                                    <button class="dropdown-item">
                                                        <a class="text-info"
                                                           href="{{route('admin.deposits.approve', [$deposit->user->uid, $deposit->ref_id])}}">
                                                            <i class="fas fa-pen-square mr-1"></i>Approve
                                                        </a>
                                                    </button>
                                                    @break
                                                    @case(1)
                                                    <button class="dropdown-item">
                                                        <a class="text-info"
                                                           href="{{route('admin.deposits.decline', [$deposit->user->uid, $deposit->ref_id])}}">
                                                            <i class="fas fa-pen-square mr-1"></i>Decline
                                                        </a>
                                                    </button>
                                                    @break
                                                    @case(2)
                                                    <button class="dropdown-item">
                                                        <a class="text-info"
                                                           href="{{route('admin.deposits.approve', [$deposit->user->uid, $deposit->ref_id])}}">
                                                            <i class="fas fa-pen-square mr-1"></i>ReApprove
                                                        </a>
                                                    </button>
                                                    @break
                                                @endswitch
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                            @endforeach
                        @endif
                        </tbody>

                        <tfoot>
                        <tr>
                            <th>S/N</th>
                            <th>Deposited By</th>
                            <th>Amount</th>
                            <th>Deposited On</th>
                            <th>Status</th>
                            <th>Actions</th>
                        </tr>
                        </tfoot>
                    </table>
                </div>
                <!-- /.card-body -->
            </div>
        </div>
    </div>
@stop
@section('page-plugin')
    <script src="{{asset('assets/plugins/datatables/jquery.dataTables.min.js')}}"></script>
    <script src="{{asset('assets/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js')}}"></script>
    <script src="{{asset('assets/plugins/datatables-responsive/js/dataTables.responsive.min.js')}}"></script>
    <script src="{{asset('assets/plugins/datatables-responsive/js/responsive.bootstrap4.min.js')}}"></script>
    <script src="{{asset('assets/plugins/datatables-buttons/js/dataTables.buttons.min.js')}}"></script>
    <script src="{{asset('assets/plugins/datatables-buttons/js/buttons.bootstrap4.min.js')}}"></script>
    <script src="{{asset('assets/plugins/jszip/jszip.min.js')}}"></script>
    <script src="{{asset('assets/plugins/pdfmake/pdfmake.min.js')}}"></script>
    <script src="{{asset('assets/plugins/pdfmake/vfs_fonts.js')}}"></script>
    <script src="{{asset('assets/plugins/datatables-buttons/js/buttons.html5.min.js')}}"></script>
    <script src="{{asset('assets/plugins/datatables-buttons/js/buttons.print.min.js')}}"></script>
    <script src="{{asset('assets/plugins/datatables-buttons/js/buttons.colVis.min.js')}}"></script>
@stop
@section('page-scripts')
    <script>
        $("#example1").DataTable({
            "responsive": true, "lengthChange": false, "autoWidth": false,
            "buttons": ["copy", "csv", "excel", "pdf", "print", "colvis"]
        }).buttons().container().appendTo('#example1_wrapper .col-md-6:eq(0)');
    </script>
@stop
