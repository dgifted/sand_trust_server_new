@php
$mailAddress = "mailto:info@" . config('app.url');
@endphp

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
    <title>Account created</title>
</head>

<body style="background-color: #f4f6f7">
<div class="container" style="margin-top: 20px;">
    <div class="row">

        <div class="col-md-3 col-lg-3 col-xm-12"></div>

        <div class="col-md-6 col-lg-5 col-xm-12" style="text-align: center">
            <img src="{{asset('assets/imgs/logo_blue.png')}}" class="img-responsive img-fluid"> <br>
        </div>

        <div class="col-md-3 col-lg-3 col-xm-12"></div>
    </div>

    <div class="row">
        <div class="col-md-2 col-lg-2 col-xm-12"></div>
        <div class="col-md-8 col-lg-8 col-xm-12"
             style="background-color: white; color: black; border-radius: 20px; text-align: justify-all; font-size: 1.4em;">
            <br>
            <strong>Hello {{$user->first_name}} {{$user->last_name}},</strong> <br>

            <p>This is to inform you that your account has been created successfully and will be activated soon <br>
                Kindly make deposit once activated</p>


            <p>Find below the details of Account Login</p>

            <strong>Account Number : {{$account->number}}</strong><br>
            <strong>Password : {{$password}}</strong><br>
{{--            <strong>Pin : 67xx771 </strong><br>--}}
            <p></p>
            <p>{{config('app.name')}} will NEVER send you a link to any external website or request your personal
                banking details via e-mail, telephone or in person. You are advised to always keep your log-on details safe and never disclose it to anyone. <br> Thank
                you for choosing {{config('app.name')}}.</p><br>
            <hr>


            <em> The Information contained and transmitted
                by this E-MAIL is proprietary to {{config('app.name')}} and/or its Customer and is intended for use only by the individual or entity to which it is addressed,
                and may contain information that is privileged, confidential or exempt from
                a disclosure under applicable law. If this is a forwarded message, the con
                tent of this E-MAIL may not have been sent with the authority of the Bank.
                {{config('app.name')}}  shall not be liable for any mails sent without due authorisation or through unauthorised access. If you are not the intended recipient,
                an agent of the intended recipient or a person responsible for delivering the information to the named recipient, you are notified that any use, distribution, transmission, printing, copying or dissemination of this information in any way or in any manner is strictly prohibited. If you have received
                this communication in error, please delete this mail and notify us immediately at <a href="{!! $mailAddress !!}">info@ {!! config('app.url') !!}</a> </em>
            <p></p><br>

        </div>


        <div class="col-md-2 col-lg-2 col-xm-12"></div>

    </div>
    <br>
    <p></p>

    <p style="margin-left: 200px;">{{config('app.url')}}</p>
</div>
</body>

</html>
