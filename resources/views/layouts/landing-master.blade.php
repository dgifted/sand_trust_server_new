<!DOCTYPE html>
<html data-wf-domain="" data-wf-page=" 5cf0968c612e87ba3849efca" data-wf-site="5cf0968c612e876b4a49efc9" lang="en">
<head>
    <meta charset="utf-8"/>
    <title>{{config('app.name')}} Bank | @yield('page-title')</title>
    <link rel="stylesheet" href="{{asset('assets/css/bootstrap.min.css')}}">
    <meta
        content="The entrepreneurial bank for Ottawa. We are responsive, creative, street smart, financial growth hunters who give individuals and owners of small-to-medium sized businesses confidence in their possibilities."
        name="description"/>
    <meta content="{{config('app.name')}} Bank" property="og:title"/>
    <meta
        content="The entrepreneurial bank for Ottawa. We are responsive, creative, street smart, financial growth hunters who give individuals and owners of small-to-medium sized businesses confidence in their possibilities."
        property="og:description"/>
    <!-- <meta content="https://assets.website-files.com/5cf0968c612e876b4a49efc9/5d51de4e2ca59a54469245f9_BSB_SEO_Home.jpg" property="og:image"/> -->
    <meta content="summary" name="twitter:card"/>
    <meta content="width=device-width, initial-scale=1" name="viewport"/>
    {{--    <meta content="hydqsqRX7ZTp5097H-f-FoY63U51Vuw_aFVuFj2YegE" name="google-site-verification"/>--}}
    @yield('page-meta')
    <link
        href="{{asset('assets/css/http_bluskyonlinebank.com_assets.website-files.com_5cf0968c612e876b4a49efc9_css_blueskybank.48010be93.min.css')}}"
        rel="stylesheet" type="text/css"/>
    <script src="{{asset('assets/js/http_bluskyonlinebank.com_use.typekit.net_tts2vet.js')}}"
            type="text/javascript"></script>
    <script type="text/javascript">try {
            Typekit.load();
        } catch (e) {
        }</script>
<!--[if lt IE 9]>
    <script src="{{url('https://cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.3/html5shiv.min.js')}}"
            type="text/javascript"></script><![endif]-->
    <script
        type="text/javascript">!function (o, c) {
            var n = c.documentElement, t = " w-mod-";
            n.className += t + "js", ("ontouchstart" in o || o.DocumentTouch && c instanceof DocumentTouch) && (n.className += t + "touch")
        }(window, document);</script>
    <link
        href="https://bluskyonlinebank.com/assets.website-files.com/5cf0968c612e876b4a49efc9/5d130f61111260574dc4f750_favicon.png"
        rel="shortcut icon" type="image/x-icon"/>
    <link href="https://bluskyonlinebank.com/assets.website-files.com/img/webclip.png" rel="apple-touch-icon"/>
    <link rel="stylesheet" href="{{asset('assets/css/http_bluskyonlinebank.com_use.typekit.net_tts2vet.css')}}">

    <!-- CSS -->
    <link rel="stylesheet" href="{{asset('assets/css/styles.css')}}"/>

    <script src="{{asset('assets/js/http_bluskyonlinebank.com_code.jquery.com_jquery-3.3.1.min.js')}}"
            type="text/javascript"></script>
    <script>
        $(document).on('click', 'a', function (e) {
            var skip = !!e.target.attributes['data-goodbye-skip'];
            if (!skip && this.href.indexOf('http') == 0 && this.href.indexOf(location.hostname) == -1) {
                var siteName = "{{config('app.name')}}";
                return confirm(siteName + ' Bank does not control external websites linked to this website, and does not guarantee the accuracy, completeness, efficiency or timeliness of the information contained therein. Be aware that the privacy and security policy of the linked website is not that of' + siteName + '  Bank.');
            }
        });
    </script>
    @yield('page-styles')
</head>
<body class="body">
@include('includes.landing.header')
@yield('content')
@include('includes.landing.footer')
{{--<script src="https://bluskyonlinebank.com/d3e54v103j8qbb.cloudfront.net/js/jquery-3.4.1.min.220afd743d.js"--}}
{{--        type="text/javascript" integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo="--}}
{{--        crossorigin="anonymous"></script>--}}
<script
    src="{{asset('assets/js/http_bluskyonlinebank.com_assets.website-files.com_5cf0968c612e876b4a49efc9_js_blueskybank.d11ecfc37.js')}}"
    type="text/javascript"></script>
<!--[if lte IE 9]>
<script src="{{url('//cdnjs.cloudflare.com/ajax/libs/placeholders/3.0.2/placeholders.min.js')}}"></script><![endif]-->
<!-- Google Tag Manager (noscript) -->
<noscript>
    <iframe src="https://www.googletagmanager.com/ns.html?id=GTM-M7RKB66" height="0" width="0"
            style="display:none;visibility:hidden"></iframe>
</noscript>

<script>
    var d = new Date();
    document.getElementById("date").innerHTML = d.getFullYear();
</script>

@yield('page-scripts')
</body>
</html>
