<!doctype html>
<html class="no-js" lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>{{config('app.name')}} - @yield('title')</title>

    <link rel="shortcut icon" href="{{asset('assets/type_2/images/fav.png')}}" type="image/x-icon">
    <link rel="stylesheet" href="{{asset('assets/type_2/css/bootstrap.min.css')}}">
    <link rel="stylesheet" href="{{asset('assets/type_2/css/fontawesome.min.css')}}">
    <link rel="stylesheet" href="{{asset('assets/type_2/css/jquery-ui.css')}}">
    <link rel="stylesheet" href="{{asset('assets/type_2/css/plugin/nice-select.css')}}">
    <link rel="stylesheet" href="{{asset('assets/type_2/css/plugin/slick.css')}}">
    <link rel="stylesheet" href="{{asset('assets/type_2/css/arafat-font.css')}}">
    <link rel="stylesheet" href="{{asset('assets/type_2/css/plugin/animate.css')}}">
    <link rel="stylesheet" href="{{asset('assets/type_2/css/style.css')}}">
</head>

<body>
<!-- start preloader -->
<div class="preloader" id="preloader"></div>
<!-- end preloader -->

<!-- Scroll To Top Start-->
<a href="javascript:void(0)" class="scrollToTop"><i class="fas fa-angle-double-up"></i></a>
<!-- Scroll To Top End -->

@include('includes.landing2.header')
@yield('content')
@include('includes.landing2.footer')

<!--==================================================================-->
<script src="{{asset('assets/type_2/js/jquery.min.js')}}"></script>
<script src="{{asset('assets/type_2/js/jquery-ui.js')}}"></script>
<script src="{{asset('assets/type_2/js/bootstrap.min.js')}}"></script>
<script src="{{asset('assets/type_2/js/fontawesome.js')}}"></script>
<script src="{{asset('assets/type_2/js/plugin/slick.js')}}"></script>
<script src="{{asset('assets/type_2/js/plugin/jquery.nice-select.min.js')}}"></script>
<script src="{{asset('assets/type_2/js/plugin/wow.min.js')}}"></script>
<script src="{{asset('assets/type_2/js/plugin/plugin.js')}}"></script>
<script src="{{asset('assets/type_2/js/main.js')}}"></script>

<script>
    var subForm = document.getElementById('subscribe-form');
    subForm.addEventListener('submit', (evt) => {
        evt.preventDefault();

        var emailInput = document.getElementById('subEmail');
        var submitButton = document.getElementById('submit');
        var originalBtnText = submitButton.innerText;

        var subscriberEmail = emailInput.value;
        if (!subscriberEmail) return;

        submitButton.innerText = 'Please wait ...';

        setTimeout(() => {
            submitButton.innerText = originalBtnText;
            emailInput.value = '';
        }, 1800);
    });
</script>
</body>
</html>
