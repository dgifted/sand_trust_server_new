@extends('layouts.landing-master2')
@section('title', 'Affiliates')

@section('content')
    <!-- banner-section start -->
    <section class="banner-section inner-banner affiliate">
        <div class="overlay">
            <div class="banner-content d-flex align-items-center">
                <div class="container">
                    <div class="row justify-content-center">
                        <div class="col-lg-10 col-md-10">
                            <div class="main-content">
                                <div class="section-text text-center">
                                    <h1 class="title">Give <span>Credit</span>, Get <span>Credit!</span></h1>
                                    <p class="xlr">Refer ------ Reward ------ Repeat</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="bottom-box pb-120">
            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-lg-12">
                        <div class="main-content d-flex align-items-center justify-content-between">
                            <div class="left-side">
                                <img src="{{asset('assets/type_2/images/affiliate-banner-1.png')}}" alt="image">
                            </div>
                            <div class="mid-side text-center d-flex align-items-center justify-content-center">
                                <div class="content-text">
                                    <h3>$25</h3>
                                    <p>Per Friend</p>
                                </div>
                            </div>
                            <div class="left-side">
                                <img src="{{asset('assets/type_2/images/affiliate-banner-2.png')}}" alt="image">
                            </div>
                        </div>
                        <div class="btn-area text-center">
                            <a href="{{ route('register') }}" class="cmn-btn">Refer Now!</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- banner-section end -->

    <!-- Refer Bankios In start -->
    <section class="account-feature affiliate">
        <div class="overlay pt-120 pb-120">
            <div class="container wow fadeInUp">
                <div class="row justify-content-center">
                    <div class="col-lg-8">
                        <div class="section-header text-center">
                            <h5 class="sub-title">Make money through your network</h5>
                            <h2 class="title">What goes around, comes around, and we take it seriously!</h2>
                            <p>You got credit, now it’s time to give your friends some credit too. With {{config('app.name')}}’s Refer-a-Friend program, you can earn a $25 referral reward when your friend activates a {{config('app.name')}} credit card using your link. What’s more, your friend gets $25 credited to their {{config('app.name')}} credit card balance too!</p>
                        </div>
                    </div>
                </div>
                <div class="row cus-mar">
                    <div class="col-md-4">
                        <div class="single-box">
                            <div class="icon-box">
                                <img src="{{asset('assets/type_2/images/icon/remittance-1.png')}}" alt="icon">
                            </div>
                            <h5>Rewarding Commissions</h5>
                            <p>Get paid for every new friend that joins</p>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="single-box">
                            <div class="icon-box">
                                <img src="{{asset('assets/type_2/images/icon/get-loan-2.png')}}" alt="icon">
                            </div>
                            <h5>Simple and Free</h5>
                            <p>Anyone can participate</p>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="single-box">
                            <div class="icon-box">
                                <img src="{{asset('assets/type_2/images/icon/dashboard.png')}}" alt="icon">
                            </div>
                            <h5>Personalized Dashboard</h5>
                            <p>Share, monitor and track your referrals all in one place</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- Refer Bankios In end -->

    <!-- How it works In start -->
    <section class="how-it-works">
        <div class="overlay pt-120 pb-120">
            <div class="container wow fadeInUp">
                <div class="row justify-content-center">
                    <div class="col-md-7">
                        <div class="section-header text-center">
                            <h5 class="sub-title">Refer {{config('app.name')}} credit card and earn $25!</h5>
                            <h2 class="title">How it Works</h2>
                            <p>It's easier than you think. Follow the following simple easy steps</p>
                        </div>
                    </div>
                </div>
                <div class="abs-item">
                    <div class="row align-items-center">
                        <div class="col-md-6">
                            <div class="text-area">
                                <div class="img-area">
                                    <img src="{{asset('assets/type_2/images/icon/how-works-affiliate-1.png')}}" alt="image">
                                </div>
                                <h4>Share Your Unique Referral Link With Friends</h4>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="abs-contant">
                                <img src="{{asset('assets/type_2/images/how-it-works-ellipse.png')}}" class="contant-bg" alt="image">
                                <div class="contant-area">
                                    <p>Share the below link with your friends to earn the rewards!</p>
                                    <div class="input-area d-flex align-items-center justify-content-between">
                                        <input type="text" disabled value="{{config('app.name')}}.com/get/yU78has7">
                                        <img src="{{asset('assets/type_2/images/icon/copy-icon.png')}}" alt="icon">
                                    </div>
                                    <div class="share-item text-center">
                                        <a href="javascript:void(0)">
                                            <img src="{{asset('assets/type_2/images/icon/share-icon.png')}}" alt="icon">
                                            Share Link
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="mid-contant pt-120 pb-120">
                    <div class="row align-items-center">
                        <div class="col-md-6 cus-ord">
                            <div class="img-area">
                                <img src="{{asset('assets/type_2/images/card-with-bg.png')}}" alt="image">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="text-area">
                                <div class="img-area">
                                    <img src="{{asset('assets/type_2/images/icon/how-works-affiliate-2.png')}}" alt="image">
                                </div>
                                <h4>Your friends have to sign up and activate their {{config('app.name')}} Credit Card</h4>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="abs-item">
                    <div class="row align-items-center">
                        <div class="col-md-6">
                            <div class="text-area">
                                <div class="img-area">
                                    <img src="{{asset('assets/type_2/images/icon/how-works-affiliate-3.png')}}" alt="image">
                                </div>
                                <h4>Both you and your friend get $25 credited to your {{config('app.name')}} credit card balance!</h4>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="abs-contant">
                                <img src="{{asset('assets/type_2/images/how-it-works-ellipse.png')}}" class="contant-bg" alt="image">
                                <div class="contant-area congratulation text-center">
                                    <h4>Congratulation</h4>
                                    <h6>You’ve Earned</h6>
                                    <h3>$25</h3>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- How it works In end -->

    <!-- Call to action In start -->
    <section class="call-action card-page affiliate">
        <div class="overlay pt-120">
            <div class="container wow fadeInUp">
                <div class="row d-flex justify-content-center">
                    <div class="col-lg-10">
                        <div class="main-content">
                            <div class="section-header text-center">
                                <h5 class="sub-title">Even better, your friends will earn $25 too!</h5>
                                <h2 class="title">Invite Your Friends & Earn $25 for Each Referral</h2>
                                <p>Sign in to your account for your personalized link and share it with friends. Once they apply for a {{config('app.name')}} account, you both get paid!</p>
                            </div>
                            <div class="btn-area text-center">
                                <a href="{{ route('register') }}" class="cmn-btn">Refer a friend now!</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- Call to action In end -->

    @include('includes.landing2.get-started')
@endsection
