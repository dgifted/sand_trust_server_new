@extends('layouts.landing-master2')
@section('title', 'Contact Us')

@section('content')
    <!-- banner-section start -->
    <section class="banner-section inner-banner contact">
        <div class="overlay">
            <div class="banner-content d-flex align-items-center">
                <div class="container">
                    <div class="row justify-content-start">
                        <div class="col-lg-7 col-md-10">
                            <div class="main-content">
                                <h1>Contact Us</h1>
                                <div class="breadcrumb-area">
                                    <nav aria-label="breadcrumb">
                                        <ol class="breadcrumb d-flex align-items-center">
                                            <li class="breadcrumb-item"><a href="index-2.html">Home</a></li>
                                            <li class="breadcrumb-item active" aria-current="page">Contact Us</li>
                                        </ol>
                                    </nav>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- banner-section end -->

    <!-- Apply for a loan In start -->
    <section class="apply-for-loan contact">
        <div class="overlay pt-120 pb-120">
            <div class="container wow fadeInUp">
                <div class="row justify-content-center">
                    <div class="col-lg-8">
                        <div class="section-header text-center">
                            <h2 class="title">Get in touch with us.</h2>
                            <p>Fill up the form and our team will get back to you within 24 hours</p>
                        </div>
                    </div>
                </div>
                <div class="row justify-content-center">
                    <div class="col-lg-10">
                        <div class="form-content">
                            <form action="#">
                                <div class="row">
                                    <div class="col-6">
                                        <div class="single-input">
                                            <label for="name">Name</label>
                                            <input type="text" id="name" placeholder="What's your name?">
                                        </div>
                                    </div>
                                    <div class="col-6">
                                        <div class="single-input">
                                            <label for="email">Email</label>
                                            <input type="text" id="email" placeholder="What's your email?">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-6">
                                        <div class="single-input">
                                            <label for="phone">Phone</label>
                                            <input type="text" id="phone" placeholder="(123) 480 - 3540">
                                        </div>
                                    </div>
                                    <div class="col-6">
                                        <div class="single-input">
                                            <label for="loan">Service interested in</label>
                                            <input type="text" id="loan" placeholder="Ex. Auto Loan, Home Loan">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-12">
                                        <div class="single-input">
                                            <label for="message">Message</label>
                                            <textarea id="message" placeholder="I would like to get in touch with you..." cols="30" rows="10"></textarea>
                                        </div>
                                    </div>
                                </div>
                                <div class="btn-area text-center">
                                    <button class="cmn-btn">Send Message</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- Apply for a loan In end -->

    <!-- Need more help In start -->
    <section class="account-feature loan-feature need-more-help">
        <div class="overlay pt-120 pb-120">
            <div class="container wow fadeInUp">
                <div class="row justify-content-center">
                    <div class="col-lg-8">
                        <div class="section-header text-center">
                            <h5 class="sub-title">You can reach out to us for all your</h5>
                            <h2 class="title">Need More Help?</h2>
                            <p>Queries, complaints and feedback. We will be happy to serve you</p>
                        </div>
                    </div>
                </div>
                <div class="row cus-mar">
                    <div class="col-md-4">
                        <div class="single-box">
                            <div class="icon-box">
                                <img src="{{asset('assets/type_2/images/icon/need-help-1.png')}}" alt="icon">
                            </div>
                            <h5>Sales</h5>
                            <p><a href="https://pixner.net/cdn-cgi/l/email-protection" class="__cf_email__" data-cfemail="0e7d6f626b7d4e6c6f60656761206d6163">[email&#160;protected]</a></p>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="single-box">
                            <div class="icon-box">
                                <img src="{{asset('assets/type_2/images/icon/need-help-2.png')}}" alt="icon">
                            </div>
                            <h5>Help & Support</h5>
                            <p><a href="https://pixner.net/cdn-cgi/l/email-protection" class="__cf_email__" data-cfemail="d4a7a1a4a4bba6a0a794b6b5babfbdbbfab7bbb9">[email&#160;protected]</a></p>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="single-box">
                            <div class="icon-box">
                                <img src="{{asset('assets/type_2/images/icon/need-help-3.png')}}" alt="icon">
                            </div>
                            <h5>Media & Press</h5>
                            <p><a href="https://pixner.net/cdn-cgi/l/email-protection" class="__cf_email__" data-cfemail="99f4fcfdf0f8d9fbf8f7f2f0f6b7faf6f4">[email&#160;protected]</a></p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- Need more help In end -->
    @include('includes.landing2.faq')
    @include('includes.landing2.get-started')
@endsection
