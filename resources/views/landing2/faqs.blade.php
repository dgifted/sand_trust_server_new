@extends('layouts.landing-master2')
@section('title', 'FAQs')

@section('content')
    <!-- banner-section start -->
    <section class="banner-section inner-banner faqs">
        <div class="overlay">
            <div class="banner-content d-flex align-items-center">
                <div class="container">
                    <div class="row justify-content-start">
                        <div class="col-lg-7 col-md-10">
                            <div class="main-content">
                                <h1>Faqs</h1>
                                <div class="breadcrumb-area">
                                    <nav aria-label="breadcrumb">
                                        <ol class="breadcrumb d-flex align-items-center">
                                            <li class="breadcrumb-item"><a href="{{ route('landing-home') }}">Home</a></li>
                                            <li class="breadcrumb-item active" aria-current="page">Faqs</li>
                                        </ol>
                                    </nav>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- banner-section end -->

    <!-- FAQs In start -->
    <section class="faqs-section">
        <div class="overlay pt-120 pb-120">
            <div class="container wow fadeInUp">
                <div class="row d-flex justify-content-center">
                    <div class="col-lg-6">
                        <div class="section-header text-center">
                            <h5 class="sub-title">If you have question,we have answer</h5>
                            <h2 class="title">Frequently asked questions</h2>
                            <p>Get answers to all questions you have and boost your knowledge so you can save, invest
                                and spend smarter.</p>
                        </div>
                    </div>
                </div>
                <div class="d-flex justify-content-center">
                    <ul class="nav nav-tabs" id="myTab" role="tablist">
                        <li class="nav-item" role="presentation">
                            <button class="nav-link active" id="account-tab" data-bs-toggle="tab"
                                    data-bs-target="#account" type="button"
                                    role="tab" aria-controls="account" aria-selected="true">account
                            </button>
                        </li>
                        <li class="nav-item" role="presentation">
                            <button class="nav-link" id="affiliates-tab" data-bs-toggle="tab"
                                    data-bs-target="#affiliates" type="button"
                                    role="tab" aria-controls="affiliates" aria-selected="false">affiliates
                            </button>
                        </li>
                        <li class="nav-item" role="presentation">
                            <button class="nav-link" id="security-tab" data-bs-toggle="tab" data-bs-target="#security"
                                    type="button"
                                    role="tab" aria-controls="security" aria-selected="false">security
                            </button>
                        </li>
                    </ul>
                </div>
                <div class="tab-content" id="myTabContent">
                    <div class="tab-pane fade show active" id="account" role="tabpanel" aria-labelledby="account-tab">
                        <h4>Account</h4>
                        <div class="row d-flex justify-content-center">
                            <div class="col-xl-8">
                                <div class="faq-box">
                                    <div class="accordion" id="accordionExample">
                                        <div class="accordion-item">
                                            <h5 class="accordion-header" id="headingTwo">
                                                <button class="accordion-button collapsed" type="button"
                                                        data-bs-toggle="collapse" data-bs-target="#collapseTwo"
                                                        aria-expanded="false" aria-controls="collapseTwo">
                                                    How do I locate the nearesty branch or ATM?
                                                </button>
                                            </h5>
                                            <div id="collapseTwo" class="accordion-collapse collapse"
                                                 aria-labelledby="headingTwo" data-bs-parent="#accordionExample">
                                                <div class="accordion-body">
                                                    <p>If your card is missing, let us know immediately. We’ll block
                                                        your card right away send over a new one on the same day.To
                                                        report a lost or stolen card, call us at (406) 555-0120.</p>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="accordion-item">
                                            <h5 class="accordion-header" id="headingThree">
                                                <button class="accordion-button collapsed" type="button"
                                                        data-bs-toggle="collapse" data-bs-target="#collapseThree"
                                                        aria-expanded="false" aria-controls="collapseThree">
                                                    What do I do if I lose my card or it gets stolen?
                                                </button>
                                            </h5>
                                            <div id="collapseThree" class="accordion-collapse collapse"
                                                 aria-labelledby="headingThree" data-bs-parent="#accordionExample">
                                                <div class="accordion-body">
                                                    <p>If your card is missing, let us know immediately. We’ll block
                                                        your card right away send over a new one on the same day.To
                                                        report a lost or stolen card, call us at (406) 555-0120.</p>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="accordion-item">
                                            <h5 class="accordion-header" id="headingFour">
                                                <button class="accordion-button collapsed" type="button"
                                                        data-bs-toggle="collapse" data-bs-target="#collapseFour"
                                                        aria-expanded="false" aria-controls="collapseFour">
                                                    What is your customer service number?
                                                </button>
                                            </h5>
                                            <div id="collapseFour" class="accordion-collapse collapse"
                                                 aria-labelledby="headingFour" data-bs-parent="#accordionExample">
                                                <div class="accordion-body">
                                                    <p>If your card is missing, let us know immediately. We’ll block
                                                        your card right away send over a new one on the same day.To
                                                        report a lost or stolen card, call us at (406) 555-0120.</p>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="accordion-item">
                                            <h5 class="accordion-header" id="headingFive">
                                                <button class="accordion-button collapsed" type="button"
                                                        data-bs-toggle="collapse" data-bs-target="#collapseFive"
                                                        aria-expanded="false" aria-controls="collapseFive">
                                                    How do I reset my pin?
                                                </button>
                                            </h5>
                                            <div id="collapseFive" class="accordion-collapse collapse"
                                                 aria-labelledby="headingFive" data-bs-parent="#accordionExample">
                                                <div class="accordion-body">
                                                    <p>If your card is missing, let us know immediately. We’ll block
                                                        your card right away send over a new one on the same day.To
                                                        report a lost or stolen card, call us at (406) 555-0120.</p>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="accordion-item">
                                            <h5 class="accordion-header" id="headingsix">
                                                <button class="accordion-button collapsed" type="button"
                                                        data-bs-toggle="collapse" data-bs-target="#collapsesix"
                                                        aria-expanded="false" aria-controls="collapsesix">
                                                    What is required to use Digital Banking?
                                                </button>
                                            </h5>
                                            <div id="collapsesix" class="accordion-collapse collapse"
                                                 aria-labelledby="headingsix" data-bs-parent="#accordionExample">
                                                <div class="accordion-body">
                                                    <p>If your card is missing, let us know immediately. We’ll block
                                                        your card right away send over a new one on the same day.To
                                                        report a lost or stolen card, call us at (406) 555-0120.</p>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="accordion-item">
                                            <h5 class="accordion-header" id="headingsaven">
                                                <button class="accordion-button collapsed" type="button"
                                                        data-bs-toggle="collapse" data-bs-target="#collapsesaven"
                                                        aria-expanded="false" aria-controls="collapsesaven">
                                                    Is digital banking secure?
                                                </button>
                                            </h5>
                                            <div id="collapsesaven" class="accordion-collapse collapse"
                                                 aria-labelledby="headingsaven" data-bs-parent="#accordionExample">
                                                <div class="accordion-body">
                                                    <p>If your card is missing, let us know immediately. We’ll block
                                                        your card right away send over a new one on the same day.To
                                                        report a lost or stolen card, call us at (406) 555-0120.</p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane fade" id="affiliates" role="tabpanel" aria-labelledby="affiliates-tab">
                        <h4>Affiliates</h4>
                        <div class="row d-flex justify-content-center">
                            <div class="col-xl-8">
                                <div class="faq-box">
                                    <div class="accordion" id="accordionExample">
                                        <div class="accordion-item">
                                            <h5 class="accordion-header" id="headingTwo">
                                                <button class="accordion-button collapsed" type="button"
                                                        data-bs-toggle="collapse" data-bs-target="#collapseTwo"
                                                        aria-expanded="false" aria-controls="collapseTwo">
                                                    How do I locate the nearesty branch or ATM?
                                                </button>
                                            </h5>
                                            <div id="collapseTwo" class="accordion-collapse collapse"
                                                 aria-labelledby="headingTwo" data-bs-parent="#accordionExample">
                                                <div class="accordion-body">
                                                    <p>If your card is missing, let us know immediately. We’ll block
                                                        your card right away send over a new one on the same day.To
                                                        report a lost or stolen card, call us at (406) 555-0120.</p>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="accordion-item">
                                            <h5 class="accordion-header" id="headingThree">
                                                <button class="accordion-button collapsed" type="button"
                                                        data-bs-toggle="collapse" data-bs-target="#collapseThree"
                                                        aria-expanded="false" aria-controls="collapseThree">
                                                    What do I do if I lose my card or it gets stolen?
                                                </button>
                                            </h5>
                                            <div id="collapseThree" class="accordion-collapse collapse"
                                                 aria-labelledby="headingThree" data-bs-parent="#accordionExample">
                                                <div class="accordion-body">
                                                    <p>If your card is missing, let us know immediately. We’ll block
                                                        your card right away send over a new one on the same day.To
                                                        report a lost or stolen card, call us at (406) 555-0120.</p>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="accordion-item">
                                            <h5 class="accordion-header" id="headingFour">
                                                <button class="accordion-button collapsed" type="button"
                                                        data-bs-toggle="collapse" data-bs-target="#collapseFour"
                                                        aria-expanded="false" aria-controls="collapseFour">
                                                    What is your customer service number?
                                                </button>
                                            </h5>
                                            <div id="collapseFour" class="accordion-collapse collapse"
                                                 aria-labelledby="headingFour" data-bs-parent="#accordionExample">
                                                <div class="accordion-body">
                                                    <p>If your card is missing, let us know immediately. We’ll block
                                                        your card right away send over a new one on the same day.To
                                                        report a lost or stolen card, call us at (406) 555-0120.</p>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="accordion-item">
                                            <h5 class="accordion-header" id="headingFive">
                                                <button class="accordion-button collapsed" type="button"
                                                        data-bs-toggle="collapse" data-bs-target="#collapseFive"
                                                        aria-expanded="false" aria-controls="collapseFive">
                                                    How do I reset my pin?
                                                </button>
                                            </h5>
                                            <div id="collapseFive" class="accordion-collapse collapse"
                                                 aria-labelledby="headingFive" data-bs-parent="#accordionExample">
                                                <div class="accordion-body">
                                                    <p>If your card is missing, let us know immediately. We’ll block
                                                        your card right away send over a new one on the same day.To
                                                        report a lost or stolen card, call us at (406) 555-0120.</p>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="accordion-item">
                                            <h5 class="accordion-header" id="headingsix">
                                                <button class="accordion-button collapsed" type="button"
                                                        data-bs-toggle="collapse" data-bs-target="#collapsesix"
                                                        aria-expanded="false" aria-controls="collapsesix">
                                                    What is required to use Digital Banking?
                                                </button>
                                            </h5>
                                            <div id="collapsesix" class="accordion-collapse collapse"
                                                 aria-labelledby="headingsix" data-bs-parent="#accordionExample">
                                                <div class="accordion-body">
                                                    <p>If your card is missing, let us know immediately. We’ll block
                                                        your card right away send over a new one on the same day.To
                                                        report a lost or stolen card, call us at (406) 555-0120.</p>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="accordion-item">
                                            <h5 class="accordion-header" id="headingsaven">
                                                <button class="accordion-button collapsed" type="button"
                                                        data-bs-toggle="collapse" data-bs-target="#collapsesaven"
                                                        aria-expanded="false" aria-controls="collapsesaven">
                                                    Is digital banking secure?
                                                </button>
                                            </h5>
                                            <div id="collapsesaven" class="accordion-collapse collapse"
                                                 aria-labelledby="headingsaven" data-bs-parent="#accordionExample">
                                                <div class="accordion-body">
                                                    <p>If your card is missing, let us know immediately. We’ll block
                                                        your card right away send over a new one on the same day.To
                                                        report a lost or stolen card, call us at (406) 555-0120.</p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane fade" id="security" role="tabpanel" aria-labelledby="security-tab">
                        <h4>Security</h4>
                        <div class="row d-flex justify-content-center">
                            <div class="col-xl-8">
                                <div class="faq-box">
                                    <div class="accordion" id="accordionExample">
                                        <div class="accordion-item">
                                            <h5 class="accordion-header" id="headingTwo">
                                                <button class="accordion-button collapsed" type="button"
                                                        data-bs-toggle="collapse" data-bs-target="#collapseTwo"
                                                        aria-expanded="false" aria-controls="collapseTwo">
                                                    How do I locate the nearesty branch or ATM?
                                                </button>
                                            </h5>
                                            <div id="collapseTwo" class="accordion-collapse collapse"
                                                 aria-labelledby="headingTwo" data-bs-parent="#accordionExample">
                                                <div class="accordion-body">
                                                    <p>If your card is missing, let us know immediately. We’ll block
                                                        your card right away send over a new one on the same day.To
                                                        report a lost or stolen card, call us at (406) 555-0120.</p>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="accordion-item">
                                            <h5 class="accordion-header" id="headingThree">
                                                <button class="accordion-button collapsed" type="button"
                                                        data-bs-toggle="collapse" data-bs-target="#collapseThree"
                                                        aria-expanded="false" aria-controls="collapseThree">
                                                    What do I do if I lose my card or it gets stolen?
                                                </button>
                                            </h5>
                                            <div id="collapseThree" class="accordion-collapse collapse"
                                                 aria-labelledby="headingThree" data-bs-parent="#accordionExample">
                                                <div class="accordion-body">
                                                    <p>If your card is missing, let us know immediately. We’ll block
                                                        your card right away send over a new one on the same day.To
                                                        report a lost or stolen card, call us at (406) 555-0120.</p>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="accordion-item">
                                            <h5 class="accordion-header" id="headingFour">
                                                <button class="accordion-button collapsed" type="button"
                                                        data-bs-toggle="collapse" data-bs-target="#collapseFour"
                                                        aria-expanded="false" aria-controls="collapseFour">
                                                    What is your customer service number?
                                                </button>
                                            </h5>
                                            <div id="collapseFour" class="accordion-collapse collapse"
                                                 aria-labelledby="headingFour" data-bs-parent="#accordionExample">
                                                <div class="accordion-body">
                                                    <p>If your card is missing, let us know immediately. We’ll block
                                                        your card right away send over a new one on the same day.To
                                                        report a lost or stolen card, call us at (406) 555-0120.</p>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="accordion-item">
                                            <h5 class="accordion-header" id="headingFive">
                                                <button class="accordion-button collapsed" type="button"
                                                        data-bs-toggle="collapse" data-bs-target="#collapseFive"
                                                        aria-expanded="false" aria-controls="collapseFive">
                                                    How do I reset my pin?
                                                </button>
                                            </h5>
                                            <div id="collapseFive" class="accordion-collapse collapse"
                                                 aria-labelledby="headingFive" data-bs-parent="#accordionExample">
                                                <div class="accordion-body">
                                                    <p>If your card is missing, let us know immediately. We’ll block
                                                        your card right away send over a new one on the same day.To
                                                        report a lost or stolen card, call us at (406) 555-0120.</p>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="accordion-item">
                                            <h5 class="accordion-header" id="headingsix">
                                                <button class="accordion-button collapsed" type="button"
                                                        data-bs-toggle="collapse" data-bs-target="#collapsesix"
                                                        aria-expanded="false" aria-controls="collapsesix">
                                                    What is required to use Digital Banking?
                                                </button>
                                            </h5>
                                            <div id="collapsesix" class="accordion-collapse collapse"
                                                 aria-labelledby="headingsix" data-bs-parent="#accordionExample">
                                                <div class="accordion-body">
                                                    <p>If your card is missing, let us know immediately. We’ll block
                                                        your card right away send over a new one on the same day.To
                                                        report a lost or stolen card, call us at (406) 555-0120.</p>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="accordion-item">
                                            <h5 class="accordion-header" id="headingsaven">
                                                <button class="accordion-button collapsed" type="button"
                                                        data-bs-toggle="collapse" data-bs-target="#collapsesaven"
                                                        aria-expanded="false" aria-controls="collapsesaven">
                                                    Is digital banking secure?
                                                </button>
                                            </h5>
                                            <div id="collapsesaven" class="accordion-collapse collapse"
                                                 aria-labelledby="headingsaven" data-bs-parent="#accordionExample">
                                                <div class="accordion-body">
                                                    <p>If your card is missing, let us know immediately. We’ll block
                                                        your card right away send over a new one on the same day.To
                                                        report a lost or stolen card, call us at (406) 555-0120.</p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- FAQs In end -->
@endsection
