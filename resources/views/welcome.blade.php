@extends('layouts.landing-master')
@section('page-title', 'Home')
@section('page-styles')
@stop

@section('content')
    <div data-delay="4000" data-animation="fade" data-autoplay="1" data-easing="ease-in-out" data-duration="500"
         data-infinite="1" class="hero-home w-slider">
        <div class="w-slider-mask">
            <div class="home-hero-slide slide-fox w-slide">


                <div class="container-1200 vertical-center">
                    <h1 class="h1-hero-home max860">THE ENTREPRENEURIAL BANK FOR OTTAWA</h1>
                    <p class="paragraph-hero-home max600">We are responsive, creative, street smart, financial growth
                        hunters who
                        give individuals and owners of small-to-medium sized businesses confidence in their
                        possibilities.</p>
                </div>
                <div class="container-1200 fixed-bottom w-clearfix">
                    <div class="slide-caption w-clearfix">
                        <h4 class="h2-caption">Fox Cleaners</h4>
                        <p class="paragraph-caption">Member since 2005</p>
                    </div>
                </div>
            </div>
            <div class="home-hero-slide slide-mangos w-slide">
                <div class="container-1200 vertical-center">
                    <h1 class="h1-hero-home max860">THE ENTREPRENEURIAL BANK FOR OTTAWA</h1>
                    <p class="paragraph-hero-home max600">We are responsive, creative, street smart, financial growth
                        hunters who
                        give individuals and owners of small-to-medium sized businesses confidence in their
                        possibilities.</p>
                </div>
                <div class="container-1200 fixed-bottom w-clearfix">
                    <div class="slide-caption w-clearfix">
                        <h4 class="h2-caption">Mango&#x27;s Cuban Cafe</h4>
                        <p class="paragraph-caption">Member since 2018</p>
                    </div>
                </div>
            </div>
            <div class="home-hero-slide slide-adorn w-slide">
                <div class="container-1200 vertical-center">
                    <h1 class="h1-hero-home max860">THE ENTREPRENEURIAL BANK FOR OTTAWA</h1>
                    <p class="paragraph-hero-home max600">We are responsive, creative, street smart, financial growth
                        hunters who
                        give individuals and owners of small-to-medium sized businesses confidence in their
                        possibilities.</p>
                </div>
                <div class="container-1200 fixed-bottom w-clearfix">
                    <div class="slide-caption w-clearfix">
                        <h4 class="h2-caption">Adorn Designs</h4>
                        <p class="paragraph-caption">Member since 2017</p>
                    </div>
                </div>
            </div>
            <div class="home-hero-slide slide-bevins w-slide">
                <div class="container-1200 vertical-center">
                    <h1 class="h1-hero-home max860">THE ENTREPRENEURIAL BANK FOR OTTAWA</h1>
                    <p class="paragraph-hero-home max600">We are responsive, creative, street smart, financial growth
                        hunters who
                        give individuals and owners of small-to-medium sized businesses confidence in their
                        possibilities.</p>
                </div>
                <div class="container-1200 fixed-bottom w-clearfix">
                    <div class="slide-caption w-clearfix">
                        <h4 class="h2-caption">Bevins Co</h4>
                        <p class="paragraph-caption">Member since 2011</p>
                    </div>
                </div>
            </div>
        </div>
        <div class="left-arrow-2 w-slider-arrow-left">
            <div class="w-icon-slider-left"></div>
        </div>
        <div class="right-arrow-2 w-slider-arrow-right">
            <div class="w-icon-slider-right"></div>
        </div>
        <div class="slide-nav-2 w-slider-nav w-round"></div>
    </div>
    <div id="section2" class="section120-white">
        <div class="container1200 flex">
            <div class="column-left-large">
                <div class="rich-text-block w-richtext">
                    <h2>Our communities need banks that can do for local individuals and business owners</h2>
                    <p>Smaller businesses drive local job growth and investment. They are the engine of prosperity. They
                        also need
                        different kinds of help to survive and thrive. Their size and unique situations make it tough
                        for big banks
                        to focus on them and offer the kind of products and consulting they need when they need it. Sand
                        Trust Bank
                        embraces this as the ultimate opportunity to do what these banks can’t do as well. Together our
                        customers
                        with us will make a lasting, local impact. </p>
                </div>
            </div>
            <div class="column-right-small center-mobile"><img
                    src="{{asset('assets/imgs/Illustration 1.jpg')}}"
                    {{--                    srcset="https://assets.website-files.com/5cf0968c612e876b4a49efc9/5cf09f1adb04e8ca89fd4c37_Illustration%201-p-500.png 500w, https://assets.website-files.com/5cf0968c612e876b4a49efc9/5cf09f1adb04e8ca89fd4c37_Illustration%201.png 800w"--}}
                    {{--                    sizes="(max-width: 479px) 46vw, (max-width: 767px) 47vw, (max-width: 991px) 38vw, 32vw" alt=""--}}
                    class="image-13"/></div>
        </div>
    </div>
    <div id="section3" class="section120-gray">
        <div class="container1200 flex w-clearfix">
            <div class="column-left-small center-mobile"><img
                    src="{{asset('assets/imgs/Illustration_2-1.png')}}"
                    width="476"
                    {{--                    srcset="https://assets.website-files.com/5cf0968c612e876b4a49efc9/5cf0a09a207f14670bc1067b_Illustration%202-p-800.png 800w, https://assets.website-files.com/5cf0968c612e876b4a49efc9/5cf0a09a207f14670bc1067b_Illustration%202-p-1080.png 1080w, https://assets.website-files.com/5cf0968c612e876b4a49efc9/5cf0a09a207f14670bc1067b_Illustration%202.png 1132w"--}}
                    {{--                    sizes="(max-width: 479px) 46vw, (max-width: 767px) 47vw, (max-width: 991px) 38vw, 32vw" alt=""--}}
                    class="image-3"/></div>
            <div class="column-right-large">
                <div class="rich-text-block w-richtext">
                    <h2>Our competitive advantage is that we are business people <strong
                            class="bold-text-3">first</strong>,
                        bankers second. This changes everything about how we work with you.</h2>
                    <p>Our team spends most of their days in the trenches helping individuals and local businesses
                        owners take new
                        steps. Instead of stock answers and rigid cookie cutter solutions from the big bank ivory tower,
                        our bankers
                        are business people who immerse themselves in the specifics and details of small businesses each
                        day. We ask
                        lots of questions so our answers and options are always geared just for you. We will have new
                        ideas for you
                        that will make a difference in what is possible.</p>
                </div>
            </div>
        </div>
    </div>
    <div id="section2" class="section120-white">
        <div class="container1200 flex">
            <div class="column-left-large">
                <div class="rich-text-block w-richtext">
                    <h2>When was the last time you decided to call your banker <strong
                            class="bold-text-2">first</strong>?</h2>
                    <p>This is how we measure what we do. Most people see banks as something they use after all the
                        thought about
                        what to do next has been done. Our customers see us as the first people they want to talk to
                        when they have
                        a half-formed question or budding idea to grow.<br/>‍<br/>A few of the questions we love
                        getting:</p>
                </div>
                <img
                    src="{{asset('assets/imgs/illustration_3.png')}}"
                    {{--                    srcset="https://assets.website-files.com/5cf0968c612e876b4a49efc9/5cf0a22edb04e8c5c9fd5456_text%20message%20graphic-p-500.png 500w, https://assets.website-files.com/5cf0968c612e876b4a49efc9/5cf0a22edb04e8c5c9fd5456_text%20message%20graphic-p-800.png 800w, https://assets.website-files.com/5cf0968c612e876b4a49efc9/5cf0a22edb04e8c5c9fd5456_text%20message%20graphic-p-1080.png 1080w, https://assets.website-files.com/5cf0968c612e876b4a49efc9/5cf0a22edb04e8c5c9fd5456_text%20message%20graphic.png 1282w"--}}
                    {{--                    sizes="(max-width: 479px) 92vw, (max-width: 767px) 95vw, (max-width: 991px) 51vw, 52vw" alt=""--}}
                    alt=""
                    class="image-5"/>
            </div>
            <div class="column-right-small hide-mobile"><img
                    src="{{asset('assets/imgs/illustration_4.jpg')}}"
                    {{--                    srcset="https://assets.website-files.com/5cf0968c612e876b4a49efc9/5d4c92f78620aecf1137b410_phone-p-500.jpeg 500w, https://assets.website-files.com/5cf0968c612e876b4a49efc9/5d4c92f78620aecf1137b410_phone-p-800.jpeg 800w, https://assets.website-files.com/5cf0968c612e876b4a49efc9/5d4c92f78620aecf1137b410_phone.jpg 840w"--}}
                    {{--                    sizes="(max-width: 767px) 100vw, (max-width: 991px) 38vw, 32vw" alt="" --}}
                    alt=""
                    class="image-4"/>
            </div>
        </div>
    </div>
    <div id="testimonials" class="section120-gray padding0-bottom-mobile">
        <div class="container1200 flex w-clearfix">
            <div class="column-half-left">
                <div class="rich-text-block w-richtext">
                    <h2><strong class="bold-text">Our real offices</strong> are in the businesses and coffee shops of
                        Ottawa.
                    </h2>
                    <p>Our expertise doesn’t come from sitting behind a desk. We’re out there with our customers in many
                        different
                        industries constantly learning, advising, and solving problems with them. Growing businesses are
                        hard and
                        also exhilarating. Banks often sit on the sidelines before and after they provide a loan—we’re
                        always in the
                        field because daily financial decisions make or break the outcome.</p>
                </div>
            </div>
            <div class="column-half-right"></div>
        </div>
        <div class="container-float-right">
            <div data-delay="4000" data-animation="fade" data-autoplay="1" data-easing="ease-in-out" data-duration="500"
                 data-infinite="1" class="slider-2 w-slider">
                <div class="w-slider-mask">
                    <div class="home-quote-slide quote-fox w-slide">
                        <div class="div-block-4">
                            <div class="slide-caption caption-left">
                                <h4 class="h2-caption no-margin">Fox Cleaners</h4>
                            </div>
                            <div class="slide-quote">
                                <p class="paragraph-quote">“We wanted that community connection and service that you
                                    just don’t get from
                                    a megabank. We’ve had the white-glove treatment from {{config('app.name')}} Bank
                                    from the very
                                    beginning. They’re
                                    always just a phone call away.”<br/><span class="text-span">– Maggie Fox, Owner, Fox Cleaners</span>
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="home-quote-slide quote-mangos w-slide">
                        <div class="div-block-4">
                            <div class="slide-caption caption-left">
                                <h4 class="h2-caption no-margin">Mango&#x27;s Cuban Cafe</h4>
                            </div>
                            <div class="slide-quote">
                                <p class="paragraph-quote">“From Day One, any questions I had, any financial issues that
                                    came up, Sand
                                    Trust Bank was on top of it. My experience with them has been great. I would
                                    recommend them to
                                    anyone.“<br/><span
                                        class="text-span">– Anthony Martinez, Owner, Mangos Cuban Cafe</span></p>
                            </div>
                        </div>
                    </div>
                    <div class="home-quote-slide quote-adorn w-slide">
                        <div class="div-block-4">
                            <div class="slide-caption caption-left">
                                <h4 class="h2-caption no-margin">Adorn Designs</h4>
                            </div>
                            <div class="slide-quote">
                                <p class="paragraph-quote">“Rather than just looking at the numbers, Ryan
                                    at {{config('app.name')}}
                                    Bank looked at
                                    my
                                    business idea and my business plan as a whole. He saw the whole picture. It was so
                                    refreshing.”<br/><span
                                        class="text-span">– Whitney Eslicker, Owner, Adorn Designs</span></p>
                            </div>
                        </div>
                    </div>
                    <div class="home-quote-slide quote-bevins w-slide">
                        <div class="div-block-4">
                            <div class="slide-caption caption-left">
                                <h4 class="h2-caption no-margin">Bevins Co</h4>
                            </div>
                            <div class="slide-quote">
                                <p class="paragraph-quote">“They’re willing to work with me, they’re flexible, they get
                                    things done.
                                    They like to say ‘yes.’ Anybody who asks me, I tell them about Sand
                                    Trust.“<br/><span
                                        class="text-span">– Rich Bevins, Owner, <strong>Bevins Co</strong></span></p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="left-arrow-3 w-slider-arrow-left">
                    <div class="w-icon-slider-left"></div>
                </div>
                <div class="right-arrow-3 w-slider-arrow-right">
                    <div class="w-icon-slider-right"></div>
                </div>
                <div class="slide-nav-2 w-slider-nav w-round"></div>
            </div>
        </div>
    </div>
    <div id="section2" class="section120-white">
        <div class="container1200 flex">
            <div class="column-left-large">
                <div class="rich-text-block w-richtext">
                    <h2>We don’t expect you to switch banks, just start a conversation. No strings attached.</h2>
                    <p>You already have a bank. Probably one that isn’t very involved. You’ve never thought a bank could
                        be more
                        than a bank. They are more a necessary hassle, than any sort of valued resource.<br/>‍<br/>There
                        is no
                        pressure talking with us and absolutely never a sales pitch. We dive straight into your
                        challenges and
                        opportunities and you’ll quickly see what difference our approach makes.<br/>‍<br/>If you decide
                        to open
                        an account, no matter the size, this allows us to get even closer. In many cases, you will be
                        able to secure
                        a line of credit with us. You should have more than one if possible.</p>
                </div>
            </div>
            <div class="column-right-small center-mobile"><img
                    src="{{asset('assets/imgs/illustration_5.png')}}"
                    {{--                    srcset="https://assets.website-files.com/5cf0968c612e876b4a49efc9/5cf0a42c612e8786884a2512_Illustration%204-p-500.png 500w, https://assets.website-files.com/5cf0968c612e876b4a49efc9/5cf0a42c612e8786884a2512_Illustration%204.png 800w"--}}
                    {{--                    sizes="(max-width: 479px) 46vw, (max-width: 767px) 47vw, (max-width: 991px) 38vw, 32vw" alt=""--}}
                    alt=""
                    class="image-6"/>
            </div>
        </div>
    </div>
    <div id="section3" class="section120-gray">
        <div class="container1200 flex w-clearfix">
            <div class="column-left-small center-mobile"><img
                    src="{{asset('assets/imgs/oklahoma-map.jpg')}}"
                    {{--                    srcset="https://assets.website-files.com/5cf0968c612e876b4a49efc9/5e4610fdad715c72cea55670_oklahoma-map-p-500.jpeg 500w, https://assets.website-files.com/5cf0968c612e876b4a49efc9/5e4610fdad715c72cea55670_oklahoma-map-p-1080.jpeg 1080w, https://assets.website-files.com/5cf0968c612e876b4a49efc9/5e4610fdad715c72cea55670_oklahoma-map.jpg 1240w"--}}
                    {{--                    sizes="(max-width: 479px) 46vw, (max-width: 767px) 47vw, (max-width: 991px) 38vw, 32vw" alt=""--}}
                    class="image-12" alt=""/>
            </div>
            <div class="column-right-large">
                <div class="rich-text-block w-richtext">
                    <h2>An Ottawa bank founded over a 100 years ago and on a mission to build our communities one at a
                        time for
                        the next 100.</h2>
                    <p>We’ve been deeply rooted in the cities and small towns of Ottawa for over a century. Our mission
                        today is
                        to make these communities even stronger by making access to capital and real world expert advice
                        easily
                        obtainable. This is the way we all will become more prosperous and have more to invest in the
                        places where
                        we live and work. No other bank is built to serve local people and business owners the way we
                        can.</p>
                </div>
            </div>
        </div>
        <div class="container1200 flex"><img
                src="{{asset('assets/imgs/timeline.png')}}"
                {{--                srcset="https://assets.website-files.com/5cf0968c612e876b4a49efc9/5e460a4f1572a70df3901c18_timeline-p-500.png 500w, https://assets.website-files.com/5cf0968c612e876b4a49efc9/5e460a4f1572a70df3901c18_timeline-p-800.png 800w, https://assets.website-files.com/5cf0968c612e876b4a49efc9/5e460a4f1572a70df3901c18_timeline.png 1200w"--}}
                {{--                sizes="(max-width: 479px) 92vw, (max-width: 767px) 95vw, (max-width: 991px) 96vw, (max-width: 1237px) 97vw, 1200px"--}}
                alt="" class="image-9"/>
        </div>
    </div>
    <div id="section2" class="section bg-gradient">
        <div class="container-8900">
            <div class="form-contact w-form">
                @include('includes.landing.contact-form')
            </div>
        </div>
    </div>
@stop

@section('page-scripts')
@endsection
