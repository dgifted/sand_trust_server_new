<?php

use Illuminate\Support\Facades\Route;

Route::get('/', function () {
    return view('welcome');
})->name('landing-home');

//Route::redirect('/login', config('app.front_url'))->name('login');
//Route::get('/login', function () {
//    return redirect(config('app.front_url'));
//})->name('login');

Route::prefix('/about-us')->group(function () {
    Route::view('/in-the-news', 'landing.about.in-the-news')->name('in-the-news');
});
Route::view('/blog', 'landing.blog')->name('blog');
Route::prefix('business-banking')->group(function () {
    Route::view('/business-checking', 'landing.business.checking')->name('business-checking');
    Route::view('/credit-card', 'landing.business.credit-cards')->name('business-credit-cards');
    Route::view('/savings-and-investments', 'landing.business.saving-and-investment')->name('business-savings');
    Route::view('/treasury-management', 'landing.business.treasury-mgt')->name('business-treasury');
    Route::view('/cdars', 'landing.business.cdar')->name('business-cdars');
    Route::view('/business-loans', 'landing.business.loans')->name('business-loans');
    Route::view('/online-banking', 'landing.business.online-banking')->name('business-online-banking');
    Route::view('/other-services', 'landing.business.other-biz')->name('business-other-services');
});
Route::view('contact-us','landing.contact')->name('contact');
Route::prefix('leadership')->group(function () {
    Route::view('board-of-directors', 'landing.leadership.board-of-directors')->name('boards');
    Route::view('executive-team', 'landing.leadership.executive-team')->name('executives');
});
Route::prefix('personal-banking')->group(function () {
    Route::view('/personal-checking', 'landing.personal.checking')->name('personal-checking');
    Route::view('/savings-and-investments', 'landing.personal.saving-and-investment')->name('personal-savings');
    Route::view('/cdars', 'landing.personal.cdar')->name('personal-cdars');
    Route::view('/credit-card', 'landing.personal.credit-card')->name('personal-credit-cards');
    Route::view('/personal-loans', 'landing.personal.loans')->name('personal-loans');
    Route::view('/online-services', 'landing.personal.online-services')->name('personal-online-services');
    Route::view('/other-services', 'landing.business.other-biz')->name('personal-other-services');
});
Route::view('security', 'landing.security')->name('security');

Route::get('/login', function () {
    return redirect(config('app.front_url') . '/login');
})->name('login');
Route::get('/register', function () {
    return redirect(config('app.front_url') . '/register');
})->name('register');

//Admin routes
Route::get('/admin/login', [\App\Http\Controllers\Auth\LoginController::class, 'showLoginForm'])->name('admin.login');
Route::post('/admin/login', [\App\Http\Controllers\Auth\LoginController::class, 'login']);
Route::post('/admin/logout', [\App\Http\Controllers\Auth\LoginController::class, 'logout'])->name('admin.logout');

Route::prefix('admin')->middleware(['auth', 'admin'])->group(function () {
    Route::get('/', [\App\Http\Controllers\Web\AdminController::class, 'index'])->name('home');
    Route::get('/customers/{uid}', [\App\Http\Controllers\Web\CustomerController::class, 'show'])->name('admin.customers.single');
    Route::post('/customers/{uid}/change-email', [\App\Http\Controllers\Web\CustomerController::class, 'changeUserEmailAddress']);
    Route::get('/customers', [\App\Http\Controllers\Web\CustomerController::class, 'index'])->name('admin.customers');
    Route::post('/customers/{uid}/deposit', [\App\Http\Controllers\Web\DepositController::class, 'store'])->name('admin.customers.single.deposit');
    Route::post('/customers/{uid}/deduct', [\App\Http\Controllers\Web\DepositController::class, 'deductFromBalance'])->name('admin.customers.single.deductt');


    Route::get('/accounts/{refId}', [\App\Http\Controllers\Web\AccountController::class, 'show'])->name('admin.accounts.single');
    Route::get('/accounts/{refId}/activate', [\App\Http\Controllers\Web\AccountController::class, 'activate'])->name('admin.accounts.single.activate');
    Route::get('/accounts/{refId}/deactivate', [\App\Http\Controllers\Web\AccountController::class, 'deActivate'])->name('admin.accounts.single.deactivate');
    Route::post('/accounts/{refId}/change-status', [\App\Http\Controllers\Web\AccountController::class, 'changeStatus'])->name('admin.accounts.single.change-status');
    Route::get('/accounts', [\App\Http\Controllers\Web\AccountController::class, 'index'])->name('admin.accounts');

    Route::post('/account-types/{refId}/update', [\App\Http\Controllers\Web\AccountTypeController::class, 'update'])->name('admin.account-types.single.update');
    Route::get('/account-types/{refId}/delete', [\App\Http\Controllers\Web\AccountTypeController::class, 'destroy'])->name('admin.account-types.single.delete');
    Route::get('/account-types/create', [\App\Http\Controllers\Web\AccountTypeController::class, 'create'])->name('admin.account-types.create');
    Route::get('/account-types/{refId}', [\App\Http\Controllers\Web\AccountTypeController::class, 'show'])->name('admin.account-types.single');
    Route::get('/account-types', [\App\Http\Controllers\Web\AccountTypeController::class, 'index'])->name('admin.account-types');
    Route::post('/account-types', [\App\Http\Controllers\Web\AccountTypeController::class, 'store']);

    Route::get('/cards', [\App\Http\Controllers\Web\CardController::class, 'showAllCards'])->name('admin.cards');
    Route::get('/card-requests', [\App\Http\Controllers\Web\CardController::class, 'showAllCardRequest'])->name('admin.card-requests');
    Route::get('/decline-card-requests/{id}', [\App\Http\Controllers\Web\CardController::class, 'declineCardRequest'])->name('admin.card-requests-decline');
    Route::get('/approve-card-requests/{id}', [\App\Http\Controllers\Web\CardController::class, 'redirectToNewCardPage'])->name('admin.card-requests-approve');
    Route::post('/approve-card-requests/{id}', [\App\Http\Controllers\Web\CardController::class, 'approveCardRequest']);
    Route::get('/card/{id}/activate', [\App\Http\Controllers\Web\CardController::class, 'activateCard'])->name('admin.card-activate');
    Route::get('/card/{id}/deactivate', [\App\Http\Controllers\Web\CardController::class, 'deactivateCard'])->name('admin.card-deactivate');

    Route::get('/credit-limit-request', [\App\Http\Controllers\Web\CreditLimitRequestController::class, 'index'])->name('admin.credit-limit-requests');
    Route::get('/credit-limit-request/{id}/approve', [\App\Http\Controllers\Web\CreditLimitRequestController::class, 'approve'])->name('admin.credit-limit-requests-approve');
    Route::get('/credit-limit-request/{id}/decline', [\App\Http\Controllers\Web\CreditLimitRequestController::class, 'approve'])->name('admin.credit-limit-requests-decline');

    Route::get('/deposits/{userId}/{refId}/approve', [\App\Http\Controllers\Web\DepositController::class, 'approve'])->name('admin.deposits.approve');
    Route::get('/deposits/{userId}/{refId}/decline', [\App\Http\Controllers\Web\DepositController::class, 'disApprove'])->name('admin.deposits.decline');
    Route::get('/deposits', [\App\Http\Controllers\Web\DepositController::class, 'index'])->name('admin.deposits');

    Route::get('/emails/new', [\App\Http\Controllers\Web\GenericEmailController::class, 'createNewEmail'])->name('admin.send-new-email');
    Route::get('/emails/{id}', [\App\Http\Controllers\Web\GenericEmailController::class, 'deleteEmail'])->name('admin.sent-emails-delete');
    Route::post('/emails/new', [\App\Http\Controllers\Web\GenericEmailController::class, 'storeNewEmail']);
    Route::get('/emails', [\App\Http\Controllers\Web\GenericEmailController::class, 'index'])->name('admin.sent-emails');

    Route::get('/settings', [\App\Http\Controllers\Web\SettingController::class, 'index'])->name('admin.settings');
    Route::post('/settings', [\App\Http\Controllers\Web\SettingController::class, 'update']);

    Route::get('/support-tickets/{id}/close', [\App\Http\Controllers\Web\SupportTicketController::class, 'closeTicket'])->name('admin.support-tickets-single-close');
    Route::get('/support-tickets/{id}/reopen', [\App\Http\Controllers\Web\SupportTicketController::class, 'reopenTicket'])->name('admin.support-tickets-single-reopen');
    Route::get('/support-tickets/{id}', [\App\Http\Controllers\Web\SupportTicketController::class, 'show'])->name('admin.support-tickets-single');
    Route::post('/support-tickets/{id}', [\App\Http\Controllers\Web\SupportTicketController::class, 'saveReply']);
    Route::get('/support-tickets', [\App\Http\Controllers\Web\SupportTicketController::class, 'index'])->name('admin.support-tickets');

    Route::get('/withdrawals', [\App\Http\Controllers\Web\WithdrawalController::class, 'index'])->name('admin.withdrawals');
    Route::get('/withdrawals/approve/{id}', [\App\Http\Controllers\Web\WithdrawalController::class, 'approve'])->name('admin.withdrawals-approve');
    Route::get('/withdrawals/decline/{id}', [\App\Http\Controllers\Web\WithdrawalController::class, 'decline'])->name('admin.withdrawals-decline');
});

