<?php

use Illuminate\Support\Facades\Route;

Route::prefix('/auth')->group(function () {
    Route::get('/status', [\App\Http\Controllers\Api\AuthController::class, 'getAuthStatus']);
    Route::post('/login', [\App\Http\Controllers\Api\AuthController::class, 'login']);
    Route::post('/logout', [\App\Http\Controllers\Api\AuthController::class, 'logout']);
    Route::post('/register', [\App\Http\Controllers\Api\AuthController::class, 'register']);
    Route::get('/user', [\App\Http\Controllers\Api\AuthController::class, 'getUserDetail']);
    Route::post('/forgot-password', [\App\Http\Controllers\Api\AuthController::class, 'initiatePasswordReset']);
    Route::post('/reset-password/{token}', [\App\Http\Controllers\Api\AuthController::class, 'resetPassword']);
});

Route::prefix('/account')->group(function () {
    Route::get('/user-accounts', [\App\Http\Controllers\Api\Users\UserAccountController::class, 'index']);
    Route::get('/user-single-account/{id}', [\App\Http\Controllers\Api\Users\UserAccountController::class, 'show']);
    Route::get('/user-stats', [\App\Http\Controllers\Api\Users\UserAccountController::class, 'stats']);
});

Route::prefix('/user-bank')->group(function () {
    Route::get('/', [\App\Http\Controllers\Api\Users\UserLocalBankController::class, 'getUserLocalBanks']);
    Route::post('/', [\App\Http\Controllers\Api\Users\UserLocalBankController::class, 'storeBank']);
});

Route::prefix('/card')->group(function () {
    Route::get('/user-cards', [\App\Http\Controllers\Api\Users\UserCardController::class, 'index']);
    Route::get('/user-single-card/{id}', [\App\Http\Controllers\Api\Users\UserCardController::class, 'show']);
    Route::post('/{id}/change-pin', [\App\Http\Controllers\Api\Users\UserCardController::class, 'changePin']);
    Route::get('/{id}/activate', [\App\Http\Controllers\Api\Users\UserCardController::class, 'activateCard']);
    Route::get('/{id}/deactivate', [\App\Http\Controllers\Api\Users\UserCardController::class, 'deActivateCard']);
    Route::get('/active-request', [\App\Http\Controllers\Api\Users\UserCardRequestController::class, 'getActiveCardRequest']);
    Route::post('/request-new', [\App\Http\Controllers\Api\Users\UserCardRequestController::class, 'requestCard']);
    Route::delete('/{id}/cancel-request', [\App\Http\Controllers\Api\Users\UserCardRequestController::class, 'cancelCardRequest']);
});

Route::prefix('credit-limit')->group(function () {
    Route::get('/', [\App\Http\Controllers\Api\Users\UserCreditLimitController::class, 'getUserAccountCreditLimit']);
    Route::post('/request-increase', [\App\Http\Controllers\Api\Users\UserCreditLimitRequestController::class, 'initiate']);
    Route::get('/pending-request', [\App\Http\Controllers\Api\Users\UserCreditLimitRequestController::class, 'getUserPendingCreditLimitRequest']);
});

Route::prefix('/deposit')->group(function () {
    Route::post('/create', [\App\Http\Controllers\Api\Users\UserDepositController::class, 'store']);
});

Route::prefix('/message')->group(function () {
    Route::get('/user-messages', [\App\Http\Controllers\Api\Users\UserMessageController::class, 'index']);
    Route::get('/user-unread-messages', [\App\Http\Controllers\Api\Users\UserMessageController::class, 'getUnreadReceivedMessage']);
    Route::get('/user-single-message/{id}', [\App\Http\Controllers\Api\Users\UserMessageController::class, 'show']);
    Route::delete('/{id}/delete', [\App\Http\Controllers\Api\Users\UserMessageController::class, 'destroy']);
    Route::post('{id}/toggle-read', [\App\Http\Controllers\Api\Users\UserMessageController::class, 'toggleRead']);
});

Route::prefix('/notification')->group(function () {
    Route::get('/recent', [\App\Http\Controllers\Api\Users\UserNotificationController::class, 'recent']);
    Route::get('/user-notifications', [\App\Http\Controllers\Api\Users\UserNotificationController::class, 'index']);
    Route::get('/user-single-notification/{id}', [\App\Http\Controllers\Api\Users\UserNotificationController::class, 'show']);
    Route::delete('{id}/delete', [\App\Http\Controllers\Api\Users\UserNotificationController::class, 'destroy']);
});

Route::prefix('/support')->group(function () {
    Route::get('/', [\App\Http\Controllers\Api\Users\UserSupportTicketController::class, 'getAllUsersSupportTickets']);
    Route::post('/', [\App\Http\Controllers\Api\Users\UserSupportTicketController::class, 'storeNewSupportTicket']);
    Route::get('/{id}/replies', [\App\Http\Controllers\Api\Users\UserSupportTicketController::class, 'getSupportTicketReplies']);
    Route::post('/{id}/replies', [\App\Http\Controllers\Api\Users\UserSupportTicketController::class, 'storeSupportTicketReply']);
});

Route::prefix('/user-withdrawal')->group(function () {
    Route::get('/pending', [\App\Http\Controllers\Api\Users\UserWithdrawalController::class, 'getUserPendingWithdrawals']);
    Route::get('/', [\App\Http\Controllers\Api\Users\UserWithdrawalController::class, 'getAllUserWithdrawals']);
    Route::post('/', [\App\Http\Controllers\Api\Users\UserWithdrawalController::class, 'storeUserWithdrawalRequest']);
});

Route::get('/user-profile', [\App\Http\Controllers\Api\Users\UserProfileController::class, 'index']);
Route::patch('/update-user-profile', [\App\Http\Controllers\Api\Users\UserProfileController::class, 'updateUserData']);
Route::post('/upload-photo', [\App\Http\Controllers\Api\Users\UserProfileController::class, 'uploadPhoto']);
Route::get('/user-preference', [\App\Http\Controllers\Api\Users\UserPreferenceController::class, 'getUserPreference']);

Route::post('/user-preference/update-lang', [\App\Http\Controllers\Api\Users\UserPreferenceController::class, 'updateUserLanguage']);
Route::post('/user-preference/update-theme', [\App\Http\Controllers\Api\Users\UserPreferenceController::class, 'updateUserTheme']);
Route::post('/user-preference/update-notification', [\App\Http\Controllers\Api\Users\UserPreferenceController::class, 'updateUserNotification']);

Route::prefix('/transfer')->group(function () {
    Route::get('/user-transfers', [\App\Http\Controllers\Api\Users\UserTransferController::class, 'index']);
    Route::get('/user-incomplete-transfer', [\App\Http\Controllers\Api\Users\UserTransferController::class, 'getUserInCompleteTransfers']);
    Route::get('/user-single-transfer/{id}', [\App\Http\Controllers\Api\Users\UserTransferController::class, 'show']);
    Route::post('/create', [\App\Http\Controllers\Api\Users\UserTransferController::class, 'store']);
    Route::post('/confirm', [\App\Http\Controllers\Api\Users\UserTransferController::class, 'confirm']);
    Route::post('/{id}/resend-otp', [\App\Http\Controllers\Api\Users\UserTransferController::class, 'regenerateTransferOtp']);
});

Route::prefix('/transaction')->group(function () {
    Route::get('/', [\App\Http\Controllers\Api\Users\UserTransactionController::class, 'index']);
    Route::get('/user-single-transaction/{id}', [\App\Http\Controllers\Api\Users\UserTransactionController::class, 'show']);
});
Route::get('/account-types', [\App\Http\Controllers\Api\MiscController::class, 'getAllAccountTypes']);
Route::get('/countries/{id}/states', [\App\Http\Controllers\Api\MiscController::class, 'getCountryStates']);
Route::get('/countries', [\App\Http\Controllers\Api\MiscController::class, 'getCountries']);
Route::get('/currencies', [\App\Http\Controllers\Api\MiscController::class, 'getCurrencies']);
Route::get('/site-settings', [\App\Http\Controllers\Api\MiscController::class, 'getSiteSettings']);
Route::get('/card-types', [\App\Http\Controllers\Api\MiscController::class, 'getAllCardType']);
Route::get('/site-logo', [\App\Http\Controllers\Api\MiscController::class, 'getSiteLogo']);
