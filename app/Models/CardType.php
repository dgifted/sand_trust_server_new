<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CardType extends Model
{
    use HasFactory;
    const AVAILABLE = 1;
    const ARCHIVED = 2;

    protected $guarded = ['id'];
    protected $hidden = ['created_at', 'updated_at'];

    public function cards()
    {
        return $this->hasMany(Card::class, 'card_type_id');
    }

    public function cardRequests()
    {
        return $this->hasMany(CardRequest::class, 'card_type_id');
    }
}
