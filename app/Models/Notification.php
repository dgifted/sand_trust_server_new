<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Notification extends Model
{
    use HasFactory;
    const UNREAD = 0;
    const READ = 1;

    protected $guarded = ['id'];
    protected $appends = ['creation_time', 'summary'];

    public function getCreationTimeAttribute()
    {
        return $this->getAttribute('created_at')->diffForHumans();
    }

    public function getSummaryAttribute()
    {
        $description = $this->getAttribute('description');
        return substr($description, 0, 40) . '...';
    }

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }
}
