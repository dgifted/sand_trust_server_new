<?php

namespace App\Models;

use App\Traits\Utils;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

class Account extends Model
{
    use HasFactory, Utils;

    const STATUS_INACTIVE = 0;
    const STATUS_ACTIVE = 1;
    const STATUS_DORMANT = 2;
    const STATUS_ON_HOLD = 3;
    const STATUS_SUSPENDED = 4;

    protected $guarded = ['id'];
    protected $appends = ['currency_name', 'status_name', 'type'];

    public function getCurrencyNameAttribute()
    {
        $currency = Currency::find($this->getAttribute('currency_id'));
        if (!$currency)
            return null;

        return $currency->name;
    }

    public function getTypeAttribute()
    {
        $accountType = AccountType::find($this->getAttribute('account_type_id'));
        if (!$accountType)
            return null;

        return $accountType->title;
    }

    public function getStatusNameAttribute()
    {
        switch ($this->getAttribute('status')) {
            case self::STATUS_ACTIVE:
                return 'active';
            case self::STATUS_DORMANT:
                return 'dormant';
            case self::STATUS_ON_HOLD:
                return 'on-hold';
            case self::STATUS_SUSPENDED:
                return 'suspended';
            default:
                return 'inactive';
        }
    }

    public static function generateAccountNumber()
    {
        return (new Account)->generateNumber();
    }

    public static function generateReferenceId()
    {
        return Str::random(16);
    }

    public static function generateSortCode()
    {
        try {
            return random_int(100000, 999999);
        } catch (\Exception $e) {
            return '498760';
        }
    }

    public function accountType()
    {
        return $this->belongsTo(AccountType::class, 'account_type_id');
    }

    public function cards()
    {
        return $this->hasMany(Card::class, 'account_id');
    }

    public function creditLimit()
    {
        return $this->hasOne(CreditLimit::class, 'account_id');
    }

    public function currency()
    {
        return $this->belongsTo(Currency::class, 'currency_id');
    }

    public function transfers()
    {
        return $this->hasMany(Transfer::class, 'source_account_id');
    }

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function withdrawals()
    {
        return $this->hasMany(Withdrawal::class, 'account_id');
    }
}
