<?php

namespace App\Models;

use App\Traits\Utils;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Card extends Model
{
    use HasFactory, Utils;

    const STATUS_ACTIVE = 1;
    const STATUS_INACTIVE = 0;

    protected $guarded = ['id'];
    protected $appends = ['balance', 'type_name'];

    public static function generateCardNumber()
    {
        return (new Card)->generateNumber('card');
    }

    public function account()
    {
        return $this->belongsTo(Account::class, 'account_id');
    }

    public function getBalanceAttribute()
    {
        $account = $this->account()->first();
        return $account->balance;
    }

    public function getTypeNameAttribute()
    {
        $account = $this->account()->first();
        $accountType = $account->accountType()->first();
        return $accountType->title;
    }

    public function cardType()
    {
        return $this->belongsTo(CardType::class, 'card_type_id');
    }

    public function formatCardNumber()
    {
        return $this->cardNumberFormat($this->getAttribute('number'));
    }

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }
}
