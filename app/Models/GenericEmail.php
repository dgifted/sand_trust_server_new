<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class GenericEmail extends Model
{
    use HasFactory;

    protected $guarded = ['id'];

    public function getExcerptAttribute()
    {
        return substr($this->getAttribute('content'), 0, 100) . '...';
    }

    public function recipient()
    {
        return $this->belongsTo(User::class, 'recipient_id');
    }
}
