<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

class Transfer extends Model
{
    use HasFactory;

    const STATUS_PENDING = 0;
    const STATUS_COMPLETED = 1;
    const STATUS_FAILED = 2;

    const TYPE_INCOME = 1;
    const TYPE_EXPENSE = 2;

    protected $guarded = ['id'];
    protected $appends = ['currency_name', 'type_name', 'status_name'];

    public function getCurrencyNameAttribute()
    {
        $currency = Currency::find($this->getAttribute('currency_id'));
        if (!$currency)
            return null;
        return $currency->name;
    }

    public function getStatusNameAttribute()
    {
        $statusText = '';
        switch ($this->getAttribute('status')) {
            case self::STATUS_PENDING:
                $statusText = 'pending';
                break;
            case self::STATUS_COMPLETED:
                $statusText = 'completed';
                break;
            case self::STATUS_FAILED:
                $statusText = 'failed';
                break;
        }
        return $statusText;
    }

    public function getTypeNameAttribute()
    {
        return $this->getAttribute('type') === self::TYPE_INCOME ? 'income' : 'expenses';
    }

    public static function generateReferenceID()
    {
        return Str::random(16);
    }

    public function currency()
    {
        return $this->belongsTo(Currency::class, 'currency_id');
    }

    public function sourceAccount()
    {
        return $this->belongsTo(Account::class, 'source_account_id');
    }

    public function destinationAccount()
    {
        return $this->belongsTo(Account::class, 'payee_account_id');
    }

    public function payer()
    {
        return $this->belongsTo(User::class, 'payer_id');
    }

    public function payee()
    {
        return $this->belongsTo(User::class, 'payee_id');
    }

    public static function generateOtp()
    {
        try {
            return random_int(100000, 999999);
        } catch (\Throwable $exception) {
            return '479375';
        }
    }
}
