<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
    use HasFactory;

    const ROLE_ADMIN = 'admin';
    const ROLE_CLIENT = 'client';
    const ROLE_STAFF = 'staff';


    protected $guarded = ['id'];

    public function users()
    {
        return $this->belongsToMany(User::class, 'role_user_pivot', 'role_id', 'user_id');
    }
}
