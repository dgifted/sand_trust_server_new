<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class LocalBank extends Model
{
    use HasFactory;

    const APPROVAL_PENDING = 0;
    const APPROVED = 1;
    const APPROVAL_CANCELLED = 2;

    protected $guarded = ['id'];

    public function withdrawals()
    {
        return $this->hasMany(Withdrawal::class, 'local_bank_id');
    }

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }
}
