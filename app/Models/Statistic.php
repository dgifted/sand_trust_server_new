<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Statistic extends Model
{
    use HasFactory;

    const TYPE_INCOME = 1;
    const TYPE_EXPENSES = 2;

    protected $guarded = ['id'];
}
