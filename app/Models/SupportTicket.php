<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

class SupportTicket extends Model
{
    use HasFactory;
    const OPEN = 1;
    const CLOSED = 2;

    protected $guarded = ['id'];
    protected $appends = ['excerpt', 'reply_count'];

    public function author()
    {
        return $this->belongsTo(User::class, 'author_id');
    }

    public function getExcerptAttribute()
    {
        return substr($this->getAttribute('content'), 0, 100) . '...';
    }

    public static function generateReferenceId()
    {
        return Str::random(32);
    }

    public function getReplyCountAttribute()
    {
        return $this->replies()->count();
    }

    public function replies()
    {
        return $this->hasMany(SupportReply::class, 'support_ticket_id');
    }
}
