<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Currency extends Model
{
    use HasFactory;

    protected $guarded = ['id'];
    protected $appends = ['symbol'];

    protected $hidden = ['created_at', 'updated_at'];

    public function getSymbolAttribute()
    {
        $abbr = $this->getAttribute('abbreviation');
        switch ($abbr) {
            case 'eur':
                return '&euro;';
            case 'gbp':
                return '&pound;';
            default:
                return '&dollar;';
        }
    }
}
