<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class WithdrawalApprovedMail extends Mailable
{
    use Queueable, SerializesModels;

    public $bank, $user, $withdrawal;

    /**
     * Create a new message instance.
     *
     * @param $withdrawal
     */
    public function __construct($withdrawal)
    {
        $this->bank = $withdrawal->bank;
        $this->user = $withdrawal->account->user;
        $this->withdrawal = $withdrawal;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject('Withdrawal request approved')
            ->view('emails.withdrawal-approved')->with([
                'bank' => $this->bank,
                'user' => $this->user,
                'withdrawal' => $this->withdrawal
            ]);
    }
}
