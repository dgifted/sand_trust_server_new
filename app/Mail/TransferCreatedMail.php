<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class TransferCreatedMail extends Mailable
{
    use Queueable, SerializesModels;

    public $otp, $user;

    /**
     * Create a new message instance.
     *
     * @param $otp
     * @param $user
     */
    public function __construct($otp, $user)
    {
        $this->otp = $otp;
        $this->user = $user;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject('Transfer OTP')
            ->view('emails.transfer-create');
    }
}
