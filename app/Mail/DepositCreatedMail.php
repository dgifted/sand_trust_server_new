<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class DepositCreatedMail extends Mailable
{
    use Queueable, SerializesModels;

    public $amount, $currency, $user;

    /**
     * Create a new message instance.
     *
     * @param $amount
     * @param $currency
     * @param $user
     */
    public function __construct($amount, $currency, $user)
    {
        $this->amount = $amount;
        $this->currency = $currency;
        $this->user = $user;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject('Account funded.')
        ->view('emails.deposit-create');
    }
}
