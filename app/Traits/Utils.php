<?php

namespace App\Traits;

trait Utils
{
    public function generateNumber($for = 'account')
    {
        try {
            $number = '';

            if ($for === 'account') {
                $number = random_int(1000000000, 9999999999);
            } else {
                foreach ([0, 1, 2, 3] as $count) {
                    $digitsGroup = random_int(1000, 9999);
                    $number .= $digitsGroup;
                }
            }
            return $number;
        } catch (\Throwable $ex) {
            return false;
        }
    }

    public function convertTimeTOAMPM($timeString)
    {
        $timeArray = explode(":", $timeString);
        $amPm = ($timeString[0] - 12) > -1 ? 'PM' : 'AM';
        $realHour = 1;

        if (($timeArray[0] - 12) === 0)
            $realHour = 12;
        if (($timeArray[0] - 12) > 0)
            $realHour = $timeArray[0] - 12;
        if (($timeArray[0] - 12) < 0)
            $realHour = $timeArray[0];

        return $realHour . ':' . $timeArray[1] . ' ' . $amPm;
    }

    public function cardNumberFormat($cardNumberRaw)
    {
        $numSplitted = explode('', $cardNumberRaw);
        $buffer = [];

        for ($i = 0; $i < count($numSplitted) + 3; $i++) {
            if ($i == 4 || $i == 8 || $i == 12) {
                array_push($buffer, ' ' . $numSplitted[$i]);
            } else {
                array_push($buffer, $numSplitted[$i]);
            }
        }

        return $buffer;
    }

    public function splitUserName($name)
    {
        $nameArray = explode(' ', $name);
        $firstName = '';
        $otherNames = [];

        foreach ($nameArray as $key => $value) {
            if ($key == 0)
                $firstName = $value;
            else
                $otherNames[] = $value;
        }
        $otherNames = implode(' ', $otherNames);

        return [$firstName, $otherNames];
    }
}
