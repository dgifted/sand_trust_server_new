<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\Controller;
use App\Mail\GenericMail;
use App\Models\GenericEmail;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Str;

class GenericEmailController extends Controller
{
    public function index()
    {
        $emails = GenericEmail::with(['recipient'])->latest()->get();

        return view('admin.sent-emails')->with([
            'emails' => $emails
        ]);
    }

    public function deleteEmail($refId)
    {
        $email = GenericEmail::where('ref_id', $refId)->firstOrFail();
        $email->delete();

        return back()->with([
            'message' => 'Email entry deleted',
            'type' => 'success'
        ]);
    }

    public function createNewEmail()
    {
        $users = User::all()
            ->filter(function ($user) {
                return $user->email !== 'admin@site.io';
            });

        return view('admin.new-email')->with(['users' => $users]);
    }

    public function storeNewEmail(Request $request)
    {
        $request->validate([
            'recipient' => ['required'],
            'subject' => ['required'],
            'content' => ['required'],
        ]);

        $recipient = User::findOrFail($request->get('recipient'));

        $emailSent = false;
        DB::transaction(function () use ($recipient, $request, &$emailSent) {

            $email = $recipient->emails()->create([
                'subject' => $request->get('subject'),
                'content' => $request->get('content'),
                'ref_id' => Str::random(32)
            ]);

            Mail::to($recipient->email)->send(new GenericMail($email->content, $email->subject, $recipient));
            $emailSent = true;
        });

        if (!$emailSent)
            return back()->with([
                'message' => 'Email could not be sent',
                'type' => 'danger'
            ]);

        return back()->with([
            'message' => 'Email sent',
            'type' => 'success'
        ]);
    }
}
