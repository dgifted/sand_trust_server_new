<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\Controller;
use App\Models\CreditLimit;
use App\Models\CreditLimitRequest;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class CreditLimitRequestController extends Controller
{
    public function __construct()
    {
        $this->middleware(['auth:sanctum']);
    }

    public function index()
    {
        $creditLimitRequests = CreditLimitRequest::with(['user.accounts.accountType'])->get();

        return view('admin.credit-limit-requests')->with([
            'creditLimitRequests' => $creditLimitRequests
        ]);

    }

    public function approve($refId)
    {
        $req = $this->findRequestById($refId);
        if (!$req)
            return back()->with([
                'message' => 'Credit limit request not found.',
                'type' => 'danger'
            ]);

        $success = false;
        DB::transaction(function () use (&$req, &$success) {
            $user = $req->user;
            $userCreditLimit = $user->accounts()->first()->creditLimit;
            $userCreditLimit->amount += $req->amount;
            $userCreditLimit->save();

            $req->delete();
            $success = true;

            $user->notificationMessages()->create([
                'title' => 'Credit limit request approval.',
                'description' => 'Your request to increase your credit limit has been approved successfully.'
            ]);
        });

        if (!$success)
            return back()->with([
                'message' => 'Credit limit request approval failed.',
                'type' => 'danger'
            ]);

        return back()->with([
            'message' => 'Credit limit request approved.',
            'type' => 'success'
        ]);
    }

    public function decline($refId)
    {
        $req = $this->findRequestById($refId);

        if (!$req)
            return back()->with([
                'message' => 'Credit limit request not found.',
                'type' => 'danger'
            ]);

        $req->delete();
        $user = $req->user;
        $user->notificationMessages()->create([
            'title' => 'Credit limit request declined.',
            'description' => 'Your request to increase your credit limit has been decline.'
        ]);
    }

    private function findRequestById($refId)
    {
        return CreditLimitRequest::where(['ref_id' => $refId])->first() ?? null;
    }
}
