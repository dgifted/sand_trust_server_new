<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\Controller;
use App\Models\SupportTicket;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;

class SupportTicketController extends Controller
{
    public function closeTicket($refId)
    {
        $ticket = $this->getTicketByRefId($refId);
        $ticket->is_closed = SupportTicket::CLOSED;
        $ticket->save();

        return back()->with([
            'message' => 'Support ticket closed.',
            'type' => 'success'
        ]);
    }

    public function index()
    {
        $tickets = SupportTicket::with(['author:id,first_name,last_name'])->latest()->get();

        return view('admin.support-tickets')->with(['tickets' => $tickets]);
    }

    public function reopenTicket($refId)
    {
        $ticket = $this->getTicketByRefId($refId);
        $ticket->is_closed = SupportTicket::OPEN;
        $ticket->save();

        return back()->with([
            'message' => 'Support ticket reopened.',
            'type' => 'success'
        ]);
    }

    public function show($refId)
    {
        $ticket = $this->getTicketByRefId($refId);
//        dd($ticket->author);
//        dd($ticket->author->created_at->diffInMonths());
        return view('admin.support-ticket-detail')
            ->with(['ticket' => $ticket]);
    }

    public function saveReply(Request $request, $refId)
    {
        $request->validate([
            'content' => ['required']
        ]);

        $ticket = $this->getTicketByRefId($refId);
        $ticket->replies()->create([
            'author_id' => auth()->id(),
            'content' => $request->get('content')
        ]);

        return redirect()
            ->route('admin.support-tickets-single', $refId)
            ->with(['message' => 'Reply posted', 'type' => 'success']);
    }

    private function getTicketByRefId($refId)
    {
        $ticket = SupportTicket::with(['author', 'replies.author:id,first_name,last_name'])
            ->where('ref_id', $refId)
            ->first();

        if (!$ticket)
            throw new ModelNotFoundException('Support ticket not found.', 404);

        return $ticket;
    }
}
