<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\Controller;
use App\Mail\WithdrawalApprovedMail;
use App\Models\Withdrawal;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;

class WithdrawalController extends Controller
{
    public function index()
    {
        $withdrawals = Withdrawal::with(['account.user', 'bank'])->latest()->get();

        return view('admin.withdrawals')->with([
            'withdrawals' => $withdrawals
        ]);
    }

    public function approve($refId)
    {
        $withdrawal = Withdrawal::where('ref_id', $refId)->with(['account.user', 'account.currency', 'bank'])->first();
        if (!$withdrawal)
            return back()->with([
                'message' => 'Withdrawal not found.',
                'type' => 'info'
            ]);

        $success = false;
        DB::transaction(function () use (&$success, &$withdrawal) {
            $userAccount = $withdrawal->account;
            $user = $userAccount->user;

            $userAccount->balance -= $withdrawal->amount;
            $userAccount->save();

            $withdrawal->approval_status = \App\Models\Withdrawal::STATUS_COMPLETED;
            $withdrawal->save();

            $user->notificationMessages()->create([
                'title' => 'Withdrawal approved',
                'description' => 'Your withdrawal of ' .
                    $withdrawal->account->currency->abbreviation .
                    $withdrawal->amount .
                    ' into your account ' .
                    $withdrawal->bank->bank_account_number . '(' . $withdrawal->bank->bank_name . ')' .
                    ' is successful.'
            ]);

            Mail::to($user->email)->send(new WithdrawalApprovedMail($withdrawal));
            $success = true;
        });

        if (!$success)
            return back()->with([
                'message' => 'Withdrawal fail.',
                'type' => 'error'
            ]);

        return back()->with([
            'message' => 'Withdrawal approved.',
            'type' => 'success'
        ]);
    }

    public function decline($refId)
    {
        $withdrawal = Withdrawal::where('ref_id', $refId)->first();
        if (!$withdrawal)
            return back()->with([
                'message' => 'Withdrawal not found.',
                'type' => 'info'
            ]);

        $success = false;
        DB::transaction(function () use (&$success, &$withdrawal) {
            $userAccount = $withdrawal->account;
            $user = $userAccount->user;

            $userAccount->balance -= $withdrawal->amount;
            $userAccount->save();

            $withdrawal->approval_status = \App\Models\Withdrawal::STATUS_COMPLETED;
            $withdrawal->save();

            $user->notificationMessages()->create([
                'title' => 'Withdrawal approved',
                'description' => 'Your withdrawal of ' .
                    $withdrawal->account->currency->abbreviation .
                    $withdrawal->amount .
                    ' into your account ' .
                    $withdrawal->bank->bank_account_number . '(' . $withdrawal->bank->bank_name . ')' .
                    ' has failed.'
            ]);

//            Mail::to($user->email)->send(new WithdrawalApprovedMail($user, $withdrawal));
            $success = true;
        });

        if (!$success)
            return back()->with([
                'message' => 'Withdrawal cancellation  failed.',
                'type' => 'error'
            ]);

        return back()->with([
            'message' => 'Withdrawal cancelled successfully.',
            'type' => 'success'
        ]);
    }
}
