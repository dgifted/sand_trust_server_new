<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\Controller;
use App\Models\AccountType;
use App\Models\Deposit;
use App\Models\User;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;

class CustomerController extends Controller
{
    public function index()
    {
        $customers = User::where('email', '<>', config('app.admin_email'))->get();
        return view('admin.customers')->with(['customers' => $customers]);
    }

    public function show($customerUid)
    {
        $customer = User::where('uid', $customerUid)->with(['accounts.creditLimit', 'accounts.currency'])->firstOrFail();

        $balance = 0;
        $accountType = AccountType::all();

        $customer->accounts()->get()->each(function ($account) use (&$balance) {
            $balance += (double)$account->balance;
        });

        return view('admin.customer-detail')->with([
            'user' => $customer,
            'accountType' => $accountType,
            'balance' => $balance,
        ]);
    }

    public function changeUserEmailAddress(Request $request, $customerUid)
    {
        $request->validate([
            'email' => ['required', 'email']
        ]);

        $customer = User::where('uid', $customerUid)->first();

        if (!$customer)
            return response()->json([
                'message' => 'customer not found.'
            ], 404);

        $customer->email = $request->get('email');
        $customer->save();

        return response()->json([
            'message' => 'Customer email changed.'
        ], 201);
    }
}
