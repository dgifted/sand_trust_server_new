<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\Controller;
use App\Models\Setting;
use Illuminate\Http\Request;

class SettingController extends Controller
{
    public function index()
    {
        $settings = Setting::firstOrCreate([]);

        return view('admin.settings')->with([
            'settings' => $settings
        ]);
    }

    public function update(Request $request)
    {
        $settings = Setting::firstOrCreate([]);

        $data = $request->all();
        $settings->fill($data);
        $settings->save();

        return back()->withMessage('Settings updated');
    }
}
