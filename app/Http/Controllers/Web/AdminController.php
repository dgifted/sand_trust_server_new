<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\Controller;
use App\Models\Deposit;
use App\Models\User;

class AdminController extends Controller
{
    public function index()
    {
        $users = User::doesntHave('roles')->latest()->get()->take(7);
        $users->each(function ($user) {
            $account = $user->accounts()->first()->type ?? 'N/A';
            $user->accountType = $account;
            if (!!$user->country) {
                $user->countryName = explode(':', $user->country)[1];
            } else {
                $user->countryName = 'N/A';
            }
        });
        $deposits = Deposit::where('status', Deposit::STATUS_PENDING)->latest()->get()->take(10);
        return view('admin.home')
            ->with([
                'users' => $users,
                'deposits' => $deposits
            ]);
    }
}
