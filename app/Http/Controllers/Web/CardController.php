<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\Controller;
use App\Models\Card;
use App\Models\CardRequest;
use Illuminate\Http\Request;

class CardController extends Controller
{
    public function showAllCards()
    {
        $cards = Card::with(['user', 'cardType', 'account'])->latest()->get();

        return view('admin.cards')->with([
            'cards' => $cards
        ]);
    }

    public function showAllCardRequest()
    {
        $cardRequests = CardRequest::with(['user.accounts', 'cardType'])->latest()->get();

        return view('admin.card-requests')->with([
            'cards' => $cardRequests
        ]);
    }

    public function redirectToNewCardPage($cardRefId)
    {
        $cardReq = CardRequest::with('user.accounts')->where('ref_id', $cardRefId)->first();
        if (!$cardReq)
            return back()->with([
                'message' => 'Card request not found',
                'type' => 'error'
            ]);

        $accountId = $cardReq->user->accounts->first()->id;
        return view('admin.new-card')->with([
            'cardRefId' => $cardRefId,
            'accountId' => $accountId,
            'cardRequest' => $cardReq
        ]);
    }

    public function approveCardRequest(Request $request, $cardReqRefId)
    {
        $cardReq = CardRequest::where('ref_id', $cardReqRefId)->firstOrFail();

        Card::create([
            'card_type_id' => $cardReq->card_type_id,
            'user_id' => $cardReq->user_id,
            'account_id' => $request->get('accountId'),
            'number' => $request->get('number'),
            'pin' => '0000',
            'cvv' => random_int(000, 999),
            'expires_month' => $request->get('expMonth'),
            'expires_year' => $request->get('expYear'),
        ]);

        $cardReq->delete();

        return redirect()->route('admin.cards')->with([
            'message' => 'Card request with ref id: ' . $cardReq->ref_id . ' has been approved successfully.',
            'type' => 'success'
        ]);
    }

    public function declineCardRequest($cardReqRefId)
    {
        $cardReq = CardRequest::where('ref_id', $cardReqRefId)->firstOrFail();
        $cardReq->deleted();

        return back()->with([
            'message' => 'Card request with ref. id: ' . $cardReq->ref_id . ' has been declined successfully.',
            'type' => 'success'
        ]);
    }

    public function activateCard($cardId)
    {
        $card = $this->findCard($cardId);

        $card->is_active = Card::STATUS_ACTIVE;
        $card->save();

        return back()->with([
            'message' => 'Card activated.',
            'type' => 'success'
        ]);
    }

    public function deactivateCard($cardId)
    {
        $card = $this->findCard($cardId);

        $card->is_active = Card::STATUS_INACTIVE;
        $card->save();

        return back()->with([
            'message' => 'Card deactivated.',
            'type' => 'success'
        ]);
    }

    private function findCard($cardId)
    {
        $card = Card::find($cardId);
        if (!$card)
            return back()->with([
                'message' => 'Card was not found',
                'type' => 'error'
            ]);

        return $card;
    }
}
