<?php

namespace App\Http\Controllers\Api;

use App\Mail\PasswordResetMail;
use App\Mail\UserCreatedMail;
use App\Models\AccountType;
use App\Models\User;
use App\Traits\Utils;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Str;

class AuthController extends ApiBaseController
{
    use Utils;

    public function __construct()
    {
        $this->middleware('auth:sanctum')->except([
            'login',
            'register',
            'initiatePasswordReset',
            'resetPassword'
        ]);
    }

    public function getAuthStatus()
    {
        if (!Auth::check()) {
            return $this->errorResponse('You are not logged in.', 401);
        }

        return $this->showMessage('You are logged in.', 200);
    }

    public function login(Request $request)
    {
        $request->validate([
            'identifier' => ['required', 'email'],
            'password' => ['required']
        ]);

        $user = User::where('email', $request->get('identifier'))->first();
        if (!$user) {
            return $this->errorResponse('Does not exist any user with the provided email.', 404);
        }

        $passwordsMatched = Hash::check($request->get('password'), $user->password);

        if (!$passwordsMatched) {
            return $this->errorResponse('Email/password not valid.', 404);
        }

        $token = $user->createToken('auth')->plainTextToken;
        $payload = (object)[
            'token' => $token,
            'detail' => (object)[
                'email' => $user->email,
                'id' => $user->id
            ]
        ];
        return response()->json($payload, 201);
    }

    public function logout()
    {
        $user = auth()->user();
        $user->tokens()->delete();

        return $this->showMessage('You have been logged out');
    }

    public function register(Request $request)
    {
        $request->validate([
            'fullName' => ['required'],
            'email' => ['required', 'email', 'unique:users'],
            'accountType' => ['required'],
            'currency' => ['required'],
            'dob' => ['required'],
            'gender' => ['required'],
            'address1' => ['required'],
        ]);

        $data = $request->all();
        $names = $this->splitUserName($data['fullName']);
        $password = User::generatePassword();

        $data['password'] = Hash::make($password);
        $data['first_name'] = $names[0];
        $data['last_name'] = $names[1];
        $data['street_address'] = $data['address1'];
        $data['post_code'] = $data['zipCode'];
        $data['uid'] = User::generateUId();
        $data['email_verified_at'] = now();
        $data['marital_status'] = $data['maritalStatus'];
        $data['spouse_name'] = $data['spouseName'];
        $data['address_2'] = $data['address2'];
        $data['monthly_income'] = $data['monthlyIncome'];
        $data['income_source'] = $data['sourceIncome'];
        $data['sex'] = $data['gender'];

        if ($request->hasFile('passport'))
            $data['passport'] = $request->file('passport')->store('', 'images');

        $userCreated = false;
        $user = null;
        $account = null;

        DB::transaction(function () use (&$userCreated, &$data, &$user, &$account, $password) {
            $user = User::create($data);
            $account = $user->accounts()->create([
                'account_type_id' => $data['accountType'],
                'currency_id' => $data['currency'],
                'sort_code' => Str::random(8),
                'number' => \App\Models\Account::generateAccountNumber(),
                'ref_id' => \App\Models\Account::generateReferenceId(),
                'status' => \App\Models\Account::STATUS_INACTIVE
            ]);
            $accountType = AccountType::find($data['accountType']);
            $account->creditLimit()->create([
                'amount' => $accountType->min_credit_limit
            ]);
            Mail::to($user->email)->send(new UserCreatedMail($user, $account, $password));

            $userCreated = true;
        });

        if (!$userCreated)
            return $this->errorResponse('Account creation failed. Please try again.', 422);

        return $this->showOne($user, 201);
    }

    public function updateUserProfile(Request $request)
    {
        $user = auth()->user();

        if ($request->has('firstName') && !!$request->get('firstName'))
            $user->first_name = $request->get('firstName');
        if ($request->has('lastName') && !!$request->get('lastName'))
            $user->last_name = $request->get('lastName');
        if ($request->has('phone') && !!$request->get('phone'))
            $user->phone = $request->get('phone');
        if ($request->has('dob') && !!$request->get('dob'))
            $user->dob = $request->get('dob');
        if ($request->has('streetAddress') && !!$request->get('streetAddress'))
            $user->street_address = $request->get('street_address');
        if ($request->has('postCode') && !!$request->get('postCode'))
            $user->post_code = $request->get('postCode');
        if ($request->has('city') && !!$request->get('city'))
            $user->post_code = $request->get('city');
        if ($request->has('sex') && !!$request->get('sex'))
            $user->post_code = $request->get('sex');
        if ($request->has('country') && !!$request->get('country'))
            $user->post_code = $request->get('country');
        if ($request->has('occupation') && !!$request->get('occupation'))
            $user->post_code = $request->get('occupation');
        if ($request->hasFile('passport'))
            $user->passport = $request->file('passport')->store('', 'public');
        if (!$user->isDirty())
            return $this->errorResponse('You need to change at least one attribute to effect a profile update', 422);
        $user->save();
        return $this->showOne($user);
    }

    public function getUserDetail()
    {
        return $this->showOne(auth()->user());
    }

    public function initiatePasswordReset(Request $request)
    {
        $user = User::where('email', $request->get('email'))->first();
        if (!$user)
            return response()->json(['data' => null], 200);

        $token = Str::random(32);
        $success = false;

        DB::transaction(function () use ($request, $token, $user, &$success) {
            DB::table('password_resets')->insert([
                'email' => $request->get('email'),
                'token' => $token,
                'created_at' => now()
            ]);

            Mail::to($user->email)->send(new PasswordResetMail($user, $token));
            $success = true;
        });

       if ($success)
           return $this->showOne($user, 201);

       return $this->errorResponse('An unexpected error occurred. Please try again.');
    }

    public function resetPassword(Request $request, $token)
    {
        $request->validate([
            'password' => ['required', 'min:8'],
            'passwordConfirm' => ['required']
        ]);

        if ($request->get('password') !== $request->get('passwordConfirm'))
            return $this->errorResponse('Passwords do not match');

        $resetEntry = DB::table('password_resets')->where([
            'token' => $token
        ])->first();

        if (!$resetEntry)
            return $this->errorResponse('Password reset link is invalid', 404);

        $user = User::where('email', $resetEntry->email)->first();

        if (!$user)
            return $this->errorResponse('Unexpected error occurred. Please try again', 500);

        $user->password = Hash::make($request->get('password'));
        $user->save();

        DB::table('password_resets')->where('token', $token)->delete();

        return $this->showOne($user);
    }
}
