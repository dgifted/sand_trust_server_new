<?php

namespace App\Http\Controllers\Api\Users;

use App\Http\Controllers\Api\ApiBaseController;
use App\Mail\DepositCreatedMail;
use App\Models\Deposit;
use App\Models\Setting;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;

class UserDepositController extends ApiBaseController
{
    public function __construct()
    {
        $this->middleware(['auth:sanctum']);
    }

    public function store(Request $request)
    {
        $request->validate([
            'amount' => ['required', 'numeric'],
            'method' => ['required']
        ]);

        $success = false;
        $deposit = null;
        $user = auth()->user();

        DB::transaction(function () use (&$success, &$deposit, $request, $user) {
            $deposit = $user->deposits()->create([
                'amount' => $request->get('amount'),
                'ref_id' => Deposit::generateReferenceID(),
            ]);
            $success = true;
        });

        if (!$success)
            return $this->errorResponse('Operation failed. Please try again.');

        $setting = Setting::firstOrCreate([]);
        $methodName = 'Bitcoin Transfer';
        $methodFlag = 'crypto';

        if ($request->get('method') == 'fiat') {
            $methodName = 'Cash Transfer';
            $methodFlag = 'account';
        }

        $getWay = (object)[
            'name' => $methodName,
            'method' => $methodFlag,
            'accountNumber' => $setting->receiving_account,
            'walletAddress' => $setting->crypto_address
        ];

        Mail::to($user->email)->send(new DepositCreatedMail($request->get('amount'), $getWay, $user));
        return $this->showOne($deposit);
    }
}
