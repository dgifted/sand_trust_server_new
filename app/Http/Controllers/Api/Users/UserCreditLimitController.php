<?php

namespace App\Http\Controllers\Api\Users;

use App\Http\Controllers\Api\ApiBaseController;
use Illuminate\Http\Request;

class UserCreditLimitController extends ApiBaseController
{
    public function __construct()
    {
        $this->middleware(['auth:sanctum']);
    }

    public function getUserAccountCreditLimit()
    {
        $user = auth()->user();
        $creditLimit = $user->accounts()->first()->creditLimit()->firstOrCreate();
        return $this->showOne($creditLimit);
    }
}
