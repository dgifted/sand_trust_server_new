<?php

namespace App\Http\Controllers\Api\Users;

use App\Http\Controllers\Api\ApiBaseController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class UserProfileController extends ApiBaseController
{
    public function __construct()
    {
        $this->middleware(['auth:sanctum']);
    }

    public function index()
    {
        $user = $userDetails = auth()->user();
        $accounts = $user->accounts()->with(['currency', 'creditLimit'])->get();
        $balance = 0;

        $accounts->each(function ($account) use (&$balance) {
            $balance += $account->balance;
        });

        $messagesCount = $user->receivedMessages()->count();

        $data = [
            'user' => $userDetails,
            'stats' => [
                'accsDetails' => [
                    'count' => $accounts->count(),
                    'balance' => $balance,
                    'currency' => $accounts->first()->currency->symbol,
                    'currencyAbbr' => strtoupper($accounts->first()->currency->abbreviation),
                    'creditLimit' => $accounts->first()->creditLimit->amount,
                ],
                'messagesCount' => $messagesCount
            ]
        ];

        return response()->json((object)$data);
    }

    public function updateUserData(Request $request)
    {
        $data = [];
        if (!!$request->get('firstName'))
            $data['first_name'] = $request->get('firstName');
        if (!!$request->get('lastName'))
            $data['last_name'] = $request->get('lastName');
        if (!!$request->get('username'))
            $data['username'] = $request->get('username');

        if (empty($data))
            return $this->errorResponse('You cannot submit an empty field.');

        $user = auth()->user();
        $user->fill($data);
        if (!$user->isDirty())
            return $this->errorResponse('You have to change at least one input field value to effect a profile update.');

        $user->save();
        return $this->showOne($user);
    }

    public function uploadPhoto(Request $request)
    {
        if (!$request->hasFile('photo'))
            return $this->errorResponse('Please choose a valid image file.');

        $user = auth()->user();
        if ($user->passport && Storage::disk('images')->exists($user->passport)) {
            Storage::disk('images')->delete($user->passport);
        }

        $user->passport = $request->file('photo')->store('', 'images');
        $user->save();

        return $this->showOne($user, 201);
    }

//    private function siftRequestData(Request $request)
//    {
//
//    }

}
