<?php

namespace App\Http\Controllers\Api\Users;

use App\Http\Controllers\Api\ApiBaseController;
use App\Models\Card;
use Illuminate\Http\Request;

class UserCardController extends ApiBaseController
{
    public function __construct()
    {
        $this->middleware('auth:sanctum');
    }

    public function index()
    {
        $cards = auth()->user()->cards()->with(['cardType'])->get();
        return $this->showAll($cards);
    }

    public function show($cardId)
    {
        $card = $this->findUserCardById($cardId);

        if (!$card)
            return $this->errorResponse('Card not found.', 404);

        return $this->showOne($card);
    }

    public function changePin(Request $request, $cardId)
    {
        $card = $this->findUserCardById($cardId);
        if (!$card)
            return $this->errorResponse('Card not found.', 404);

        $request->validate([
            'newPin' => ['required', 'size:3']
        ]);

        $card->pin = $request->get('newPin');
        $card->save();

        return $this->showOne($card);
    }

    public function activateCard($cardId)
    {
        $card = $this->findUserCardById($cardId);
        if (!$card)
            return $this->errorResponse('Card not found.', 404);

        $card->is_active = Card::STATUS_ACTIVE;
        $card->save();

        return $this->showOne($card);
    }

    public function deActivateCard($cardId)
    {
        $card = $this->findUserCardById($cardId);
        if (!$card)
            return $this->errorResponse('Card not found.', 404);

        $card->is_active = Card::STATUS_INACTIVE;
        $card->save();

        return $this->showOne($card);
    }

    private function findUserCardById($cardId)
    {
        $cards = auth()->user()->cards()->get()
            ->filter(function ($card) use ($cardId) {
                return (int)$card->id === (int)$cardId;
            });

        if ($cards->count() === 0)
            return null;

        return $cards->first();
    }
}
