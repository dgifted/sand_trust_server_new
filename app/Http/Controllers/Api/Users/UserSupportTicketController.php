<?php

namespace App\Http\Controllers\Api\Users;

use App\Http\Controllers\Api\ApiBaseController;
use App\Models\SupportTicket;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;

class UserSupportTicketController extends ApiBaseController
{
    public function __construct()
    {
        $this->middleware(['auth:sanctum']);
    }

    public function getAllUsersSupportTickets()
    {
        $user = auth()->user();
        $tickets = $user->tickets()->latest()->get();

        return $this->showAll($tickets);
    }

    public function getSupportTicketReplies($ticketId)
    {
        $ticket = $this->findTicketById($ticketId);
        $replies = $ticket->replies()->with(['author:id,first_name,last_name'])->latest()->get();

        return $this->showAll($replies);
    }

    public function storeNewSupportTicket(Request $request)
    {
        $request->validate([
            'title' => ['required'],
            'content' => ['required']
        ]);

        $user = auth()->user();
        $ticket = $user->tickets()->create([
            'ref_id' => SupportTicket::generateReferenceId(),
            'title' => $request->get('title'),
            'content' => $request->get('content')
        ]);

        if ($user->isNotificationOn())
            $user->notificationMessages()->create([
                'title' => 'New support ticket',
                'description' => 'You have just open a new support ticket. Our team members will respond to your need as soon as possible.'
            ]);

        return $this->showOne($ticket, 201);
    }

    public function storeSupportTicketReply(Request $request, $ticketId)
    {
        $ticket = $this->findTicketById($ticketId);
        $reply = $ticket->replies()->create([
            'content' => $request->get('content'),
            'author_id' => auth()->id()
        ]);

        return $this->showOne($reply, 201);
    }

    private function findTicketById($ticketId)
    {
        $user = auth()->user();
        $userTickets  = $user->tickets()->get()->pluck('id')->toArray();
        if (array_search($ticketId, $userTickets) < 0)
            throw new ModelNotFoundException('Ticket with the specified identifier cannot be found.');

         return SupportTicket::findOrFail($ticketId);
    }
}
