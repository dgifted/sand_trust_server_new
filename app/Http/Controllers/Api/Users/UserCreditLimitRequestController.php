<?php

namespace App\Http\Controllers\Api\Users;

use App\Http\Controllers\Api\ApiBaseController;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

class UserCreditLimitRequestController extends ApiBaseController
{
    public function __construct()
    {
        $this->middleware(['auth:sanctum']);
    }

    public function getUserPendingCreditLimitRequest()
    {
        $pendingCreditLimitRequest = auth()->user()->creditLimitRequest()->first();

        if (!$pendingCreditLimitRequest)
            return response()->json(['data' => null], 200);

        return $this->showOne($pendingCreditLimitRequest);
    }

    public function initiate()
    {
        if ($this->checkPendingRequest()[0])
            return $this->errorResponse('You already have a pending request');

        $user = auth()->user();
        $userAccount = $user->accounts()->first();
        $currentUserCreditLimit = $userAccount->creditLimit()->firstOrCreate();
        $userAccountType = $userAccount->accountType;

        if ($userAccountType->max_credit_limit <= $currentUserCreditLimit->amount)
            return $this->errorResponse('You have reached the credit limit for your account type.');

        $creditLimitReq = $user->creditLimitRequest()->firstOrCreate([
            'ref_id' => Str::random(32)
        ]);
        if (($userAccountType->min_credit_limit + $currentUserCreditLimit->amount) > $userAccountType->max_credit_limit) {
            $toppingAmount = $userAccountType->max_credit_limit - $currentUserCreditLimit->amount;
            $creditLimitReq->amount = $toppingAmount;
        } else {
            $creditLimitReq->amount = $userAccountType->min_credit_limit + $currentUserCreditLimit->amount;
        }

        $creditLimitReq->save();

        return $this->showOne($creditLimitReq, 201);
    }

    private function checkPendingRequest()
    {
        $pendingCreditLimitRequest = auth()->user()->creditLimitRequest()->first();
        return [!!$pendingCreditLimitRequest, $pendingCreditLimitRequest];
    }
}
