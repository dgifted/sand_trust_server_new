<?php

namespace App\Http\Controllers\Api\Users;

use App\Http\Controllers\Api\ApiBaseController;
use App\Models\Withdrawal;
use Illuminate\Http\Request;

class UserWithdrawalController extends ApiBaseController
{
    public function __construct()
    {
        $this->middleware(['auth:sanctum']);
    }

    public function getAllUserWithdrawals()
    {
        $user = auth()->user();
        $withdrawals = $user->accounts()->first()->withdrawals()->get();

        return $this->showAll($withdrawals);
    }

    public function getUserPendingWithdrawals()
    {
        $withdrawals = auth()->user()->accounts()->first()->withdrawals()->where([
            'approval_status' => Withdrawal::STATUS_PENDING
        ])->get();

        return $this->showAll($withdrawals);

    }

    public function storeUserWithdrawalRequest(Request $request)
    {
        $request->validate([
            'amount' => ['required'],
            'bank' => ['required']
        ]);
        $userAccount = auth()->user()->accounts()->first();


        if ($request->get('amount') > $userAccount->balance)
            return $this->errorResponse('Withdrawal amount cannot be more than your account balance');

        $withdrawal = $userAccount->withdrawals()->create([
            'local_bank_id' => $request->get('bank'),
            'amount' => $request->get('amount'),
            'ref_id' => \App\Models\Withdrawal::generateRefId()
        ]);

        return $this->showOne($withdrawal, 201);
    }
}
