<?php

namespace App\Http\Controllers\Api\Users;

use App\Http\Controllers\Api\ApiBaseController;
use App\Models\Message;

class UserMessageController extends ApiBaseController
{
    public function __construct()
    {
        $this->middleware('auth:sanctum');
    }

    public function index()
    {
//        $message = auth()->user()->receivedMessages()->get();
        $message = auth()->user()->messages()->get();
        return $this->showAll($message);
    }

    public function show($messageId)
    {
        $message = $this->findUserMessageById($messageId);
        if (!$message)
            return $this->errorResponse('Message not found.', 404);

        return $this->showOne($message);
    }

    public function destroy($messageId)
    {
        $message = $this->findUserMessageById($messageId);
        if (!$message)
            return $this->errorResponse('Message not found.', 404);

        $message->delete();
        return $this->showOne($message);
    }

    public function toggleRead($messageId)
    {
        $message = $this->findUserMessageById($messageId);
        if (!$message)
            return $this->errorResponse('Message not found.', 404);

        $status = $message->is_read;
        $newStatus = $status === Message::STATUS_READ ? Message::STATUS_UNREAD : Message::STATUS_READ;
        $message->is_read = $newStatus;
        $message->save();

        return $this->showOne($message, 201);
    }

    public function getUnreadReceivedMessage()
    {
        $messages = auth()->user()->receivedMessages()
            ->where([
                'is_read' => Message::STATUS_UNREAD
            ])
            ->with(['sender:id,email', 'recipient:id,email'])
            ->latest()
            ->get();

        return $this->showAll($messages);
    }

    private function findUserMessageById($messageId)
    {
        $messages = auth()->user()->messages()->get()
            ->filter(function ($msg) use ($messageId) {
                return (int)$msg->id === (int)$messageId;
            });

        if ($messages->count() === 0)
            return null;

        return $messages->first();
    }
}
