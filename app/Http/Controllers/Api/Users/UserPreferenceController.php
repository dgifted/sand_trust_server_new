<?php

namespace App\Http\Controllers\Api\Users;

use App\Http\Controllers\Api\ApiBaseController;
use App\Models\UserPreference;
use Illuminate\Http\Request;

class UserPreferenceController extends ApiBaseController
{
    public function __construct()
    {
        $this->middleware(['auth:sanctum']);
    }

    public function getUserPreference()
    {
        $preference = $this->getPreference();

        return $this->showOne($preference);
    }

    public function updateUserLanguage(Request $request)
    {
        $lang = !!$request->get('lang') ? $request->get('lang') : 'English';
        $preference = $this->getPreference();
        $preference->lang = $lang;
        $preference->save();

        return $this->showOne($preference);
    }

    public function updateUserTheme()
    {
        $preference = $this->getPreference();
        $theme = $preference->theme === UserPreference::LIGHT_MODE ? UserPreference::DARK_MODE : UserPreference::LIGHT_MODE;
        $preference->theme = $theme;
        $preference->save();

        return $this->showOne($preference);
    }

    public function updateUserNotification()
    {
        $preference = $this->getPreference();
        $notifyState = $preference->notification_state === UserPreference::NOTIFICATION_ON ? UserPreference::NOTIFICATION_OFF : UserPreference::NOTIFICATION_ON;
        $preference->notification_state = $notifyState;
        $preference->save();

        return $this->showOne($preference);
    }

    private function getPreference()
    {
        $user = auth()->user();
        $preference = $user->preference()->firstOrCreate([]);

        return $preference->refresh();
    }
}
