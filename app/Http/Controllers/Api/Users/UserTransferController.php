<?php

namespace App\Http\Controllers\Api\Users;

use App\Http\Controllers\Api\ApiBaseController;
use App\Mail\TransferCompleted;
use App\Mail\TransferCreatedMail;
use App\Models\Transfer;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;

class UserTransferController extends ApiBaseController
{
    public function __construct()
    {
        $this->middleware('auth:sanctum');
    }

    public function index()
    {
        $transfers = auth()->user()->transfers()
            ->with(['payer:id,first_name', 'currency:id,name,abbreviation'])
            ->get();
        return $this->showAll($transfers);
    }

    public function show($transferId)
    {
        $transfer = $this->findUserTransferById($transferId);

        if (!$transfer)
            return $this->errorResponse('Transfer not found.', 404);

        return $this->showOne($transfer);
    }

    public function store(Request $request)
    {
        $request->validate([
            'payeeName' => ['required'],
            'payeeBankName' => ['required'],
            'payeeAccountNumber' => ['required'],
            'payeeSortcode' => ['required'],
            'amount' => ['required']
        ]);

        $user = auth()->user();
        $account = $user->accounts()->first();

        $transfer = Transfer::create([
            'currency_id' => $account->currency->id,
            'source_account_id' => $account->id,
            'payer_id' => auth()->id(),
            'payee_name' => $request->get('payeeName'),
            'payee_account_number' => $request->get('payeeAccountNumber'),
            'payee_sort_code' => $request->get('payeeSortcode'),
            'amount' => $request->get('amount'),
            'ref_id' => Transfer::generateReferenceID(),
            'otp' => Transfer::generateOtp(),
        ]);

        $currencyAbbreviation = auth()->user()->accounts()->first()->currency->abbreviation;

        auth()->user()->notificationMessages()->create([
            'title' => 'Transfer OTP Alert',
            'description' => 'One time password (OTP) for the transfer with reference ID : ' . $transfer->ref_id .
                ' and amount ' . $currencyAbbreviation . ' ' . $transfer->amount
        ]);

        Mail::to(auth()->user()->email)->send(new TransferCreatedMail($transfer->otp, auth()->user()));
        return $this->showOne($transfer, 201);
    }

    public function confirm(Request $request)
    {
        $transfers = $this->checkInCompletedTransfer();
        if ($transfers->count() === 0)
            return $this->errorResponse('You do not have a pending transfer');

        $transferIds = $transfers->pluck('otp')->toArray();
        $data = ['offset'];
        foreach ($transferIds as $key => $val) {
            array_push($data, $val);
        }

        $targetTransferFound = array_search($request->get('otp'), $data, false);

        if (!$targetTransferFound)
            return $this->errorResponse('The otp is incorrect.', 404);

        $transfer = Transfer::where([
            'payer_id' => auth()->id(),
            'otp' => $request->get('otp'),
        ])->first();

        if (!$transfer)
            return $this->errorResponse('Transfer has either been completed or was deleted.', 404);
        if ($transfer->status === Transfer::STATUS_COMPLETED)
            return $this->errorResponse('Transfer has been completed.', 404);

        $success = false;
        $senderAccount = $transfer->sourceAccount;

        DB::transaction(function () use (&$senderAccount, &$success, &$transfer) {
            $transfer->status = Transfer::STATUS_COMPLETED;
            $transfer->otp = null;
            $transfer->save();

            $senderAccount->balance = (double)$senderAccount->balance - (double)$transfer->amount;
            $senderAccount->save();

            $success = true;
        });

        if (!$success)
            return $this->errorResponse('Transfer failed. Please try again later.');

        $currency = $senderAccount->currency->abbreviation;
        auth()->user()->notificationMessages()->create([
            'title' => 'Transfer Success Alert',
            'description' => 'Your transfer of ' . $currency . ' ' . $transfer->amount . ' to ' . $transfer->payee_account_number . ' is successful.'
        ]);
        Mail::to(auth()->user()->email)->send(new TransferCompleted($transfer->amount, $currency, $transfer->payee_account_number, auth()->user()));

        return $this->showOne($transfer, 201);
    }

    public function getUserInCompleteTransfers()
    {
        return $this->showAll($this->checkInCompletedTransfer());
    }

    public function regenerateTransferOtp($transferId)
    {
        $transfer = $this->findUserTransferById($transferId);

        if (!$transfer)
            return $this->errorResponse('Transfer with specified identification could not be found', 404);
        if ($transfer->status === Transfer::STATUS_COMPLETED)
            return $this->errorResponse('Transfer has been completed.', 404);

        $success = false;
        DB::transaction(function () use (&$success, &$transfer) {
            $transfer->otp = Transfer::generateOtp();
            $transfer->save();

            $currencyAbbreviation = auth()->user()->accounts()->first()->currency->abbreviation;

            auth()->user()->notificationMessages()->create([
                'title' => 'Transfer OTP Resent',
                'description' => 'One time password (OTP) for the transfer with reference ID : ' . $transfer->ref_id .
                    ' and amount ' . $currencyAbbreviation . ' ' . $transfer->amount . ' has been resent.'
            ]);
            Mail::to(auth()->user()->email)->send(new TransferCreatedMail($transfer->otp, auth()->user()));
            $success = true;
        });

        if (!$success)
            return $this->errorResponse('An unexpect error occurred. Please try again.');

        return $this->showOne($transfer);
    }

    private function checkInCompletedTransfer()
    {
        return auth()->user()->transfers()->where(['status' => Transfer::STATUS_PENDING])->get();
    }

    private function findUserTransferById($transferId)
    {
        $transfers = auth()->user()->transfers()->get()
            ->filter(function ($transfer) use ($transferId) {
                return (int)$transfer->id === (int)$transferId;
            });

        if ($transfers->count() === 0)
            return null;

        return $transfers->first();
    }
}
