<?php

namespace App\Http\Controllers\Api\Users;

use App\Http\Controllers\Api\ApiBaseController;
use App\Models\Deposit;
use App\Models\Transfer;
use App\Traits\Utils;
use Illuminate\Http\Request;

class UserTransactionController extends ApiBaseController
{
    use Utils;

    public function __construct()
    {
        $this->middleware(['auth:sanctum']);
    }

    public function index()
    {
        $user = auth()->user();
        $transactions = [];

        $user->deposits()->get()
            ->each(function ($deposit) use (&$transactions, $user) {
                $timeArray = $deposit->created_at->setTimezone('Africa/Lagos')->toArray();
                $timeString = $timeArray['hour'] . ':' . $timeArray['minute'];
                array_push($transactions, [
                    'id' => $deposit->ref_id,
                    'type' => 'deposit',
                    'amount' => $deposit->amount,
                    'createdOn' => $deposit->created_at->diffForHumans(),
                    'createdTime' => $this->convertTimeTOAMPM($timeString),
                    'status' => $deposit->status_name,
                    'timeStamp' => $deposit->created_at,
                    'receiver' => $user->first_name . ' ' . $user->last_name
                ]);
            });
        $user->transferOuts()->get()
            ->each(function ($transfer) use (&$transactions) {
                $timeArray = $transfer->created_at->setTimezone('Africa/Lagos')->toArray();
                $timeString = $timeArray['hour'] . ':' . $timeArray['minute'];
                array_push($transactions, [
                    'id' => $transfer->ref_id,
                    'type' => 'transfer',
                    'amount' => $transfer->amount,
                    'createdOn' => $transfer->created_at->diffForHumans(),
                    'createdTime' => $this->convertTimeTOAMPM($timeString),
                    'status' => $transfer->status_name,
                    'timeStamp' => $transfer->created_at,
                    'receiver' => $transfer->payee_name
                ]);
            });

        $transactions = collect($transactions)->sortByDesc('timeStamp');

        return $this->showAll($transactions);
    }

    public function show(Request $request, $id)
    {
        $type = $request->has('type') ? $request->get('type') : 'deposit';

        if ($type == 'deposit') {
            $deposit = Deposit::where('ref_id', $id)->first();
            if (!$deposit)
                return $this->errorResponse('Deposit not found.', 404);
            return $this->showOne($deposit);
        }

        $transaction = Transfer::where('ref_id', $id)->with(['payer'])->first();
        if (!$transaction)
            return $this->errorResponse('Transfer not found.', 404);
        return $this->showOne($transaction);
    }
}
