<?php

namespace App\Http\Controllers\Api\Users;

use App\Http\Controllers\Api\ApiBaseController;
use App\Models\LocalBank;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

class UserLocalBankController extends ApiBaseController
{
    public function __construct()
    {
        $this->middleware(['auth:sanctum']);
    }

    public function getUserLocalBanks()
    {
        $banks = auth()->user()->localBanks()->where(['is_approved' => LocalBank::APPROVED])->get();

        return $this->showAll($banks);
    }

    public function storeBank(Request $request)
    {
        $request->validate([
            'bankName' => ['required'],
            'bankAccountNumber' => ['required'],
        ]);

        $bankAccNumbers = LocalBank::all()->pluck('bank_account_number');
        $numberExists = $bankAccNumbers->contains($request->get('bankAccountNumber'));
        if ($numberExists)
            return $this->errorResponse('Bank account number already linked.');

        $bank = auth()->user()->localBanks()->create([
            'ref_id' => Str::random(32),
            'bank_name' => $request->get('bankName'),
            'bank_account_number' => $request->get('bankAccountNumber')
        ]);

        return $this->showOne($bank, 201);
    }
}
