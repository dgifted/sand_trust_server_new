<?php

namespace App\Http\Controllers\Api\Users;

use App\Http\Controllers\Api\ApiBaseController;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

class UserCardRequestController extends ApiBaseController
{
    public function __construct()
    {
        $this->middleware(['auth:sanctum']);
    }

    public function getActiveCardRequest()
    {
        $activeRequest = auth()->user()->cardRequest()->first();

        if (!$activeRequest)
            return $this->errorResponse('You do not have a pending card request.', 404);

        return $this->showOne($activeRequest);
    }

    public function requestCard(Request $request)
    {
        if (!$request->has('cardType') || !$request->get('cardType'))
            return $this->errorResponse('Card type is required.');

        $cardRequest = auth()->user()->cardRequest()->create([
            'card_type_id' => $request->get('cardType'),
            'ref_id' => Str::random(32)
        ]);

        return $this->showOne($cardRequest, 201);
    }

    public function cancelCardRequest($cardRefId)
    {
        $userCardRequest = auth()->user()->cardRequest()->first();

        if ($userCardRequest->ref_id != $cardRefId)
            return $this->errorResponse('Sorry you do not have a pending card request.');

        $userCardRequest->delete();

        return $this->showOne($userCardRequest, 201);
    }
}
