<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColsToAccountTypesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('account_types', function (Blueprint $table) {
            $table->double('max_credit_limit')->after('title');
            $table->double('min_credit_limit')->default(1000)->after('max_credit_limit');
        });

        \App\Models\AccountType::all()
            ->each(function ($accountType) {
                if ($accountType->title === 'basic') {
                    $accountType->max_credit_limit = 2000;
                    $accountType->save();
                }
                if ($accountType->title === 'premium') {
                    $accountType->max_credit_limit = 5000;
                    $accountType->save();
                }
                if ($accountType->title === 'gold') {
                    $accountType->max_credit_limit = 10000;
                    $accountType->save();
                }
                if ($accountType->title === 'platinum') {
                    $accountType->max_credit_limit = 20000;
                    $accountType->save();
                }
            });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('account_types', function (Blueprint $table) {
            $table->dropColumn(['max_credit_limit', 'min_credit_limit']);
        });
    }
}
