<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRolesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('roles', function (Blueprint $table) {
            $table->id();
            $table->string('label');
            $table->timestamps();
        });

        foreach ([\App\Models\Role::ROLE_ADMIN, \App\Models\Role::ROLE_CLIENT, \App\Models\Role::ROLE_STAFF] as $value) {
            \App\Models\Role::create([
                'label' => $value
            ]);
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('roles');
    }
}
