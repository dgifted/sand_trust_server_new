<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTransfersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('transfers', function (Blueprint $table) {
            $table->id();
            $table->foreignId('currency_id');
            $table->bigInteger('source_account_id');

            $table->bigInteger('payer_id');
            $table->string('payee_name');
            $table->bigInteger('payee_account_number');
            $table->string('payee_sort_code')->nullable();
            $table->double('amount')->default(0.0);
            $table->string('ref_id');
            $table->tinyInteger('status')->default(\App\Models\Transfer::STATUS_PENDING);
            $table->tinyInteger('type')->default(\App\Models\Transfer::TYPE_EXPENSE);
            $table->string('otp')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('transfers');
    }
}
