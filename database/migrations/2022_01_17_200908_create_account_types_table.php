<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAccountTypesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('account_types', function (Blueprint $table) {
            $table->id();
            $table->string('title');
            $table->timestamps();
        });

        foreach ([
                     \App\Models\AccountType::BASIC,
                     \App\Models\AccountType::PREMIUM,
                     \App\Models\AccountType::GOLD,
                     \App\Models\AccountType::PLATINUM
                 ] as $value) {
            \App\Models\AccountType::create(['title' => $value]);
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('account_types');
    }
}
