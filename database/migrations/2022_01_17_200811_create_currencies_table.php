<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCurrenciesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('currencies', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->string('abbreviation');
            $table->timestamps();
        });

        $currencies = [
            0 => [
                'name' => 'United States Dollar',
                'abbr' => 'usd'
            ],
            1 => [
                'name' => 'Euro',
                'abbr' => 'eur'
            ],
            3 => [
                'name' => 'Pound Sterling',
                'abbr' => 'gbp'
            ],
        ];

        foreach ($currencies as $value) {
            \App\Models\Currency::create([
                'name' => $value['name'],
                'abbreviation' => $value['abbr']
            ]);
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('currencies');
    }
}
