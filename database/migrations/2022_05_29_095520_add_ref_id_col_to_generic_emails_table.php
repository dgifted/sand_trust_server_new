<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddRefIdColToGenericEmailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('generic_emails', function (Blueprint $table) {
            $table->string('ref_id')->after('id');
        });

        \App\Models\GenericEmail::all()
            ->each(function ($email) {
                $email->ref_id = \Illuminate\Support\Str::random(32);
                $email->save();
            });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('generic_emails', function (Blueprint $table) {
            $table->dropColumn('ref_id');
        });
    }
}
