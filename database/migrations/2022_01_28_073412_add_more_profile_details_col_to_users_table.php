<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddMoreProfileDetailsColToUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->enum('sex', [
                \App\Models\User::SEX_FEMALE,
                \App\Models\User::SEX_MALE,
                \App\Models\User::SEX_OTHERS,
            ])->nullable();
            $table->string('country')->nullable();
            $table->string('occupation')->nullable();
            $table->string('passport')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn(['sex', 'country', 'occupation', 'passport']);
        });
    }
}
