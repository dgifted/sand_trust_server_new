<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLocalBanksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('local_banks', function (Blueprint $table) {
            $table->id();
            $table->foreignId('user_id');
            $table->string('ref_id');
            $table->string('bank_name'); //TODO removed unique method
            $table->string('bank_account_number')->unique();
            $table->tinyInteger('is_approved')->default(\App\Models\LocalBank::APPROVAL_PENDING);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('local_banks');
    }
}
