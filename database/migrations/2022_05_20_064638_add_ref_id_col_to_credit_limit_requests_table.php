<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddRefIdColToCreditLimitRequestsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('credit_limit_requests', function (Blueprint $table) {
            $table->string('ref_id')->after('id');
        });

        App\Models\CreditLimitRequest::all()
            ->each(function ($req) {
                $req->ref_id = \Illuminate\Support\Str::random(32);
                $req->save();
            });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('credit_limit_requests', function (Blueprint $table) {
            $table->dropColumn('ref_id');
        });
    }
}
