<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class SupportTicketsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\Models\SupportTicket::factory()->count(20)->create();
    }
}
