<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class GenericEmailsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\Models\GenericEmail::factory()->count(30)->create();
    }
}
