<?php

namespace Database\Seeders;

use App\Models\SupportReply;
use Illuminate\Database\Seeder;

class SupportRepliesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        SupportReply::factory()->count(50)->create();
    }
}
