<?php

namespace Database\Factories;

use App\Models\SupportTicket;
use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Factory;

class SupportReplyFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     * @throws \Exception
     */
    public function definition()
    {
        $supportTickets = SupportTicket::all()->pluck('id')->toArray();
        $users = User::all()->pluck('id')->toArray();
        return [
            'support_ticket_id' => $this->faker->randomElement($supportTickets),
            'author_id' => $this->faker->randomElement($users),
            'content' => $this->faker->paragraph(random_int(2, 6))
        ];
    }
}
