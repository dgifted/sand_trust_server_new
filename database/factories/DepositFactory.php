<?php

namespace Database\Factories;

use App\Models\Deposit;
use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Factory;

class DepositFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     * @throws \Exception
     */
    public function definition()
    {
        $userIds = User::all()->pluck('id')->toArray();
        $user = $this->faker->randomElement($userIds);

        return [
            'user_id' => $user,
            'ref_id' => Deposit::generateReferenceID(),
            'amount' => random_int(0, 200000),
            'status' => $this->faker->randomElement([
                Deposit::STATUS_PENDING,
                Deposit::STATUS_COMPLETED,
                Deposit::STATUS_FAILED
            ])
        ];
    }
}
