<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

class CardFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     * @throws \Exception
     */
    public function definition()
    {
        $usersId = \App\Models\User::all('id')->pluck('id')->toArray();
        $acctIds = \App\Models\Account::all('id')->pluck('id')->toArray();
        $cardTypeIds = \App\Models\CardType::all('id')->pluck('id')->toArray();

        return [
            'user_id' => $this->faker->randomElement($usersId),
            'account_id' => $this->faker->randomElement($acctIds),
            'card_type_id' => $this->faker->randomElement($cardTypeIds),
            'cvv' => random_int(100, 999),
            'number' => \App\Models\Card::generateCardNumber(),
            'pin' => random_int(100, 999),
            'expires_month' => random_int(1, 12),
            'expires_year' => $this->faker->year(),
        ];
    }
}
