<?php

namespace Database\Factories;

use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;

class GenericEmailFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     * @throws \Exception
     */
    public function definition()
    {
        $userIds = User::all()->pluck('id')->toArray();

        return [
            'recipient_id' => $this->faker->randomElement($userIds),
            'subject' => $this->faker->sentence(random_int(8,15)),
            'content' => $this->faker->paragraph(random_int(3, 5)),
            'ref_id' => Str::random(32)
        ];
    }
}
