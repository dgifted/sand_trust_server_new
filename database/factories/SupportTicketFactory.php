<?php

namespace Database\Factories;

use App\Models\SupportTicket;
use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Factory;

class SupportTicketFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     * @throws \Exception
     */
    public function definition()
    {
        $users = User::all()->pluck('id')->toArray();

        return [
            'author_id' => $this->faker->randomElement($users),
            'title' => $this->faker->sentence(random_int(3, 7)),
            'content' => $this->faker->paragraph(random_int(2, 5)),
            'is_closed' => $this->faker->randomElement([SupportTicket::OPEN, SupportTicket::CLOSED])
        ];
    }
}
