<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

class StatisticFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     * @throws \Exception
     */
    public function definition()
    {
        $accountsId = \App\Models\Account::all('id')->pluck('id')->toArray();

        return [
            'account_id' => $this->faker->randomElement($accountsId),
            'type' => $this->faker->randomElement([
                \App\Models\Statistic::TYPE_INCOME,
                \App\Models\Statistic::TYPE_EXPENSES
            ]),
            'amount' => (double)random_int(100, 99999),
        ];
    }
}
